"""Utilities for API endpoints allowing users to search for certain objects.
These can, for example, be ``list_``, ``mass_edit`` and ``mass_delete``.
"""

import operator
import typing

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import database

__all__ = [
	"get_order_by_expression",
	"parse_search"
]


OPERATORS = {
	"$equals": lambda attr, value: attr == value,
	"$less_than": lambda attr, value: attr < value,
	"$greater_than": lambda attr, value: attr > value,
	"$less_than_or_equal_to": lambda attr, value: attr <= value,
	"$greater_than_or_equal_to": lambda attr, value: attr >= value,
	"$in": lambda attr, value: attr.in_(value),
	"$contains": lambda attr, value: attr.any(
		value,
		operator=operator.eq
	),
	"$regex": lambda attr, value: attr.regexp_match(value)
}
r"""Operators for ``filter`` values in API endpoints. Generally
self-explanatory.

``$equals``:
	``attr`` is equal to ``value``. ``attr`` can be of virtually any type,
	``value`` can as well.

``$less_than``:
	``attr``'s value is lower than ``value``. ``attr`` must be a type which
	supports these comparisons - for example, an
	:class:`Integer <sqlalchemy.Integer>` or
	:class:`DateTime <sqlalchemy.DateTime>`.
	:class:`String <sqlalchemy.String>`\ s are not allowed, comparing them in
	this way is allowed in some database engines, but causes errors in others.
	It is also largely useless and its functionality can be replaced with regex.

``$greater_than``:
	``attr``'s value is higher than ``value``. ``attr`` must be a type which
	supports these comparisons - for example, an
	:class:`Integer <sqlalchemy.Integer>` or
	:class:`DateTime <sqlalchemy.DateTime>`.

``$less_than_or_equal_to``:
	``attr``'s value is lower than ``value``, or equal to it. ``attr`` must be a
	type which supports these comparisons - for example, an
	:class:`Integer <sqlalchemy.Integer>` or
	:class:`DateTime <sqlalchemy.DateTime>`.

``$greater_than_or_equal_to``:
	``attr``'s value is greater than ``value``, or equal to it. ``attr`` must be a
	type which supports these comparisons - for example, an
	:class:`Integer <sqlalchemy.Integer>` or
	:class:`DateTime <sqlalchemy.DateTime>`.

``$in``:
	``attr``'s value equals one of the values contained within ``value``. Like
	``$equals``, both ``attr`` and ``value``'s contents can be of virtually any
	type. ``value`` itself must be an array.

``$contains``:
	``attr``, an ``ARRAY``, contains ``value``. Not supported with
	:class:`String <sqlalchemy.String>`\ s, due to potential inconsistencies and
	regex being far more suitable for this use as well.

``$regex``:
	``attr`` matches the regex contained within ``value``. Both must be of the
	:class:`String <sqlalchemy.String>` type.

.. seealso::
	:func:`.parse_search`
"""


def parse_search(
	conditions: typing.Dict[
		str,
		typing.Union[
			typing.Dict,
			typing.Any
		]
	],
	model: database.Base
) -> typing.Union[
	sqlalchemy.sql.elements.BinaryExpression,
	sqlalchemy.sql.elements.ClauseList
]:
	"""Converts the given dictionary formatted conditions, ones seen in different
	API endpoints' ``filter`` keys, to SQLAlchemy ones. The conditions are
	presumed to have already been validated using Cerberus and no extra checks
	are performed.

	:param conditions: The conditions.
	:param model: The model to use the given conditions on.

	:returns: SQLAlchemy conditions.

	Example:
		.. code-block:: python

			{
				"$and": [
					{"$greater_than": {"post_count": 1000}},
					{"$or": [
						{"$equals": {"user_id": "UUID_HERE"}},
						{"$equals": {"user_id": "UUID_HERE"}}
					]}
				]
			}

		Is converted to:

		.. code-block:: sql

			post_count > 1000 AND (user_id = UUID_HERE OR user_id = UUID_HERE)

	.. seealso::
		:const:`.OPERATORS`
	"""

	condition_operator = next(iter(conditions))

	if condition_operator == "$and":
		return sqlalchemy.and_(
			parse_search(condition, model)
			for condition in conditions[condition_operator]
		)

	if condition_operator == "$or":
		return sqlalchemy.or_(
			parse_search(condition, model)
			for condition in conditions[condition_operator]
		)

	if condition_operator == "$not":
		return sqlalchemy.not_(
			parse_search(
				conditions[condition_operator],
				model
			)
		)

	attr = next(iter(conditions[condition_operator]))

	return OPERATORS[condition_operator](
		getattr(model, attr),
		conditions[condition_operator][attr]
	)


def get_order_by_expression(
	model: sqlalchemy.orm.DeclarativeMeta,
	nulls_last: bool = True
) -> sqlalchemy.sql.elements.UnaryExpression:
	"""Generates an expression from the ``order`` key in a JSON request body which
	other queries can use to order their results. Contains the ``NULLS LAST``
	modifier by default. This function must be run inside a Flask request context.

	:param model: The model, one of whose columns should be used for ordering.
	:param nulls_last: Whether or not ``NULL`` values should be last. :data:`True`
		by default.

	:returns: The expression.
	"""

	order_column = getattr(
		model,
		flask.g.json["order"]["by"]
	)

	order_expression = (
		sqlalchemy.asc(order_column)
		if flask.g.json["order"]["asc"]
		else sqlalchemy.desc(order_column)
	)

	if nulls_last:
		return sqlalchemy.nulls_last(order_expression)

	return order_expression
