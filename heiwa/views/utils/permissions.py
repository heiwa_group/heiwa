"""Utilities for the handling of permissions within API endpoints.

.. seealso::
	:class:`BasePermissionMixin <heiwa.database.utils.BasePermissionMixin>`

	:class:`ForumPermissionMixin <heiwa.database.ForumPermissionMixin>`
"""

import functools
import typing

import flask
import sqlalchemy.orm

from heiwa import database, exceptions

__all__ = [
	"requires_permission",
	"validate_permission"
]


def requires_permission(
	action: str,
	resource: typing.Union[
		sqlalchemy.orm.DeclarativeMeta,
		database.Base
	],
	**kwargs
) -> typing.Callable:
	"""A wrapper that checks whether or not the current :attr:`flask.g.user` has
	permission to perform ``action`` on the given ``resource``. If not, an
	exception is raised.

	:param action: The action.
	:param resource: The resource the action should be performed on. If this is
		not an instance of a model, but only a model, the decision is based on
		its ``static_actions`` attribute. Otherwise, the decision is based on
		``instance_actions``.
	:param kwargs: Keyword arguments to be passed to the underlying function.

	:raises heiwa.exceptions.APINoPermission: Raised when the user does not have
		permission to perform said action. The resource name, as well as the action
		are included in its
		:attr:`details <heiwa.exceptions.APINoPermission.details>`.

	.. seealso::
		:class:`PermissionControlMixin <heiwa.database.utils.PermissionControlMixin>`
	"""

	def wrapper(
		func: typing.Callable
	) -> typing.Callable:
		@functools.wraps(func)
		def decorator(*wrapped_args, **wrapped_kwargs) -> typing.Any:
			validate_permission(
				flask.g.user,
				action,
				resource,
				**kwargs
			)

			return func(*wrapped_args, **wrapped_kwargs)
		return decorator
	return wrapper


def validate_permission(
	user: database.User,
	action: str,
	resource: typing.Union[
		sqlalchemy.orm.DeclarativeMeta,
		database.Base
	],
	**kwargs
) -> None:
	"""Checks whether or not ``user`` has permission to perform ``action`` on the
	given ``resource``. If not, an exception is raised.

	:param action: The action.
	:param resource: The resource the action should be performed on. If this is
		not an instance of a model, but only a model, the decision is based on
		its ``static_actions`` attribute. Otherwise, the decision is based on
		``instance_actions``.
	:param kwargs: Keyword arguments to be passed to the underlying function.

	:raises heiwa.exceptions.APINoPermission: Raised when the user does not have
		permission to perform said action. The resource name, as well as the action
		are included in its
		:attr:`details <heiwa.exceptions.APINoPermission.details>`.

	.. seealso::
		:class:`PermissionControlMixin <heiwa.database.utils.PermissionControlMixin>`
	"""

	if isinstance(resource, sqlalchemy.orm.DeclarativeMeta):
		resource_name = resource.__name__

		allowed = resource.static_actions[action](user, **kwargs)
	else:
		resource_name = resource.__class__.__name__

		allowed = resource.instance_actions[action](user, **kwargs)

	if not allowed:
		raise exceptions.APINoPermission({
			"resource": resource_name,
			"action": action
		})
