r"""Tables relating to the :class:`.User`\ s who have subscribed to different
:class:`.Forum`\ s.
"""

import sqlalchemy

from .. import Base, utils

__all__ = ["forum_subscribers"]


forum_subscribers = sqlalchemy.Table(
	"forum_subscribers",
	Base.metadata,
	sqlalchemy.Column(
		"forum_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	),
	sqlalchemy.Column(
		"user_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
)
r"""A table defining which :class:`.User`\ s have subscribed to which
:class:`.Forum`\ s.
"""
