r"""API views for :class:`Group <heiwa.database.Group>`\ s."""

import http.client
import operator
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_group,
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)
from .permissions import group_permissions_blueprint
from .utils import get_if_last_default_group

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"list_",
	"mass_delete",
	"mass_edit",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"group_blueprint",
	"group_permissions_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.0.13"

group_blueprint = flask.Blueprint(
	"group",
	__name__,
	url_prefix="/groups"
)

group_blueprint.register_blueprint(group_permissions_blueprint)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"maxlength": constants.BIG_INTEGER_LIMIT
	},
	"default_for": {
		"type": "list",
		"check_with": "has_no_duplicates",
		"schema": {
			"type": "string",
			"minlength": 1,
			"maxlength": (
				database.Group.default_for.property.columns[0].type.item_type.length
			)
		}
	},
	"level": {
		"type": "integer",
		"min": -2147483647,
		"max": 2147483647
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Group.name.property.columns[0].type.length
	},
	"description": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Group.description.property.columns[0].type.length
	}
}

CREATE_EDIT_SCHEMA = {
	"default_for": {
		**ATTR_SCHEMAS["default_for"],
		"required": True
	},
	"level": {
		**ATTR_SCHEMAS["level"],
		"required": True
	},
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"description": {
		**ATTR_SCHEMAS["description"],
		"nullable": True,
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"level"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=group_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(group_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"level": ATTR_SCHEMAS["level"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"default_for": ATTR_SCHEMAS["default_for"],
			"level": ATTR_SCHEMAS["level"],
			"name": ATTR_SCHEMAS["name"],
			"description": {
				**ATTR_SCHEMAS["description"],
				"nullable": True
			}
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"default_for": {
				"type": "list",
				"schema": ATTR_SCHEMAS["default_for"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"level": {
				"type": "list",
				"schema": ATTR_SCHEMAS["level"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"description": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["description"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$contains": {
		"type": "dict",
		"schema": {
			"default_for": ATTR_SCHEMAS["default_for"]["schema"]
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			},
			"description": {
				**ATTR_SCHEMAS["description"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@group_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Group)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a group with the requested ``name``, ``description``,
	``default_for`` and ``level`` attributes.

	:returns: The newly created group, with the HTTP ``201`` status code.
	"""

	group = database.Group.create(
		flask.g.sa_session,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(group), http.client.CREATED


@group_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Group)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all groups that the current :attr:`flask.g.user` is allowed to view
	and match the requested filter, if one is given.

	:returns: The list of groups, with the HTTP ``200`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Group
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Group.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Group),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@group_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"default_for": ATTR_SCHEMAS["default_for"],
				"level": ATTR_SCHEMAS["level"],
				"name": ATTR_SCHEMAS["name"],
				"description": {
					**ATTR_SCHEMAS["description"],
					"nullable": True
				}
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Group)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all groups that the current :attr:`flask.g.user` is allowed to.
	If there is a filter requested, they must also match it.

	If ``default_for`` is being edited and does not contain ``*``, the last
	(as per the given order) group which is default for ``*`` is excluded.

	:returns: :data:`None`, with the HTTP ``200`` status code.
	"""

	order_column = getattr(
		database.Group,
		flask.g.json["order"]["by"]
	)

	conditions = (
		database.Group.id != (
			sqlalchemy.select(database.Group.id).
			where(
				database.Group.default_for.any(
					"*",
					operator=operator.eq
				)
			).
			order_by(
				# Order the opposite way, not ``NULLS FIRST``
				sqlalchemy.asc(order_column)
				if not flask.g.json["order"]["asc"]
				else sqlalchemy.desc(order_column)
			).
			limit(1).
			scalar_subquery()
		)
		if (
			"default_for" in flask.g.json["values"] and
			"*" not in flask.g.json["values"]["default_for"]
		)
		else True
	)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Group
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Group).
		where(
			database.Group.id.in_(
				database.Group.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.Group.action_queries["edit"](flask.g.user),
					conditions=conditions,
					order_by=get_order_by_expression(database.Group),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@group_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Group)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all groups that the current :attr:`flask.g.user` is allowed to.
	If there is a filter requested, they must also match it.

	The last (as per the given order) group which is default for ``*`` is
	excluded.

	:returns: :data:`None`, with the HTTP ``200`` status code.
	"""

	order_column = getattr(
		database.Group,
		flask.g.json["order"]["by"]
	)

	# Don't delete the last default group
	conditions = (
		database.Group.id != (
			sqlalchemy.select(database.Group.id).
			where(
				database.Group.default_for.any(
					"*",
					operator=operator.eq
				)
			).
			order_by(
				# Order the opposite way, not ``NULLS FIRST``
				sqlalchemy.asc(order_column)
				if not flask.g.json["order"]["asc"]
				else sqlalchemy.desc(order_column)
			).
			limit(1).
			scalar_subquery()
		)
	)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Group
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.Group).
		where(
			database.Group.id.in_(
				database.Group.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.Group.action_queries["delete"](flask.g.user),
					conditions=conditions,
					order_by=get_order_by_expression(database.Group),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@group_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Group)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the group with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group.

	:returns: The group, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_group(id_)
	), http.client.OK


@group_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Group)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the group with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group.

	:raises heiwa.exceptions.APIGroupUnchanged: Raised when no attributes of
		the group would be changed, should the function continue.

	:returns: The updated group, with the HTTP ``200`` status code.
	"""

	group = get_group(id_)

	validate_permission(
		flask.g.user,
		"edit",
		group
	)

	handle_edit(
		group,
		flask.g.json,
		exceptions.APIGroupUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(group), http.client.OK


@group_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Group)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the group with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group.

	:raises heiwa.exceptions.APIGroupIsLastDefault: Raised when the group is the
		last one whose :attr:`default_for <heiwa.database.Group.default_for>`
		attribute contains ``*``, meaning it's default for all users.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	group = get_group(id_)

	validate_permission(
		flask.g.user,
		"delete",
		group
	)

	if "*" in group.default_for and get_if_last_default_group(group.id):
		raise exceptions.APIGroupIsLastDefault

	group.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@group_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Group)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is authorized to perform on
	the group with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group.

	:returns: The list of allowed actions, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_group(id_)
	), http.client.OK


@group_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is authorized to
	perform on all groups, without any knowledge of their identity.

	:returns: The list of allowed actions, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		database.Group.get_allowed_static_actions(flask.g.user)
	), http.client.OK
