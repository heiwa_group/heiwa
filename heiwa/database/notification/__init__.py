"""Notification models."""

from __future__ import annotations

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants, enums

from .. import Base, utils

__all__ = ["Notification"]
__version__ = "1.1.2"


class Notification(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	Base
):
	"""Notification model."""

	__tablename__ = "notifications"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who received a
	notification.
	"""

	is_read = sqlalchemy.Column(
		sqlalchemy.Boolean,
		default=False,
		nullable=False
	)
	"""Whether or not a notification has been read by its receiver."""

	type = sqlalchemy.Column(
		sqlalchemy.Enum(enums.NotificationTypes),
		nullable=False
	)
	"""The type of a notification. These types are defined in
	:class:`NotificationTypes <heiwa.enums.NotificationTypes>`. The
	``NOTIFICATION_TYPES`` attribute of models which can be subjects of
	notifications define which ones relate to them.
	"""

	identifier = sqlalchemy.Column(
		utils.UUID,
		nullable=False
	)
	"""The identifier (usually ``id`` attribute) of the object a notification
	relates to.

	.. note::
		Since this can relate to multiple types of objects whose identifiers
		aren't guaranteed not to collide, this column has not been defined as a
		foreign or unique key.
	"""

	def get_id_url(self: Notification) -> str:
		"""Gets the JSON-LD ``@id`` route to this notification.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"notification.view",
			id_=self.id,
			_external=True
		)


sqlalchemy.event.listen(
	Notification.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION notifications_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE users
					SET notification_count = notification_count + 1
					WHERE
						id = NEW.user_id AND
						notification_count < {constants.BIG_INTEGER_LIMIT};

					IF (NEW.is_read IS FALSE) THEN
						UPDATE users
						SET notification_unread_count = notification_unread_count + 1
						WHERE
							id = NEW.user_id AND
							notification_unread_count < {constants.BIG_INTEGER_LIMIT};
					END IF;
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE users
					SET notification_count = notification_count - 1
					WHERE id = OLD.user_id;

					IF (OLD.is_read IS TRUE) THEN
						UPDATE users
						SET notification_unread_count = notification_unread_count - 1
						WHERE id = OLD.user_id;
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE OR REPLACE FUNCTION notifications_edit_update_counters()
		RETURNS TRIGGER
		AS $trigger$
			BEGIN
				IF NEW.is_read = OLD.is_read THEN
					RETURN NULL;
				END IF;

				IF NEW.is_read IS TRUE THEN
					UPDATE users
					SET notification_unread_count = notification_unread_count - 1
					WHERE id = NEW.user_id;
				ELSE
					UPDATE users
					SET notification_unread_count = notification_unread_count + 1
					WHERE
						id = NEW.user_id AND
						notification_unread_count < {constants.BIG_INTEGER_LIMIT};
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON notifications
		FOR EACH ROW EXECUTE FUNCTION notifications_create_delete_update_counters();

		CREATE TRIGGER edit_update_counters
		AFTER UPDATE OF is_read
		ON notifications
		FOR EACH ROW EXECUTE FUNCTION notifications_edit_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
