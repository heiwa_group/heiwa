"""API views for
:obj:`thread_subscribers <heiwa.database.thread_subscribers>`, belonging under
the default :class:`Thread <heiwa.database.Thread>` set of views.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import get_thread, requires_permission, validate_permission

__all__ = [
	"create",
	"delete",
	"thread_subscription_blueprint",
	"view"
]

thread_subscription_blueprint = flask.Blueprint(
	"subscription",
	__name__,
	url_prefix="/<uuid:id_>/subscription"
)


@thread_subscription_blueprint.route("", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_subscription", database.Thread)
def create(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Creates a subscription for :attr:`flask.g.user` to the thread with the
	requested ``id_``.
	"""

	thread = get_thread(id_)

	validate_permission(
		flask.g.user,
		"edit_subscription",
		thread
	)

	if thread.get_is_subscribed(flask.g.user.id):
		raise exceptions.APIThreadSubscriptionAlreadyExists

	flask.g.sa_session.execute(
		sqlalchemy.insert(database.thread_subscribers).
		values(
			thread_id=thread.id,
			user_id=flask.g.user.id
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@thread_subscription_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view_subscription", database.Thread)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Returns whether or not :attr:`flask.g.user` is subscribed to the thread
	with the requested ``id_``.
	"""

	thread = get_thread(id_)

	validate_permission(
		flask.g.user,
		"view_subscription",
		thread
	)

	return flask.jsonify(
		thread.get_is_subscribed(flask.g.user.id)
	), http.client.OK


@thread_subscription_blueprint.route("", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_subscription", database.Thread)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Removes :attr:`flask.g.user`'s subscription to the thread with the requested
	``id_``.
	"""

	thread = get_thread(id_)

	validate_permission(
		flask.g.user,
		"edit_subscription",
		thread
	)

	if not thread.get_is_subscribed(flask.g.user.id):
		raise exceptions.APIThreadSubscriptionNotFound

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.thread_subscribers).
		where(
			sqlalchemy.and_(
				database.thread_subscribers.c.thread_id == thread.id,
				database.thread_subscribers.c.user_id == flask.g.user.id
			)
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT
