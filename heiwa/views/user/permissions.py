r"""API views for
:class:`UserPermissions <heiwa.database.UserPermissions>`, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	BASE_PERMISSION_CREATE_EDIT_SCHEMA,
	get_session_and_user_defaults,
	handle_edit,
	requires_permission,
	validate_permission
)
from .utils import get_user_self_or_id, validate_user_self_or_id_exists

__all__ = [
	"delete",
	"edit",
	"get_permissions",
	"user_permissions_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

user_permissions_blueprint = flask.Blueprint(
	"permissions",
	__name__
)


def get_permissions(
	user_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.UserPermissions:
	"""Gets the user permissions with the given ``user_id``.

	:param user_id: The :attr:`user_id <heiwa.database.UserPermissions.user_id>`
		of the permissions to find.
	:param session: The session to find the permissions with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the permissions. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIUserPermissionsNotFound: Raised when the
		permissions don't exist, or the ``user`` does not have permission to view
		them.

	:returns: The permissions.
	"""

	session, user = get_session_and_user_defaults(session, user)

	permissions = session.execute(
		database.UserPermissions.get(
			session,
			user,
			conditions=(database.UserPermissions.user_id == user_id)
		)
	).scalars().one_or_none()

	if permissions is None:
		raise exceptions.APIUserPermissionsNotFound

	return permissions


@user_permissions_blueprint.route(
	"/users/<uuid:id_>/permissions",
	methods=["GET"]
)
@user_permissions_blueprint.route(
	"/self/permissions",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserPermissions)
def view(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Gets the set permissions for the user with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current
		:attr:`flask.g.user`.

	:returns: The permissions, with the ``200`` HTTP status code.
	"""

	validate_user_self_or_id_exists(
		id_,
		flask.g.sa_session
	)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.UserPermissions.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=(database.UserPermissions.user_id == id_)
			)
		).scalars().one_or_none()
	), http.client.OK


@user_permissions_blueprint.route(
	"/users/<uuid:id_>/permissions",
	methods=["PUT"]
)
@user_permissions_blueprint.route(
	"/self/permissions",
	defaults={"id_": None},
	methods=["PUT"]
)
@validators.validate_json(BASE_PERMISSION_CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserPermissions)
def edit(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Updates set permissions for the user with the given ``id_``, or creates
	them if there aren't any.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current
		:attr:`flask.g.user`.

	:raises heiwa.database.APIUserPermissionsUnchanged: Raised when the
		permissions already existed, and the given attributes are the same as
		the past ones.

	:returns: The updated permissions, with the ``200`` HTTP status code. If
		they have been created, the status code is ``201``.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	permissions = flask.g.sa_session.execute(
		database.UserPermissions.get(
			flask.g.sa_session,
			flask.g.user,
			conditions=(database.UserPermissions.user_id == id_)
		)
	).scalars().one_or_none()

	if permissions is None:
		validate_permission(
			flask.g.user,
			"edit",
			database.UserPermissions,
			receiving_user=user
		)

		database.UserPermissions.create(
			flask.g.sa_session,
			user_id=user.id,
			**flask.g.json
		)

		status = http.client.CREATED
	else:
		validate_permission(
			flask.g.user,
			"edit",
			permissions
		)

		handle_edit(
			permissions,
			flask.g.json,
			exceptions.APIUserPermissionsUnchanged,
			current_utc_time=flask.g.current_utc_time
		)

		status = http.client.OK

	flask.g.sa_session.commit()

	return flask.jsonify(permissions), status


@user_permissions_blueprint.route(
	"/users/<uuid:id_>/permissions",
	methods=["DELETE"]
)
@user_permissions_blueprint.route(
	"/self/permissions",
	defaults={"id_": None},
	methods=["DELETE"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserPermissions)
def delete(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Deletes set permissions for the user with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current
		:attr:`flask.g.user`.

	:returns: :data:`None`, with the ``204`` HTTP status code.
	"""

	validate_user_self_or_id_exists(
		id_,
		flask.g.sa_session
	)

	permissions = get_permissions(id_ if id_ is not None else flask.g.user.id)

	validate_permission(
		flask.g.user,
		"edit",
		permissions
	)

	permissions.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@user_permissions_blueprint.route(
	"/users/<uuid:id_>/permissions/allowed-actions",
	methods=["GET"]
)
@user_permissions_blueprint.route(
	"/self/permissions/allowed-actions",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserPermissions)
def view_allowed_actions_instance(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on the
	user with the given ``id_``'s permissions. They must at least have the
	permission to view them.

	:param id_: The ID of the user. If :data:`None`, it's assumed to be that
		of the current user.

	:raises heiwa.exceptions.APIUserNotFound: Raised when the ``id_`` does not
		correspond to any user, or the current user does not have the permission
		to view them.
	:raises heiwa.exceptions.APIUserPermissionsNotFound: Raised when the user
		does not have any set permissions.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	validate_user_self_or_id_exists(
		id_,
		flask.g.sa_session
	)

	return flask.jsonify(
		get_permissions(id_ if id_ is not None else flask.g.user.id)
	), http.client.OK


@user_permissions_blueprint.route(
	"/users/permissions/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	user permissions, regardless of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.UserBan.get_allowed_static_actions(flask.g.user)
	), http.client.OK
