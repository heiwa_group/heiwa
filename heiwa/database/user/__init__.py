"""Models and tables for users."""

from __future__ import annotations

import datetime
import mimetypes
import io
import os
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.ext.hybrid
import sqlalchemy.orm

from heiwa import constants

from .. import Base, utils
from .ban import UserBan
from .blocks import user_blocks
from .follows import user_follows
from .groups import user_groups
from .permissions import UserPermissions
from .preferences import UserPreferences
from .reputation import UserReputation

__all__ = [
	"User",
	"UserBan",
	"UserPermissions",
	"UserPreferences",
	"UserReputation",
	"user_blocks",
	"user_follows",
	"user_groups"
]
__version__ = "1.7.0"


class User(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""User model."""

	__tablename__ = "users"

	registered_by = sqlalchemy.Column(
		sqlalchemy.String(128),
		index=True,
		nullable=True
	)
	r"""The service a user was registered by. By default, this can be one of
	2 values: ``guest``, for those who registered using the
	:func:`guest.token <heiwa.views.guest.token>` endpoint and ``oidc``,
	for those who signed up using :mod:`OIDC <heiwa.views.oidc>`.
	:class:`.Group`\ s who have this value in their
	:attr:`default_for <.Group.default_for>` column will automatically be assigned
	to the user.
	"""

	external_id = sqlalchemy.Column(
		sqlalchemy.String(64),
		index=True,
		nullable=True
	)
	r"""The service a user was registered by's identifier for them. For guests,
	this will be a version of their IP address hashed using SCrypt. For users
	registered using OIDC, it will be the ``sub`` key in ``userinfo``.

	.. note::
		Generally, guests will not have permission to create new content
		like threads and posts, but some administrators may allow them to
		do so. In those cases, once their sessions have expired, these
		accounts won't be deleted, but since there is no longer any use
		for it, the hashed IP address stored in this column will be erased.

	.. seealso::
		The OIDC `specification <https://oidc.net/specs/oidc-connect-cor\
		e-1_0.html#UserInfoResponse>`_ for a successful UserInfo response.

		:attr:`.User.has_content`
	"""

	# ^ Can't make a unique constraint on ``registered_by`` and ``external_id``
	# together, due to the possibility of multiple guest accounts being
	# made from the same IP address.

	last_active_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		nullable=True
	)
	"""The approximate last time a user has been active. If this is :data:`None`,
	the user has likely left the option to store this value disabled.

	.. seealso::
		:class:`.UserPreferences.should_record_activity`
	"""

	avatar_type = sqlalchemy.Column(
		sqlalchemy.String(255),
		default=None,
		nullable=True
	)
	r"""The media type of a user's avatar. If there is no avatar, it will remain
	`None`.

	.. seealso::
		IANA's `list <https://www.iana.org/assignments/media-types/media-types\
		.xhtml>`_ of officially recognized media types.

		`RFC 4288 - maximum length <https://datatracker.ietf.org/doc/html/rfc4\
		288#section-4.2>`_

	.. note::
		Media types were formerly recognized as MIME types and are still
		commonly referred to as such.
	"""

	is_banned = sqlalchemy.Column(
		sqlalchemy.Boolean,
		default=False,
		nullable=False
	)
	"""Whether or not a user has been banned. When a ban exists for this user,
	this must be :data:`True` in order for it to be recognized. Defaults to
	:data:`False`.

	.. seealso::
		:class:`.UserBan`

		:meth:`.User.create_ban()`

		:meth:`.User.delete_ban()`

		:meth:`.User.get_ban()`
	"""

	parsed_permissions = sqlalchemy.Column(
		sqlalchemy.JSON,
		nullable=False
	)
	"""A user's parsed permissions. This is a combination of the user's groups'
	permissions (where groups with the highest :attr:`level <.Group.level>`
	attribute take precedence) and permissions specific to this user.

	.. seealso::
		:class:`.GroupPermissions`

		:class:`.UserPermissions`

		:meth:`.User.parse_permissions`

	.. note::
		Parsed permissions don't necessarily need to be stored, but doing
		so will make their usage much faster.
	"""

	encrypted_private_key = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=True
	)
	r"""A user's RSA private key, encrypted using AES-CBC with a padding size
	of 16. If they choose not to supply one and enter it manually upon decryption
	of the :class:`.Message`\ s they have received, they can still set a
	:attr:`public_key <.User.public_key>`.
	"""

	encrypted_private_key_iv = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=True
	)
	"""The IV used during the AES encryption of a user's private key. If it's not
	:data:`None`, this must also be set..

	.. seealso::
		:attr:`.User.encrypted_private_key`
	"""

	public_key = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=True
	)
	r"""A user's RSA public key. This must be set for them to be able to receive
	:class:`.Message`\ s, which are always encrypted.

	.. note::
		No set length here, since users can have different key sizes. Though the
		API's upper limit is 4096 bits, which should be enough for effectively
		everyone now.
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=True
	)
	"""A user's name."""

	status = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
	"""A user's status. In general, the usage of this should be similar to status
	messages on GitLab and social media.
	"""

	followee_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The number of users this user is following."""

	follower_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The number of users following a user."""

	file_total_size = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total size of all :class:`.File`\ s this user has uploaded, in
	bytes.
	"""

	message_received_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total number of :class:`.Message`\ s this user has received."""

	message_received_unread_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total number of :class:`.Message`\ s this user has received where
	the :attr:`is_read <.Message.is_read>` attribute is :data:`False`.
	"""

	message_sent_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total number of :class:`.Message`\ s this user has sent."""

	notification_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total number of :class:`.Notification`\ s this user has."""

	notification_unread_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The total number of :class:`.Notification`\ s this user has where
	the :attr:`is_read <.Notification.is_read>` attribute is :data:`False`.
	"""

	thread_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The number of :class:`.Thread`\ s this user has made."""

	post_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The number of :class:`.Post`\ s this user has made."""

	reputation_value = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The final value of a user's reputation. Positive reputation points will
	add ``1``, negative ones ``-1`` and neutral ones ``0``. Negative numbers
	are allowed.

	.. seealso::
		:class:`.UserReputation`
	"""

	bans = sqlalchemy.orm.relationship(
		UserBan,
		backref=sqlalchemy.orm.backref(
			"user",
			uselist=False,
		),
		order_by=lambda: sqlalchemy.desc(UserBan.expiration_timestamp),
		passive_deletes="all",
		foreign_keys=[UserBan.receiver_id],
		lazy=True
	)
	"""All of this user's bans, past ones and possibly the current one. This can
	also be an empty list, if they have never been banned.

	.. seealso::
		:class:`.UserBan`
	"""

	blockees = sqlalchemy.orm.relationship(
		lambda: User,
		secondary=user_blocks,
		primaryjoin=lambda: User.id == user_blocks.c.blocker_id,
		secondaryjoin=lambda: User.id == user_blocks.c.blockee_id,
		backref="blockers",
		order_by=lambda: sqlalchemy.desc(User.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""The users this user has blocked.

	.. seealso::
		:obj:`.user_blocks`
	"""

	followees = sqlalchemy.orm.relationship(
		lambda: User,
		secondary=user_follows,
		primaryjoin=lambda: User.id == user_follows.c.follower_id,
		secondaryjoin=lambda: User.id == user_follows.c.followee_id,
		backref="followers",
		order_by=lambda: sqlalchemy.desc(User.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""The users this user has followed.

	.. seealso::
		:obj:`.user_follows`
	"""

	permissions = sqlalchemy.orm.relationship(
		UserPermissions,
		uselist=False,
		backref=sqlalchemy.orm.backref(
			"user",
			uselist=False
		),
		passive_deletes="all",
		lazy=True
	)
	r"""A list of permissions specific to this user, which takes precedence over
	all of the assigned :class:`.Group`\ s'.

	.. seealso::
		:class:`.UserPermissions`
	"""

	preferences = sqlalchemy.orm.relationship(
		UserPreferences,
		uselist=False,
		backref=sqlalchemy.orm.backref(
			"user",
			uselist=False
		),
		passive_deletes="all",
		lazy=True
	)
	"""A list of this user's preferences, created automatically when this user
	registered, or right before it.

	.. seealso::
		:class:`.UserPreferences`
	"""

	reputation = sqlalchemy.orm.relationship(
		UserReputation,
		backref=sqlalchemy.orm.backref(
			"user",
			uselist=False
		),
		order_by=lambda: sqlalchemy.desc(UserReputation.creation_timestamp),
		passive_deletes="all",
		foreign_keys=[UserReputation.receiver_id],
		lazy=True
	)
	"""This user's reputation points.

	.. seealso::
		:class:`.UserReputation`
	"""

	def get_id_url(self: User) -> str:
		"""Gets the JSON-LD ``@id`` route to this user.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"user.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: User) -> None:
		r"""Adds the following attributes:

		:attr:`instance_actions <.User.instance_actions>`:

			Actions a specific :class:`.User` is allowed to perform on another user.

			``create_ban``:
				Whether or not a user can ban another user.

			``create_reputation``:
				Whether or not a user can add reputation points to another user.

			``delete``:
				Whether or not a user can delete another user.

			``edit``:
				Whether or not a user can edit another user.

			``edit_ban``:
				Whether or not a user can edit another user's ban.

			``edit_block``:
				Whether or not a user can block / unblock another user.

			``edit_follow``:
				Whether or not a user can follow / unfollow another user.

			``edit_group``:
				Whether or not a user can edit the :class:`.Group`\ s assigned to another
				user.

			``edit_permissions``:
				Whether or not a user can edit another user's permissions.

			``view``:
				Whether or not a user can view another user.

			``view_ban``:
				Whether or not a user can view another user's ban.

			``view_block``:
				Whether or not a user can view their block for this user.

			``view_follow``:
				Whether or not a user can view their block for this user.

			``view_groups``:
				Whether or not a user can view another user's groups.

			``view_permissions``:
				Whether or not a user can view another user's permissions.

			.. warning::
				Input validation is not handled here. When permissions are being evaluated,
				it should have been performed already. If data entered is invalid (e.g.
				adding another entry to a one-on-one relasionship), the user's permission
				to perform said action can still be evaluated as :data:`True`.

		:attr:`controlled_columns <.User.controlled_columns>`:

			The columns in a :class:`.User` object, possibly allowed to be viewed
			by other users.

			Public, visible to all:
				:attr:`.User.id`

				:attr:`.User.creation_timestamp`

				:attr:`.User.edit_timestamp`

				:attr:`.User.edit_count`

				:attr:`.User.avatar_type`

				:attr:`.User.is_banned`

				:attr:`.User.parsed_permissions`

				:attr:`.User.public_key`

				:attr:`.User.name`

				:attr:`.User.status`

				:attr:`.User.follower_count`

				:attr:`.User.thread_count`

				:attr:`.User.post_count`

			Private, only visible to the user themselves:
				:attr:`.User.registered_by`

				:attr:`.User.external_id`

				:attr:`.User.encrypted_private_key`

				:attr:`.User.message_received_count`

				:attr:`.User.message_received_unread_count`

				:attr:`.User.message_sent_count`

				:attr:`.User.notification_count`

				:attr:`.User.notification_unread_count`
		"""

		super().permission_control_init()

		self.instance_actions = {
			"create_ban": self.get_instance_action_edit_ban,
			"create_reputation": self.get_instance_action_edit_reputation,
			"delete": self.get_instance_action_delete,
			"delete_reputation": self.get_instance_action_edit_reputation,
			"edit": self.get_instance_action_edit,
			"edit_ban": self.get_instance_action_edit_ban,
			"edit_block": self.get_instance_action_only_for_others,
			"edit_follow": self.get_instance_action_only_for_others,
			"edit_group": self.get_instance_action_edit_group,
			"edit_permissions": self.get_instance_action_edit_permissions,
			"edit_reputation": self.get_instance_action_edit_reputation,
			"view": self.get_instance_action_view,
			"view_ban": self.get_instance_action_view,
			"view_block": self.get_instance_action_view,
			"view_follow": self.get_instance_action_view,
			"view_groups": self.get_instance_action_view,
			"view_permissions": self.get_instance_action_view,
			"view_reputation": self.get_instance_action_view
		}

		self.controlled_columns = {
			"registered_by": self.get_viewable_column_private,
			"external_id": self.get_viewable_column_private,
			"encrypted_private_key": self.get_viewable_column_private,
			"file_total_size": self.get_viewable_column_private,
			"message_received_count": self.get_viewable_column_private,
			"message_received_unread_count": self.get_viewable_column_private,
			"message_sent_count": self.get_viewable_column_private,
			"notification_count": self.get_viewable_column_private,
			"notification_unread_count": self.get_viewable_column_private
		}

	def get_instance_action_delete(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this user.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return (
			self.id == user.id or (
				self.instance_actions["view"](user) and
				user.parsed_permissions["user_delete"] and
				user.highest_group.level > self.highest_group.level
			)
		)

	def get_instance_action_edit(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this user.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return (
			self.id == user.id or (
				self.instance_actions["view"](user) and
				user.parsed_permissions["user_edit"] and
				user.highest_group.level > self.highest_group.level
			)
		)

	def get_instance_action_edit_ban(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this user's ban.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:class:`.UserBan`
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["user_edit_ban"] and (
				self.id == user.id or
				user.highest_group.level > self.highest_group.level
			)
		)

	def get_instance_action_only_for_others(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to perform an action that
		should only be allowed if they're also not the current user.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.id != user.id
		)

	def get_instance_action_edit_group(
		self: User,
		user: User,
		edited_group=None
	) -> bool:
		r"""Checks whether or not ``user`` is allowed to edit one of this user's
		:class:`.Group`\ s.

		:param user: The performing user.
		:param edited_group: The group whose relationship with the user is being
			edited.

		:returns: The result of the check.

		.. seealso:
			:obj:`.user_groups`
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["user_edit_groups"] and
			user.highest_group.level > self.highest_group.level and
			(
				edited_group is None or
				user.highest_group.level > edited_group.level
			)
		)

	def get_instance_action_edit_permissions(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this user's
		permissions.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso:
			:class:`.UserPermissions`
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["user_edit_permissions"] and
			user.highest_group.level > self.highest_group.level
		)

	def get_instance_action_edit_reputation(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit their reputation
		points for another user.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:class:`.UserReputation`
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["user_edit_reputation"] and
			self.id != user.id
		)

	def get_instance_action_view(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to view the current user.
		Always :data:`True` by default.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return True

	def get_viewable_column_private(self: User, user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to view a private column.

		:param user: The user attempting to view the column.

		:returns: The result of the check.
		"""

		return self.id == user.id

	@staticmethod
	def get_static_action_edit_ban(user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit users' bans.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return (
			User.static_actions["view"](user) and
			user.parsed_permissions["user_edit_ban"]
		)

	@staticmethod
	def get_static_action_edit_group(user: User) -> bool:
		r"""Checks whether or not ``user`` is allowed to edit users' assigned
		:class:`.Group`\ s.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:obj:`.user_groups`

			:class:`.Group`
		"""

		return (
			User.static_actions["view"](user) and
			user.parsed_permissions["user_edit_groups"]
		)

	@staticmethod
	def get_static_action_edit_permissions(user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit users' permissions.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:class:`.UserPermissions`
		"""

		return (
			User.static_actions["view"](user) and
			user.parsed_permissions["user_edit_permissions"]
		)

	@staticmethod
	def get_static_action_edit_reputation(user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to edit their reputation
		points for other users.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:class:`.UserReputation`
		"""

		return (
			User.static_actions["view"](user) and
			user.parsed_permissions["user_edit_reputation"]
		)

	@staticmethod
	def get_static_action_view(user: User) -> bool:
		"""Checks whether or not ``user`` is allowed to view other users.

		:param user: The performing user.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_action_query_delete(user: User) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to delete.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			sqlalchemy.or_(
				User.id == user.id,
				sqlalchemy.and_(
					user.parsed_permissions["user_delete"],
					user.highest_group.level > User.highest_group.level
				)
			)
		)

	@staticmethod
	def get_action_query_edit(user: User) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to edit.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			sqlalchemy.or_(
				User.id == user.id,
				sqlalchemy.and_(
					user.parsed_permissions["user_edit"],
					user.highest_group.level > User.highest_group.level
				)
			)
		)

	@staticmethod
	def get_action_query_edit_ban(
		user: User
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to edit the bans of.

		:param user: The user.

		:returns: The query.

		.. seealso::
			:class:`.UserBan`
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			sqlalchemy.or_(
				User.id == user.id,
				sqlalchemy.and_(
					user.parsed_permissions["user_edit_ban"],
					user.highest_group.level > User.highest_group.level
				)
			)
		)

	@staticmethod
	def get_action_query_only_for_others(
		user: User
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to perform an action on that should only be allowed if their
		identities are not the same.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			User.id != user.id
		)

	@staticmethod
	def get_action_query_edit_group(
		user: User
	) -> sqlalchemy.sql.elements.ClauseList:
		r"""Generates a selectable condition representing which users ``user`` is
		allowed to edit the assigned :class:`.Group`\ s of.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			user.parsed_permissions["user_edit_group"],
			user.highest_group.level > User.highest_group.level
		)

	@staticmethod
	def get_action_query_edit_permissions(
		user: User
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to the permissions of.

		:param user: The user.

		:returns: The query.

		.. seealso::
			:class:`.UserPermissions`
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			user.parsed_permissions["user_edit_permissions"],
			user.highest_group.level > User.highest_group.level
		)

	@staticmethod
	def get_action_query_edit_reputation(
		user: User
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to edit the reputation points of.

		:param user: The performing user.

		:returns: The result of the check.

		.. seealso::
			:class:`.UserReputation`
		"""

		return sqlalchemy.and_(
			User.action_queries["view"](user),
			user.parsed_permissions["user_edit_reputation"],
			user.id != User.id
		)

	@staticmethod
	def get_action_query_view(user: User) -> bool:
		"""Generates a selectable condition representing which users ``user`` is
		allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		return True

	static_actions = {
		"create_ban": get_static_action_edit_ban,
		"create_reputation": get_static_action_edit_reputation,
		"delete": get_static_action_view,
		"delete_reputation": get_static_action_edit_reputation,
		"edit": get_static_action_view,
		"edit_ban": get_static_action_edit_ban,
		"edit_block": get_static_action_view,
		"edit_follow": get_static_action_view,
		"edit_group": get_static_action_edit_group,
		"edit_permissions": get_static_action_edit_permissions,
		"edit_reputation": get_static_action_edit_reputation,
		"view": get_static_action_view,
		"view_ban": get_static_action_view,
		"view_block": get_static_action_view,
		"view_follow": get_static_action_view,
		"view_groups": get_static_action_view,
		"view_permissions": get_static_action_view,
		"view_reputation": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any user, without any
	indication of which one it is.

	``create_ban``:
		Whether or not a user can ban another user.

	``create_reputation``:
		Whether or not a user can add reputation points to another user.

	``delete``:
		Whether or not a user can delete another user.

	``edit``:
		Whether or not a user can edit another user.

	``edit_ban``:
		Whether or not a user can edit another user's ban.

	``edit_block``:
		Whether or not a user can block / unblock another user.

	``edit_follow``:
		Whether or not a user can follow / unfollow another user.

	``edit_group``:
		Whether or not a user can edit the :class:`.Group`\ s assigned to another
		user.

	``edit_permissions``:
		Whether or not a user can edit another user's permissions.

	``view``:
		Whether or not a user can view another user.

	``view_ban``:
		Whether or not a user can view another user's ban.

	``view_block``:
		Whether or not a user can view their blocks for other users.

	``view_follow``:
		Whether or not a user can view their follows for other users.

	``view_groups``:
		Whether or not a user can view another user's groups.

	``view_permissions``:
		Whether or not a user can view another user's permissions.
	"""

	action_queries = {
		"create_ban": get_action_query_edit_ban,
		"create_reputation": get_action_query_edit_reputation,
		"delete": get_action_query_delete,
		"delete_reputation": get_action_query_edit_reputation,
		"edit": get_action_query_edit,
		"edit_ban": get_action_query_edit_ban,
		"edit_block": get_action_query_only_for_others,
		"edit_follow": get_action_query_only_for_others,
		"edit_group": get_action_query_edit_group,
		"edit_permissions": get_action_query_edit_permissions,
		"edit_reputation": get_action_query_edit_reputation,
		"view": get_action_query_view,
		"view_ban": get_action_query_view,
		"view_block": get_action_query_view,
		"view_follow": get_action_query_view,
		"view_groups": get_action_query_view,
		"view_permissions": get_action_query_view,
		"view_reputation": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Group.instance_actions>`.
	"""

	def write(
		self: User,
		session: sqlalchemy.orm.Session,
		bypass_first_user_check: bool = False,
		current_app: typing.Union[flask.Flask, None] = None,
		avatar: typing.Union[None, bytes] = None,
		avatar_type: typing.Union[None, str] = None,
		avatar_location: typing.Union[None, str] = None
	) -> None:
		r"""If no :class:`.Group`\ s have already been assigned to this user,
		they're set to all groups where the :attr:`default_for <.Group.default_for>`
		column contains the service this user was registered by or `*`.

		If the provided Flask app has not yet been configured and
		``bypass_first_user_check`` is :data:`False`, all groups specified in its
		``GROUPS_FIRST_USER`` config key will also be added.

		If no :class:`.UserPreferences` instance exists with this user attached,
		it's automatically created and filled with the default values.

		If no parsed permissions have been given, they're automatically parsed.

		:param session: The session to add the user instance to.
		:param bypass_first_user_check: Whether or not to bypass the check for
			this being the first user registered on the service.
		:param current_app: The current Flask app. If :data:`None`, all checks
			concerning it are skipped.

		.. warning::
			If an avatar is to be set, the following **2** arguments have to
			contain a non-:data:`None` value.

		:param avatar: The user's avatar, in bytes.
		:param avatar_type: The media (MIME) type of the avatar.
		:param avatar_location: A full path to where avatars are stored. Optional,
			defaults to the ``AVATAR_LOCATION`` environment variable or
			``avatars``.

		.. seealso::
			:obj:`.user_groups`

			:attr:`.User.registered_by`

			:attr:`.User.parsed_permissions`

			:attr:`.User.preferences`

			:attr:`.ConfiguredLockFlask.configured`

			:meth:`.User.parse_permissions`
		"""

		from ..group import Group

		if self.parsed_permissions is None:
			self.parsed_permissions = {}

		# Premature session add and flush. We have to access the ID later.
		session.add(self)
		session.flush()

		if not (
			session.execute(
				sqlalchemy.select(Group).
				where(
					Group.id.in_(
						sqlalchemy.select(user_groups.c.group_id).
						where(user_groups.c.user_id == self.id)
					)
				).
				exists().
				select()
			).scalars().one()
		):
			group_conditions = sqlalchemy.or_(
				Group.default_for.any("*"),  # ``eq`` by default
				# NOTE: Is there any way to properly do ``startswith`` here?
				# This works too and might even be better in some cases, but
				# I'm not completely happy with it.
				Group.default_for.any(self.registered_by)
			)

			if (
				not bypass_first_user_check and
				current_app is not None and
				not current_app.configured
			):
				group_conditions = sqlalchemy.or_(
					group_conditions,
					Group.name.in_(current_app.config["GROUPS_FIRST_USER"])
				)

				current_app.configured = True

			group_ids = session.execute(
				sqlalchemy.select(Group.id).
				where(group_conditions).
				order_by(
					sqlalchemy.desc(Group.level)
				)
			).scalars().all()

			self.logger.debug(
				"Generated default groups for user ID %s: %s",
				self.id,
				group_ids
			)

			for group_id in group_ids:
				session.execute(
					sqlalchemy.insert(user_groups).
					values(
						user_id=self.id,
						group_id=group_id
					)
				)

		if self.preferences is None:
			UserPreferences.create(
				session,
				user_id=self.id
			)

		if self.parsed_permissions == {}:
			self.parse_permissions()

		if (
			avatar is not None and
			avatar_type is not None
		):
			self.set_avatar(
				avatar,
				avatar_type,
				avatar_location
			)

		return super().write(session)

	@sqlalchemy.ext.hybrid.hybrid_property
	def has_content(self: User) -> bool:
		r"""Checks whether or not this user has created any of the following:

			- :class:`.UserBan`\ s.
			- :class:`.UserReputation` points.
			- :class:`.Thread`\ s.
			- :class:`.Post`\ s.
			- :class:`.ThreadVote`\ s.
			- :class:`.PostVote`\ s.
			- Sent :class:`.Message`\ s.

		:returns: The result of the check.

		.. note::
			This function doesn't use the ``has_content`` expression, since it's
			not necessary to emit additional SQL queries for the thread and post
			count.
		"""

		from ..message import Message
		from ..post import PostVote
		from ..thread import ThreadVote

		return (
			self.thread_count == 0 and
			self.post_count == 0 and
			sqlalchemy.orm.object_session(self).execute(
				sqlalchemy.select(
					(
						sqlalchemy.select(UserBan.id).
						where(UserBan.giver_id == self.id).
						exists().
						scalar_subquery()
					),
					(
						sqlalchemy.select(UserReputation.id).
						where(UserReputation.giver_id == self.id).
						exists().
						scalar_subquery()
					),
					(
						sqlalchemy.select(ThreadVote.user_id).
						where(ThreadVote.user_id == self.id).
						exists().
						scalar_subquery()
					),
					(
						sqlalchemy.select(PostVote.user_id).
						where(PostVote.user_id == self.id).
						exists().
						scalar_subquery()
					),
					(
						sqlalchemy.select(Message.sender_id).
						where(
							sqlalchemy.and_(
								Message.sender_id == self.id,
								Message.receiver_id != self.id
							)
						).
						exists().
						scalar_subquery()
					)
				)
			).scalars().one()
		)

	@has_content.expression
	def has_content(cls: User) -> sqlalchemy.sql.elements.ClauseList:
		r"""Generates a set of SQLAlchemy conditions representing whether or
		not a user has created any of the following:

			- :class:`.UserBan`\ s.
			- :class:`.UserReputation` points.
			- :class:`.Thread`\ s.
			- :class:`.Post`\ s.
			- :class:`.ThreadVote`\ s.
			- :class:`.PostVote`\ s.
			- Sent :class:`.Message`\ s.

		:returns: The set of conditions.
		"""

		from ..message import Message
		from ..post import PostVote
		from ..thread import ThreadVote

		return sqlalchemy.and_(
			(
				sqlalchemy.select(UserBan.id).
				where(UserBan.giver_id == cls.id).
				exists()
			),
			(
				sqlalchemy.select(UserReputation.id).
				where(UserReputation.giver_id == cls.id).
				exists()
			),
			cls.thread_count == 0,
			cls.post_count == 0,
			(
				sqlalchemy.select(ThreadVote.user_id).
				where(ThreadVote.user_id == cls.id).
				exists()
			),
			(
				sqlalchemy.select(PostVote.user_id).
				where(PostVote.user_id == cls.id).
				exists()
			),
			(
				sqlalchemy.select(Message.sender_id).
				where(
					sqlalchemy.and_(
						Message.sender_id == cls.id,
						Message.receiver_id != cls.id
					)
				).
				exists()
			)
		)

	@sqlalchemy.ext.hybrid.hybrid_property
	def highest_group(self: User):
		"""Finds the :class:`.Group` with the highest :attr:`level <.Group.level>`
		this user has.

		:returns: The group.
		"""

		from ..group import Group

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(Group).
			where(
				Group.id.in_(
					sqlalchemy.select(user_groups.c.group_id).
					where(user_groups.c.user_id == self.id)
				)
			).
			order_by(
				sqlalchemy.desc(Group.level)
			).
			limit(1)
		).scalars().one()

	@highest_group.expression
	def highest_group(cls: User) -> sqlalchemy.sql.Select:
		r"""Generates a selection query that represents the :class:`.Group` with
		the highest :attr:`level <.Group.level>` this user has.

		:returns: The query.
		"""

		from ..group import Group

		return (
			sqlalchemy.select(Group).
			where(
				Group.id.in_(
					sqlalchemy.select(user_groups.c.group_id).
					where(user_groups.c.user_id == cls.id)
				)
			).
			order_by(
				sqlalchemy.desc(Group.level)
			).
			limit(1)
		)

	def get_avatar_location(
		self: User,
		location: typing.Union[None, str] = None
	) -> typing.Union[None, str]:
		"""Gets the full location to where this user's avatar is stored.

		This can, for example, be:
		``/home/heiwa/avatars/4a8b4487-4b19-4d98-b8b5-7d7333ed785c.png``

		:param location: The directory where avatars are stored. If :data:`None`,
			this is automatically set to the ``AVATAR_LOCATION`` environment
			variable. If that is also unset, the final fallback is
			``$current_working_directory/avatars``.

		:returns: The full avatar location. If there is no avatar type set for
			this user, :data:`None` is returned instead - they have no
			registered avatar.
		"""

		if self.avatar_type is None:
			return None

		if location is None:
			location = os.environ.get(
				"AVATAR_LOCATION",
				os.path.join(os.getcwd(), "avatars")
			)

		extension = mimetypes.guess_extension(self.avatar_type)

		if extension is None:
			self.logger.warning(
				"Could not guess user ID %s's avatar extension. The set type is %s",
				self.id,
				self.avatar_type
			)

			extension = ""

		return os.path.join(
			location,
			f"{self.id}{extension}"
		)

	def get_avatar(
		self: User,
		storage_location: typing.Union[None, str] = None
	) -> typing.Union[None, io.BufferedReader]:
		"""Gets this user's avatar, if there is one.

		:param storage_location: The directory where avatars are stored.

		:returns: A byte stream with the avatar contents, or :data:`None` if
			there isn't one.

		.. seealso::
			``storage_location`` can be :data:`None`, see
			:meth:`.User.get_avatar_location`.
		"""

		if self.avatar_type is None:
			return None

		with open(
			self.get_avatar_location(storage_location),
			"rb"
		) as avatar_file:
			return avatar_file

	def set_avatar(
		self: User,
		avatar: bytes,
		type_: str,
		storage_location: typing.Union[None, str] = None
	) -> None:
		"""Updates this user's avatar.

		:param avatar: The avatar's bytes..
		:param type_: The avatar's media (MIME) type.
		:param storage_location: The directory where avatars are stored.

		.. seealso::
			``storage_location`` can be :data:`None`, see
			:meth:`.User.get_avatar_location`.
		"""

		self.logger(
			"Setting avatar for user ID %s, %s bytes",
			self.id,
			len(avatar)
		)

		self.avatar_type = type_

		with open(
			self.get_avatar_location(storage_location),
			"wb"
		) as avatar_file:
			avatar_file.write(avatar)

	def delete_avatar(
		self: User,
		storage_location: typing.Union[None, str] = None
	) -> None:
		"""Deletes this user's avatar.

		:param storage_location: The directory where avatars are stored.

		.. seealso::
			``storage_location`` can be :data:`None`, see
			:meth:`.User.get_avatar_location`.
		"""

		location = self.get_avatar_location(storage_location)

		self.logger(
			"Deleting user ID %s's avatar, %s bytes",
			self.id,
			os.path.getsize(location)
		)

		if self.avatar_type is not None:
			os.remove(location)

		self.avatar_type = None

	def get_ban(
		self: User,
		check_expired: bool = False
	) -> typing.Union[None, UserBan]:
		"""Gets this user's current ban.

		:param check_expired: Whether or not to also check that the current ban
			is expired and handle it if so. :data:`False` by default.

		:returns: The ban.

		.. seealso::
			:class:`.UserBan`
		"""

		ban = sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(UserBan).
			where(UserBan.receiver_id == self.id).
			order_by(
				sqlalchemy.desc(UserBan.expiration_timestamp)
			).
			limit(1)
		).scalars().one_or_none()

		if (
			check_expired and
			ban is not None and
			ban.is_expired
		):
			self.delete_ban()

			return None

		return ban

	def create_ban(
		self: User,
		giver_id: uuid.UUID,
		expiration_timestamp: datetime.datetime,
		reason: typing.Union[None, str] = None
	) -> UserBan:
		"""Bans this user with the provided values.

		:param giver_id: The ID of the user who issued the ban.
		:param expiration_timestamp: The timestamp of the ban's expiry.
		:param reason: An optional reason why the ban was issued. While recommended,
			this is not required.

		:returns: The newly created ban.

		.. warning::
			This function doesn't validate whether or not there is ban already
			present. It will run without any exceptions, but having 2 non-expired
			bans existing at the same time can quietly break other things in
			unexpected ways.

		.. seealso::
			:class:`.UserBan`

			:attr:`.UserBan.expiration_timestamp`

			:attr:`.UserBan.reason`
		"""

		self.is_banned = True

		return UserBan.create(
			sqlalchemy.orm.object_session(self),
			giver_id=giver_id,
			receiver_id=self.id,
			expiration_timestamp=expiration_timestamp,
			reason=reason
		)

	def delete_ban(
		self: User,
		set_expiration_timestamp: bool = False,
		current_utc_time: typing.Union[None, datetime.datetime] = None
	) -> None:
		"""Unbans this user. To preserve a history of past bans, the current ban
		is not deleted.

		:param set_expiration_timestamp: Whether or not to also set the current
			ban's :attr:`expiration_timestamp <.UserBan.expiration_timestamp> to
			the current date and time. This is primarily used for premature
			unbans. :data:`False` by default.
		:param current_utc_time: The current time, in the UTC timezone. If
			``set_expiration_timestamp`` is :data:`True`, this is used as the
			new expiration timestamp value. If :data:`None`, the time is recorded
			at runtime.

		.. warning::
			This function does not validate whether or not there is a ban actually
			present. If there isn't, it will break.

		.. seealso::
			:class:`.UserBan`
		"""

		self.is_banned = False

		if set_expiration_timestamp:
			sqlalchemy.orm.object_session(self).execute(
				sqlalchemy.update(UserBan).
				where(
					sqlalchemy.and_(
						UserBan.receiver_id == self.id,
						~UserBan.is_expired
					)
				).
				values(
					expiration_timestamp=(
						current_utc_time
						if current_utc_time is not None
						else datetime.datetime.now(tz=datetime.timezone.utc)
					)
				)
			)

	def get_is_blockee(self: User, user_id: uuid.UUID) -> bool:
		"""Checks whether or not this user has blocked the user with the given
		``user_id``.

		:param user_id: The :attr:`id <.User.id>` of the user to check.

		:returns: The result of the check.
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(user_blocks.c.blocker_id).
			where(
				sqlalchemy.and_(
					user_blocks.c.blocker_id == self.id,
					user_blocks.c.blockee_id == user_id
				)
			).
			exists().
			select()
		).scalars().one()

	def get_is_followee(self: User, user_id: uuid.UUID) -> bool:
		"""Checks whether or not this user has followed the user with the given
		``user_id``.

		:param user_id: The :attr:`id <.User.id>` of the user to check.

		:returns: The result of the check.
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(user_follows.c.follower_id).
			where(
				sqlalchemy.and_(
					user_follows.c.follower_id == self.id,
					user_follows.c.followee_id == user_id
				)
			).
			exists().
			select()
		).scalars().one()

	def get_in_group(self: User, group_id: uuid.UUID) -> bool:
		"""Checks whether or not this user is part of the :class:`.Group` with
		the given ``group_id``, as per :obj:`.user_groups`.

		:param group_id: The `id <.Group.id>` of the group.

		:returns: The result of the check.
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(user_groups.c.user_id).
			where(
				sqlalchemy.and_(
					user_groups.c.user_id == self.id,
					user_groups.c.group_id == group_id
				)
			).
			exists().
			select()
		).scalars().one()

	def parse_permissions(self: User) -> None:
		r"""Sets this user's :attr:`parsed_permissions <.User.parsed_permissions>`
		column to the combination of:

			#. This user's :class:`.Group`\ s' permissions, where the group with
			   the highest :attr:`level <.Group.level>` attribute is the most
			   important.
			#. This user's permissions.

		The lower on the list an item is, the higher priority it has.

		.. seealso::
			:class:`.GroupPermissions`

			:class:`.UserPermissions`
		"""

		from ..group import Group, GroupPermissions

		session = sqlalchemy.orm.object_session(self)

		result = {}

		group_permission_sets = session.execute(
			sqlalchemy.select(
				GroupPermissions
			).
			where(
				GroupPermissions.group_id.in_(
					sqlalchemy.select(user_groups.c.group_id).
					where(user_groups.c.user_id == self.id)
				)
			).
			order_by(
				sqlalchemy.desc(
					sqlalchemy.select(Group.level).
					where(Group.id == GroupPermissions.group_id).
					scalar_subquery()
				)
			)
		).scalars().all()

		self.logger.debug(
			"Parsing permissions for user id %s, group permission sets: %s",
			self.id,
			group_permission_sets
		)

		for group_number, group_permissions in enumerate(group_permission_sets):
			if group_permissions is None:
				continue

			for permission_name, permission_value in (
				group_permissions.to_permissions().items()
			):
				# Populates all permissions regardless of whether or not they are set.
				if permission_name in result:
					continue

				if permission_value is not None:
					result[permission_name] = permission_value
				elif len(group_permission_sets) - 1 == group_number:
					result[permission_name] = False

		own_permissions = session.execute(
			sqlalchemy.select(UserPermissions).
			where(UserPermissions.user_id == self.id)
		).scalars().one_or_none()

		if own_permissions is not None:
			self.logger.debug(
				"Parsing permissions for user id %s, accounting for own permissions: %s",
				self.id,
				own_permissions
			)

			for permission_name, permission_value in (
				own_permissions.to_permissions().items()
			):
				if permission_value is None:
					continue

				result[permission_name] = permission_value

		self.logger.debug(
			"Parsed permissions for user ID %s: %s",
			self.id,
			result
		)

		self.parsed_permissions = result


sqlalchemy.event.listen(
	UserReputation.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION user_reputation_create_delete_update_value()
		RETURNS TRIGGER
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
					IF (TG_OP = 'UPDATE' AND NEW.is_positive = OLD.is_positive) THEN
						RETURN NULL;
					END IF;

					IF NEW.is_positive IS TRUE THEN
						UPDATE users
						SET reputation_value = reputation_value + 1
						WHERE
							id = NEW.user_id AND
							reputation_value < {constants.BIG_INTEGER_LIMIT};
					ELSIF NEW.is_positive IS FALSE THEN
						UPDATE users
						SET reputation_value = reputation_value - 1
						WHERE id = NEW.user_id;
					END IF;
				ELSIF (TG_OP = 'DELETE') THEN
					IF OLD.is_positive IS TRUE THEN
						UPDATE users
						SET reputation_value = reputation_value - 1
						WHERE id = OLD.user_id;
					ELSIF OLD.is_positive IS FALSE THEN
						UPDATE users
						SET reputation_value = reputation_value + 1
						WHERE
							id = OLD.user_id AND
							reputation_value < {constants.BIG_INTEGER_LIMIT};
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_value
		AFTER INSERT OR DELETE OR UPDATE OF is_positive
		ON user_reputation
		FOR EACH ROW EXECUTE FUNCTION user_reputation_create_delete_update_value();
		"""
	).execute_if(dialect="postgresql")
)

sqlalchemy.event.listen(
	user_follows,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION user_follows_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE users
					SET followee_count = followee_count + 1
					WHERE
						id = NEW.follower_id AND
						followee_count < {constants.BIG_INTEGER_LIMIT};

					UPDATE users
					SET follower_count = follower_count + 1
					WHERE
						id = NEW.followee_id AND
						follower_count < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE users
					SET followee_count = followee_count - 1
					WHERE id = OLD.follower_id;

					UPDATE users
					SET follower_count = follower_count - 1
					WHERE id = OLD.followee_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON user_follows
		FOR EACH ROW EXECUTE FUNCTION user_follows_create_delete_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
