"""Models for local user accounts' TOTPs."""

from __future__ import annotations

import pyotp
import sqlalchemy

from .. import Base, utils

__all__ = ["LocalAccountTOTP"]


class LocalAccountTOTP(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Local user account TOTP model."""

	__tablename__ = "local_account_totps"

	local_account_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"local_accounts.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.LocalAccount.id>` of the local account a set of TOTP
	information belongs to.
	"""

	_last_used_code = sqlalchemy.Column(
		sqlalchemy.String(6),
		nullable=True
	)
	"""The last code used for a TOTP. This exists to prevent
	`replay attacks <https://en.wikipedia.org/wiki/Replay_attack>`.
	(`Wikiless <https://wikiless.org/wiki/Replay_attack>`)

	Since this will never be used as an integer and storing it as a string is
	more universal, a :class:`String <sqlalchemy.String>` type was chosen. If
	necessary, however, it should always be convertible back to numbers unless
	:data:`None`.
	"""

	backup_codes = sqlalchemy.Column(
		sqlalchemy.ARRAY(sqlalchemy.String(32)),
		nullable=False,
		default=[]
	)

	secret = sqlalchemy.Column(
		sqlalchemy.String(32),
		nullable=False,
		default=pyotp.random_base32
	)
	"""A TOTP secret.

	.. note::
		This was originally supposed to be implemented with encryption, where the
		account's plaintext password is used to decrypt the secret. In the end,
		however, it wouldn't have done much to improve security as an attacker
		with access to the database will be able to fake their way into the
		service anyway and TOTP secrets aren't sensitive in the same way passwords
		are.
	"""

	def write(
		self: LocalAccountTOTP,
		session: sqlalchemy.orm.Session,
		backup_code_count: int
	) -> None:
		"""Generates :attr:`backup_codes <.LocalAccountTOTP.backup_codes>` for
		the new instance.

		:param backup_code_count: The number of backup codes to generate.

		:param session: The session to add the new instance to.
		"""

		self.generate_backup_codes(backup_code_count)

		return super().write(session)

	def generate_backup_codes(
		self: LocalAccountTOTP,
		count: int
	) -> None:
		"""Generates new :attr:`backup_codes <.LocalAccountTOTP.backup_codes>`
		for this instance, ignoring ones that already exist.

		:param count: The amount of codes to generate.
		"""

		self.backup_codes = [
			pyotp.random_base32()
			for _ in range(count)
		]

	def get_code_valid(
		self: LocalAccountTOTP,
		code: str
	) -> bool:
		"""Checks whether or not the given ``code`` is valid for the
		:attr:`secret <.LocalAccountTOTP.secret>` right now.

		The code must:
			* Not be the same value as
			  :attr:`_last_used_code <.LocalAccountTOTP._last_used_code>`.
			* Be valid for the secret.

		If the code passes the check, the
		:attr:`_last_used_code <.LocalAccountTOTP._last_used_code>` is set to it.

		:param code: The 6-character number code.

		:returns: The result of the check.
		"""

		if code == self._last_used_code:
			return False

		totp = pyotp.TOTP(self.secret)

		if totp.verify(code):
			self._last_used_code = code

			return True

		return False
