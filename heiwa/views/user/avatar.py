r"""API views for :class:`User <heiwa.database.User>`\ s' avatars, belonging
under the default set of vies for :class:`User <heiwa.database.User>`\ s.

.. seealso::
	:meth:`heiwa.database.User.get_avatar`

	:meth:`heiwa.database.User.set_avatar`
"""

import http.client
import io
import os
import typing
import uuid

import flask
import magic

from heiwa import (
	authentication,
	database,
	exceptions,
	get_config,
	limiter,
	validators
)

from ..utils import requires_permission, validate_permission
from .utils import get_user_self_or_id

__all__ = [
	"delete",
	"edit",
	"user_avatar_blueprint",
	"view"
]

user_avatar_blueprint = flask.Blueprint(
	"avatar",
	__name__
)


@user_avatar_blueprint.route("/users/<uuid:id_>/avatar", methods=["GET"])
@user_avatar_blueprint.route(
	"/self/avatar",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.User)
def view(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Gets the user with the given ``id_``'s avatar.

	:param id_: The :attr: `id <heiwa.database.User.id>` of the user.

	:returns: The avatar, not encoded in base64. If there is no avatar, it's
		:data:`None` encoded in JSON. The HTTP status code is ``200``.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	if user.avatar_type is None:
		resposnse = flask.jsonify(None)
	else:
		location = user.get_avatar_location()

		resposnse = flask.send_file(
			location,
			mimetype=user.avatar_type,
			as_attachment=True,
			attachment_filename=os.path.basename(location),
			last_modified=os.path.getmtime(location)
		)

	return resposnse, http.client.OK


@user_avatar_blueprint.route("/users/<uuid:id_>/avatar", methods=["PUT"])
@user_avatar_blueprint.route(
	"/self/avatar",
	defaults={"id_": None},
	methods=["PUT"]
)
@validators.validate_json({
	"avatar": {
		"type": "binary",
		"coerce": "decode_base64",
		"maxlength": get_config()["USER_MAX_AVATAR_SIZE"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.User)
def edit(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Sets the avatar of the user with the requested ``id_`` to the decoded
	base64 value contained inside the ``avatar`` JSON key.

	The maximum length of the avatar is set to the ``USER_MAX_AVATAR_SIZE`` config
	key, if this function is defined while there is a Flask app context. If there
	isn't one, it falls back to 5 MiB. If the app is running in a regular manner,
	this should never happen.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user whose avatar
		should be edited. If :data:

	:raises heiwa.exceptions.APIUserAvatarUnchanged: Raised when the avatar is
		the same as the user's old one.
	:raises heiwa.exceptions.APIUserAvatarNotAllowedType: Raised when the avatar's
		media type is not one of those defined within the current app's config's
		``USER_AVATAR_TYPES`` key.

	:returns: :data:`None`. If there was an avatar before, the ``200`` HTTP
		status code is returned. Otherwise, it's ``201``.

	.. note::
		Only the first 4KiB (or less) of the avatar are considered when their type
		is being guessed. This means large avatars - although unlikely to exist
		in a production environment to the point where they would cause issues
		- will not unnecessarily use twice the memory space during the time
		they're processed.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	validate_permission(
		flask.g.user,
		"edit",
		user
	)

	if (
		user.avatar_type is not None and
		user.get_avatar().read() == flask.g.json["avatar"]
	):
		raise exceptions.APIUserAvatarUnchanged

	guessed_type = magic.from_buffer(
		io.BytesIO(flask.g.json["avatar"][:4096])
	)

	if guessed_type not in flask.current_app.config["USER_AVATAR_TYPES"]:
		raise exceptions.APIUserAvatarNotAllowedType(guessed_type)

	if user.avatar_type is None:
		status = http.client.CREATED
	else:
		status = http.client.OK

	user.set_avatar(
		flask.g.json["avatar"],
		guessed_type
	)

	user.edited(current_utc_time=flask.g.current_utc_time)

	return flask.jsonify(None), status


@user_avatar_blueprint.route("/users/<uuid:id_>/avatar", methods=["DELETE"])
@user_avatar_blueprint.route(
	"/self/avatar",
	defaults={"id_": None},
	methods=["DELETE"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.User)
def delete(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Deletes the user with the given ``id_``'s avatar.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIUserAvatarNotFound: Raised when no avatar was
		found for the user.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	validate_permission(
		flask.g.user,
		"edit",
		user
	)

	if user.avatar_type is None:
		raise exceptions.APIUserAvatarNotFound

	user.delete_avatar()

	return flask.jsonify(None), http.client.NO_CONTENT
