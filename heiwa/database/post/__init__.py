"""Post models."""

from __future__ import annotations

import typing

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants, enums

from .. import Base, utils
from .vote import PostVote

__all__ = [
	"Post",
	"PostVote"
]
__version__ = "1.1.15"


class Post(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Post model."""

	__tablename__ = "posts"

	thread_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"threads.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.Thread.id>` of the :class:`.Thread` a post is in."""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who created a post. This
	cannot be changed.
	"""

	subject = sqlalchemy.Column(
		sqlalchemy.String(256),
		nullable=True
	)
	"""The subject of a post."""

	content = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=False
	)
	"""A post's content."""

	vote_value = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The final value of a post's votes. Upvotes will add ``1``, downvotes
	will subtract ``1``. If there are no votes at all, this will be ``0``.
	Negative numbers are allowed.

	.. seealso::
		:class:`.PostVote`
	"""

	# Shortcut
	forum = sqlalchemy.orm.relationship(
		"Forum",
		uselist=False,
		secondary="threads",
		passive_deletes="all",
		overlaps="forum",  # Does it?
		lazy=True
	)
	"""A post's :class:`.Thread`'s :class:`.Forum`."""

	thread = sqlalchemy.orm.relationship(
		"Thread",
		uselist=False,
		passive_deletes="all",
		overlaps="forum",
		lazy=True
	)
	"""A post's :class:`.Thread`."""

	votes = sqlalchemy.orm.relationship(
		PostVote,
		backref=sqlalchemy.orm.backref(
			"post",
			uselist=False
		),
		order_by=sqlalchemy.desc(PostVote.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""A post's votes.

	.. seealso::
		:class:`.PostVote`
	"""

	user = sqlalchemy.orm.relationship(
		"User",
		order_by="desc(User.creation_timestamp)",
		passive_deletes="all",
		lazy=True
	)
	"""The :class:`.User` who created a post."""

	def get_id_url(self: Post) -> str:
		"""Gets the JSON-LD ``@id`` route to this post.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"post.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Post) -> None:
		r"""Adds the :attr:`instance_actions <.Post.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific posts:

			``delete``:
				Whether or not a user is allowed to delete this post.

			``edit``:
				Whether or not a user is allowed to edit this post.

			``edit_vote``:
				Whether or not a user is allowed to edit their vote on this post.

			``move``:
				Whether or not a user is allowed to move this post to another
				:class:`.Thread`.

			``view``:
				Whether or not a user is allowed to view this post.

			``view_vote``:
				Whether or not a user is allowed to view their votes on this post.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"edit_vote": self.get_instance_action_edit_vote,
			"move": self.get_instance_action_move,
			"view": self.get_instance_action_view,
			"view_vote": self.get_instance_action_view
		}

	def get_instance_action_delete(self: Post, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this post.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.post_delete_own
				) or
				parsed_permissions.post_delete_any
			)
		)

	def get_instance_action_edit(self: Post, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this post.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.post_edit_own
				) or
				parsed_permissions.post_edit_any
			)
		)

	def get_instance_action_edit_vote(self: Post, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit their vote on
		this post.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.

		.. seealso::
			:class:`.PostVote`
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.get_parsed_permissions(user).post_edit_vote
		)

	def get_instance_action_move(
		self: Post,
		user,
		future_thread=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move this post to another
		:class:`.Thread`.

		:param user: The user, a :class:`.User`.
		:param future_thread: The thread posts are about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.thread.user_id == user.id and
					parsed_permissions.post_move_own
				) or
				parsed_permissions.post_move_any
			) and (
				future_thread is None or
				future_thread.instance_actions["move_post"](user)
			)
		)

	def get_instance_action_view(self: Post, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this post.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return self.forum.get_parsed_permissions(user).post_view

	@staticmethod
	def get_static_action_create(
		user,
		thread=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create posts.

		:param user: The user, a :class:`.User`.
		:param thread: The :class:`.Thread` posts would be created in. By default,
			this is :data:`None`, meaning it's not considered.

		:returns: The result of the check.
		"""

		return (
			Post.static_actions["view"](user) and
			user.parsed_permissions["post_create"] and (
				thread is None or
				thread.instance_actions["create_post"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete posts.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Post.static_actions["view"](user) and (
				user.parsed_permissions["post_delete_own"] or
				user.parsed_permissions["post_delete_any"]
			)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit posts.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Post.static_actions["view"](user) and (
				user.parsed_permissions["post_edit_own"] or
				user.parsed_permissions["post_edit_any"]
			)
		)

	@staticmethod
	def get_static_action_edit_vote(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit their votes on posts.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.

		.. seealso::
			:class:`.PostVote`
		"""

		return (
			Post.static_actions["view"](user) and
			user.parsed_permissions["post_edit_vote"]
		)

	@staticmethod
	def get_static_action_move(
		user,
		future_thread=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move posts.

		:param user: The user, a :class:`.User`.
		:param future_thread: The :class:`.Thread` posts would be moved to.
			By default, this is :data:`None`, meaning it's not considered.

		:returns: The result of the check.
		"""

		return (
			Post.static_actions["view"](user) and (
				user.parsed_permissions["post_move_own"] or
				user.parsed_permissions["post_move_any"]
			) and (
				future_thread is None or
				future_thread.instance_actions["move_post"](user)
			)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view posts.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["post_view"]

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which posts ``user`` is
		allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Post.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Post.user_id == user.id,
							ForumParsedPermissions.post_delete_own.is_(True)
						),
						ForumParsedPermissions.post_delete_any.is_(True)
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which posts ``user`` is
		allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Post.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Post.user_id == user.id,
							ForumParsedPermissions.post_edit_own.is_(True)
						),
						ForumParsedPermissions.post_edit_any.is_(True)
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_edit_vote(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which posts ``user`` is
		allowed to edit their votes on.

		:param user: The user, a :class:`.User`.

		:returns: The query.

		.. seealso::
			:class:`.PostVote`
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Post.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.post_edit_vote.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_move(
		user,
		future_thread=None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which posts ``user`` is
		allowed to move to another :class:`.Thread`.

		:param user: The user, a :class:`.User`.
		:param future_thread: The thread posts are about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Post.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Post.user_id == user.id,
							ForumParsedPermissions.post_move_own.is_(True)
						),
						ForumParsedPermissions.post_move_any.is_(True)
					)
				).
				exists()
			),
			(
				True
				if future_thread is None
				else future_thread.action_queries["move_post"]
			)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a selectable condition representing which posts ``user`` is
		allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return (
			sqlalchemy.select(ForumParsedPermissions.forum_id).
			where(ForumParsedPermissions.post_view.is_(True)).
			exists()
		)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"edit_vote": get_static_action_edit_vote,
		"move": get_static_action_move,
		"view": get_static_action_view,
		"view_vote": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all posts, without
	any indication of which post it is.

	``create``:
		Whether or not the user can create posts.

	``delete``:
		Whether or not a user can delete posts.

	``edit``:
		Whether or not a user can edit posts.

	``edit_vote``:
		Whether or not a user can edit their votes on posts.

	``move``:
		Whether or not a user can move posts.

	``view``:
		Whether or not a user is allowed to view posts.

	``view_vote``:
		Whether or not a user is allowed to view their votes on posts.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"edit_vote": get_action_query_edit_vote,
		"move": get_action_query_move,
		"view": get_action_query_view,
		"view_vote": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Group.instance_actions>`.
	"""

	NOTIFICATION_TYPES = (
		enums.NotificationTypes.NEW_POST_FROM_FOLLOWEE,
		enums.NotificationTypes.NEW_POST_IN_SUBSCRIBED_THREAD
	)

	def write(
		self: Post,
		session: sqlalchemy.orm.Session
	) -> None:
		"""Creates a notification about this post for:

			#. Users subscribed to the parent thread.
			#. The author's followers. (Who aren't subscribed to the thread)

		:param session: The session to add the post instance to.
		"""

		from ..notification import Notification
		from ..user import user_follows

		# Premature session add and flush. We have to access the ID later.
		session.add(self)
		session.flush()

		subscriber_ids = self.thread.get_subscriber_ids()

		for subscriber_id in subscriber_ids:
			Notification.create(
				session,
				user_id=subscriber_id,
				type=enums.NotificationTypes.NEW_POST_IN_SUBSCRIBED_THREAD,
				identifier=self.id
			)

		for follower_id in session.execute(
			sqlalchemy.select(user_follows.c.follower_id).
			where(
				sqlalchemy.and_(
					user_follows.c.followee_id == self.user_id,
					~user_follows.c.followee_id.in_(subscriber_ids)
				)
			)
		).scalars().all():
			Notification.create(
				session,
				user_id=follower_id,
				type=enums.NotificationTypes.NEW_POST_FROM_FOLLOWEE,
				identifier=self.id
			)

		return super().write(session)

	def delete(self: Post) -> None:
		r"""Deletes all :class:`.Notification`\ s associated with this post, as
		well as the post itself.
		"""

		from ..notification import Notification

		session = sqlalchemy.orm.object_session(self)

		session.execute(
			sqlalchemy.delete(Notification).
			where(
				sqlalchemy.and_(
					Notification.type.in_(self.NOTIFICATION_TYPES),
					Notification.identifier == self.id
				)
			).
			execution_options(synchronize_session="fetch")
		)

		return super().delete()

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False
	) -> sqlalchemy.sql.Select:
		"""Generates a selection query with permissions already handled.

		Since the posts' :class:`.Thread`'s :class:`.Forum`'s permissions may
		not be parsed, this will always emit additional queries to check.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that posts should fulfill,
			including the context of parsed forum permissions, unlike
			``conditions``. Generally, this will be generated using
			:attr:`action_queries <.Post.action_queries>`. :data:`True` by
			default, meaning there are no additional conditions.
		:param conditions: Any additional conditions, outside a parsed forum
			permission context. :data:`True` by default, meaning there are no
			conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the primary
			key.

		:returns: The query.
		"""

		from ..forum import Forum, ForumParsedPermissions
		from ..thread import Thread

		inner_conditions = sqlalchemy.and_(
			ForumParsedPermissions.forum_id == Thread.forum_id,
			ForumParsedPermissions.user_id == user.id
		)

		while True:
			rows = session.execute(
				sqlalchemy.select(
					cls.id,
					Thread.forum_id,
					(
						sqlalchemy.select(ForumParsedPermissions.forum_id).
						where(inner_conditions).
						exists()
					)
				).
				where(
					sqlalchemy.and_(
						conditions,
						Thread.id == Post.thread_id,
						sqlalchemy.or_(
							~(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(inner_conditions).
								exists()
							),
							(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(
									sqlalchemy.and_(
										inner_conditions,
										cls.action_queries["view"](user),
										action_conditions
									)
								).
								exists()
							)
						)
					)
				).
				order_by(order_by).
				limit(limit).
				offset(offset)
			).all()

			if len(rows) == 0:
				# No need to hit the database with a complicated query twice
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(False)
				)

			post_ids = []
			unparsed_permission_forum_ids = []

			for row in rows:
				(
					post_id,
					forum_id,
					parsed_forum_permissions_exist
				) = row

				if not parsed_forum_permissions_exist:
					unparsed_permission_forum_ids.append(forum_id)

					continue

				post_ids.append(post_id)

			if len(unparsed_permission_forum_ids) != 0:
				for forum in (
					session.execute(
						sqlalchemy.select(Forum).
						where(Forum.id.in_(unparsed_permission_forum_ids))
					).scalars()
				):
					forum.parse_permissions(user)

				session.commit()
			else:
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(cls.id.in_(post_ids)).
					order_by(order_by)
				)


sqlalchemy.event.listen(
	PostVote.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION post_votes_create_delete_edit_update_value()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
					IF (TG_OP = 'UPDATE' AND NEW.is_upvote = OLD.is_upvote) THEN
						RETURN NULL;
					END IF;

					IF NEW.is_upvote IS TRUE THEN
						UPDATE posts
						SET vote_value = vote_value + 1
						WHERE
							id = NEW.post_id AND
							vote_value < {constants.BIG_INTEGER_LIMIT};
					ELSE
						UPDATE posts
						SET vote_value = vote_value - 1
						WHERE id = NEW.post_id;
					END IF;
				ELSIF (TG_OP = 'DELETE') THEN
					IF OLD.is_upvote IS TRUE THEN
						UPDATE posts
						SET vote_value = vote_value - 1
						WHERE id = OLD.post_id;
					ELSE
						UPDATE posts
						SET vote_value = vote_value + 1
						WHERE
							id = OLD.post_id AND
							vote_value < {constants.BIG_INTEGER_LIMIT};
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_edit_update_value
		AFTER INSERT OR DELETE OR UPDATE OF is_upvote
		ON post_votes
		FOR EACH ROW EXECUTE FUNCTION post_votes_create_delete_edit_update_value();
		"""
	).execute_if(dialect="postgresql")
)

sqlalchemy.event.listen(
	Post.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION posts_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE users
					SET post_count = post_count + 1
					WHERE
						id = NEW.user_id AND
						post_count < {constants.BIG_INTEGER_LIMIT};

					UPDATE forums
					SET post_count = post_count + 1
					WHERE
						id = (
							SELECT forum_id
							FROM threads
							WHERE thread_id = NEW.thread_id
						) AND
						post_count < {constants.BIG_INTEGER_LIMIT};

					UPDATE threads
					SET post_count = post_count + 1
					WHERE
						id = NEW.thread_id AND
						post_count < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE users
					SET post_count = post_count - 1
					WHERE id = OLD.user_id;

					UPDATE forums
					SET post_count = post_count - 1
					WHERE
						id = (
							SELECT forum_id
							FROM threads
							WHERE thread_id = OLD.thread_id
						);

					UPDATE threads
					SET post_count = post_count - 1
					WHERE id = OLD.thread_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE OR REPLACE FUNCTION posts_move_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				UPDATE threads
				SET post_count = post_count - 1
				WHERE id = OLD.thread_id;

				UPDATE threads
				SET post_count = post_count + 1
				WHERE
					id = NEW.thread_id AND
					post_count < {constants.BIG_INTEGER_LIMIT};

				IF (NEW.forum_id != OLD.forum_id) THEN
					UPDATE forums
					SET post_count = post_count - 1
					WHERE
						id = (
							SELECT forum_id
							FROM threads
							WHERE thread_id = OLD.thread_id
						);

					UPDATE forums
					SET post_count = post_count + 1
					WHERE
						id = (
							SELECT forum_id
							FROM threads
							WHERE thread_id = NEW.thread_id
						) AND
						post_count < {constants.BIG_INTEGER_LIMIT};
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON posts
		FOR EACH ROW EXECUTE FUNCTION posts_create_delete_update_counters();

		CREATE TRIGGER move_update_counters
		AFTER UPDATE OF thread_id
		ON posts
		FOR EACH ROW EXECUTE PROCEDURE posts_move_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
