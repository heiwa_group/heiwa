"""Utilities for finding differnet models and validating whether or not they
exist.
"""

import typing
import uuid

import flask
import sqlalchemy.orm

from heiwa import database, exceptions

__all__ = [
	"get_category",
	"get_forum",
	"get_group",
	"get_session_and_user_defaults",
	"get_thread",
	"get_user",
	"validate_category_exists",
	"validate_forum_exists",
	"validate_group_exists",
	"validate_thread_exists",
	"validate_user_exists"
]


def get_session_and_user_defaults(
	session: typing.Union[None, sqlalchemy.orm.Session],
	user: typing.Union[None, database.User]
) -> typing.Tuple[sqlalchemy.orm.Session, database.User]:
	"""If ``session`` is :data:`None`, sets it to :attr:`flask.g.sa_session`, the
	standard global session variable. The same happens with ``user`` and
	:attr:`flask.g.user`.

	**While not forbidden, this function is only meant to be used as a shortcut
	in this module's functions and other similar ones. It is not to be used on
	its own**

	:param session: The current SQLAlchemy session.
	:param user: The current user.
	"""

	if session is None:
		session = flask.g.sa_session

	if user is None:
		user = flask.g.user

	return session, user


def get_category(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Category:
	"""Gets the :class:`Category <heiwa.database.Category>` with the given
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category to
		find.
	:param session: The session to find the category with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the category. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category doesn't
		exist, or the user does not have permission to view it.

	:returns: The category.
	"""

	session, user = get_session_and_user_defaults(session, user)

	category = session.execute(
		database.Category.get(
			session,
			user,
			conditions=(database.Category.id == id_)
		)
	).scalars().one_or_none()

	if category is None:
		raise exceptions.APICategoryNotFound

	return category


def get_forum(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Forum:
	"""Gets the :class:`Forum <heiwa.database.Forum>` with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum to find.
	:param session: The session to find the forum with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the forum. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIForumNotFound: Raised when the forum doesn't
		exist, or the user does not have permission to view it.

	:returns: The forum.
	"""

	session, user = get_session_and_user_defaults(session, user)

	forum = session.execute(
		database.Forum.get(
			session,
			user,
			conditions=(database.Forum.id == id_)
		)
	).scalars().one_or_none()

	if forum is None:
		raise exceptions.APIForumNotFound

	return forum


def get_group(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Group:
	"""Gets the :class:`Group <heiwa.database.Group>` with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group to find.
	:param session: The session to find the group with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the group. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIGroupNotFound: Raised when the group doesn't
		exist, or the user does not have permission to view it.

	:returns: The group.
	"""

	session, user = get_session_and_user_defaults(session, user)

	group = session.execute(
		database.Group.get(
			session,
			user,
			conditions=(database.Group.id == id_)
		)
	).scalars().one_or_none()

	if group is None:
		raise exceptions.APIGroupNotFound(id_)

	return group


def get_thread(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Thread:
	"""Gets the :class:`Thread <heiwa.database.Thread>` with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread to find.
	:param session: The session to find the thread with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the thread. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIThreadNotFound: Raised when the thread doesn't
		exist, or the user does not have permission to view it.

	:returns: The thread.
	"""

	session, user = get_session_and_user_defaults(session, user)

	thread = session.execute(
		database.Thread.get(
			session,
			user,
			conditions=(database.Thread.id == id_)
		)
	).scalars().one_or_none()

	if thread is None:
		raise exceptions.APIThreadNotFound(id_)

	return thread


def get_user(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.User:
	"""Gets the :class:`User <heiwa.database.User>` with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user to find.
	:param session: The session to find the user with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The user who must have permission to view the requested user.
		Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIUserNotFound: Raised when the requested user
		doesn't exist, or ``user`` lacks the permission to view them.

	:returns: The user.
	"""

	session, user = get_session_and_user_defaults(session, user)

	user = session.execute(
		database.User.get(
			session,
			user,
			conditions=(database.User.id == id_)
		)
	).scalars().one_or_none()

	if user is None:
		raise exceptions.APIUserNotFound(id_)

	return user


def validate_category_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates that the :class:`Category <heiwa.database.Category>` with the
	given ``id_`` exists.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category to
		find.
	:param session: The session to find the category with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the category. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category doesn't
		exist, or the user does not have permission to view it.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.Category.get(
				session,
				user,
				conditions=(database.Category.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APICategoryNotFound(id_)


def validate_forum_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates that the :class:`Forum <heiwa.database.Forum>` with the given
	ID exists.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum to find.
	:param session: The session to find the forum with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the forum. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIForumNotFound: Raised when the forum doesn't
		exist, or the user does not have permission to view it.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.Forum.get(
				session,
				user,
				conditions=(database.Forum.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APIForumNotFound(id_)


def validate_group_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates that the :class:`Group <heiwa.database.Group>` with the given
	ID exists.

	:param id_: The :attr:`id <heiwa.database.Group.id>` of the group to find.
	:param session: The session to find the group with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the group. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIGroupNotFound: Raised when the group doesn't
		exist, or the user does not have permission to view it.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.Group.get(
				session,
				user,
				conditions=(database.Group.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APIGroupNotFound(id_)


def validate_thread_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates that the :class:`Thread <heiwa.database.Thread>` with the given
	ID exists.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread to find.
	:param session: The session to find the thread with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the thread. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIThreadNotFound: Raised when the thread doesn't
		exist, or the user does not have permission to view it.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.Thread.get(
				session,
				user,
				conditions=(database.Thread.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APIForumNotFound(id_)


def validate_user_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates that the :class:`User <heiwa.database.User>` with the given
	``id_`` exists.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user to find.
	:param session: The session to find the user with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The user who must have permission to view the requested user.
		Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIUserNotFound: Raised when the requested user
		doesn't exist, or ``user`` lacks the permission to view them.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.User.get(
				session,
				user,
				conditions=(database.User.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APIUserNotFound(id_)
