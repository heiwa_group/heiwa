"""Models related to :class:`.Thread` votes."""

from __future__ import annotations

import flask
import sqlalchemy

from .. import Base, utils

__all__ = ["ThreadVote"]


class ThreadVote(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	r"""A model for storing votes on :class:`.Thread`\ s.

	At a given time, only one instance of this model can exist for a specific
	thread and user combination.
	"""

	__tablename__ = "thread_votes"

	thread_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"threads.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Thread.id>` of the :class:`.Thread` a vote belongs to."""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who created a vote."""

	is_upvote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		index=True,
		nullable=False
	)
	"""Whether or not a vote is an upvote, or a downvote, if :data:`True`, it's
	considered an upvote, otherwise, it's considered a downvote.

	.. seealso::
		:attr:`.Thread.vote_value`
	"""

	def get_id_url(self: ThreadVote) -> str:
		"""Gets the JSON-LD ``@id`` route to this thread vote.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"thread.vote.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: ThreadVote) -> None:
		r"""Adds the :attr:`instance_actions <.ThreadVote.instance_actions>`
		attribute, containing possible actions :class:`.User`\ s are allowed to
		perform on votes:

			``delete``:
				Whether or not a user can delete this vote.

			``edit``:
				Whether or not a user can edit this vote.

			``view``:
				Whether or not a user can view this vote.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: ThreadVote, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this vote.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.thread.instance_actions["edit_vote"](user)
		)

	def get_instance_action_edit(self: ThreadVote, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this vote.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.thread.instance_actions["edit_vote"](user)
		)

	def get_instance_action_view(self: ThreadVote, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this vote.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.user_id == user.id and
			self.thread.instance_actions["view_vote"](user)
		)

	@staticmethod
	def get_static_action_create(
		user,
		thread=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create votes.

		:param user: The user, a :class:`.User`.
		:param thread: The thread to assign the vote to. :data:`None` by default,
			meaning this is not considered.

		:returns: The result of the check.
		"""

		from . import Thread

		return (
			ThreadVote.static_actions["view"](user) and
			Thread.static_actions["edit_vote"](user) and (
				thread is None or
				thread.instance_actions["edit_vote"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete votes.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Thread

		return (
			ThreadVote.static_actions["view"] and
			Thread.static_actions["edit_vote"](user)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit votes.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Thread

		return (
			ThreadVote.static_actions["view"] and
			Thread.static_actions["edit_vote"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view votes.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Thread

		return Thread.static_actions["view_vote"](user)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which votes ``user`` is
		allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Thread

		return sqlalchemy.and_(
			ThreadVote.action_queries["view"](user),
			Thread.action_queries["edit_vote"](user)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which votes ``user`` is
		allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Thread

		return sqlalchemy.and_(
			ThreadVote.action_queries["view"](user),
			Thread.action_queries["edit_vote"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which votes ``user`` is
		allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Thread

		return sqlalchemy.and_(
			ThreadVote.user_id == user.id,
			Thread.action_queries["view_vote"](user)
		)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any vote, without any
	indication of which one it is.

	``create``:
		Whether or not a user can create votes.

	``delete``:
		Whether or not a user can delete votes.

	``edit``:
		Whether or not a user can edit votes.

	``view``:
		Whether or not a user can view votes.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.ThreadVote.instance_actions>`.
	"""
