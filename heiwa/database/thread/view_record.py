"""Tables related to :class:`.Thread` views."""

import sqlalchemy

from .. import Base, utils

__all__ = ["ThreadViewRecord"]


class ThreadViewRecord(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	Base
):
	"""A model for storing :class:`.Thread` views.

	Instances of this model should be deleted after a short cooldown, about
	one hour is recommended. This is to protect the privacy of users who
	choose to send information about them viewing a certain thread, while
	also preventing basic spam.

	.. note::
		The creation of these view records doesn't have its own permission (like
		``create_view``), their only purpose is to aid with the counting of views
		on threads.

	.. warning::
		Instances of this model should never be directly editable or visible,
		but generated automatically when a user claims to have viewed a certain
		thread and used to help with storing the total view count.
	"""

	__tablename__ = "thread_view_records"

	thread_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"threads.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Thread.id>` of a :class:`.Thread` which has been
	viewed.
	"""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who viewed a thread."""
