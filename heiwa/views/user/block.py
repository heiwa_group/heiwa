r"""API views for
:obj:`user_blocks <heiwa.database.user_blocks>`, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import get_user, requires_permission, validate_permission

__all__ = [
	"create",
	"delete",
	"user_block_blueprint",
	"view"
]

user_block_blueprint = flask.Blueprint(
	"block",
	__name__
)


@user_block_blueprint.route("/users/<uuid:id_>/block", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_block", database.User)
def create(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Creates a block from the current :attr:`flask.g.user` for the user with
	the given ``id_``.

	If the current user is also following this user, the follow is automatically
	removed.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIUserBlockAlreadyExists: Raised when the current
		user has already blocked this user before.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"edit_block",
		user
	)

	if flask.g.user.get_is_blockee(user.id):
		raise exceptions.APIUserBlockAlreadyExists

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.user_follows).
		where(
			sqlalchemy.and_(
				database.user_follows.c.follower_id == flask.g.user.id,
				database.user_follows.c.followee_id == user.id
			)
		)
	)

	flask.g.sa_session.execute(
		sqlalchemy.insert(database.user_blocks).
		values(
			blocker_id=flask.g.user.id,
			blockee_id=user.id
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_block_blueprint.route("/users/<uuid:id_>/block", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view_block", database.User)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets whether or not the current :attr:`flask.g.user` has blocked the user
	with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:returns: The boolean result, with the ``200`` HTTP status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"view_block",
		user
	)

	return flask.jsonify(
		flask.g.user.get_is_blockee(user.id)
	), http.client.OK


@user_block_blueprint.route("/users/<uuid:id_>/block", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_block", database.User)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Removes the current :attr:`flask.g.user`'s block for the user with the
	given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIUserBlockNotFound: Raised when the current user
		has not blocked this user, thus there is nothing to remove.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"edit_block",
		user
	)

	existing_block = flask.g.sa_session.execute(
		sqlalchemy.select(database.user_blocks).
		where(
			sqlalchemy.and_(
				database.user_blocks.c.blocker_id == flask.g.user.id,
				database.user_blocks.c.blockee_id == user.id
			)
		)
	).scalars().one_or_none()

	if existing_block is None:
		raise exceptions.APIUserBlockNotFound

	flask.g.sa_session.delete(existing_block)
	return flask.jsonify(None), http.client.NO_CONTENT
