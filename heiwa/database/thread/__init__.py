"""Models and tables for threads."""

from __future__ import annotations

import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants, enums

from .. import Base, utils
from .subscribers import thread_subscribers
from .view_record import ThreadViewRecord
from .vote import ThreadVote

__all__ = [
	"Thread",
	"ThreadViewRecord",
	"ThreadVote",
	"thread_subscribers"
]
__version__ = "1.2.0"


class Thread(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Thread model."""

	__tablename__ = "threads"

	forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.Forum.id>` of the :class:`.Forum` a thread is in. This
	can be changed after the thread has been created, as long as the user
	attempting to do so has sufficient permissions.
	"""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who created a thread."""

	is_approved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	"""Whether or not a thread has been approved. If :data:`False`, a user with
	the permission to must approve it in order for it to be shown by default on
	frontend programs. However, users should still be able to choose to see
	unapproved threads, as the API is not preventing them from doing so.
	"""

	is_locked = sqlalchemy.Column(
		sqlalchemy.Boolean,
		default=False,
		nullable=False
	)
	"""Whether or not a thread has been locked. :data:`False` by default. If
	:data:`True`, creating any new posts in the thread will be disabled for
	everyone, including administrators.
	"""

	is_pinned = sqlalchemy.Column(
		sqlalchemy.Boolean,
		default=False,
		nullable=False
	)
	"""Whether or not a thread is pinned. :data:`False` by default. If
	:data:`True`, the thread should float to the top of lists on frontend
	programs and be marked as such, or even appear in a separate section.
	This, however, is not mandatory and API endpoints for listing threads will
	not apply this order by default.
	"""

	tags = sqlalchemy.Column(
		sqlalchemy.ARRAY(sqlalchemy.String(128)),
		nullable=False
	)
	"""A thread's tags. For example, when a thread is related to tech support,
	the tags can be ``tech support`` and ``help requested``.
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""A thread's name."""

	content = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=False
	)
	"""A thread's content. Generally, this will often be shown as the first post
	in a thread.
	"""

	vote_value = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The final value of a thread's votes. Upvotes will add ``1``, downvotes
	will subtract ``1``. If there are no votes at all, this will be ``0``.
	Negative numbers are allowed.

	.. seealso::
		:class:`.ThreadVote`
	"""

	post_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The amount of :class:`.Post`\ s in a thread."""

	subscriber_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The amount of :class:`.User`\ s who have subscribed to a thread.

	.. seealso::
		:obj:`.thread_subscribers`
	"""

	view_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The amount of views this thread has. This isn't limited to a single view
	per user, but a cooldown should be imposed.

	.. seealso::
		:class:`.ThreadViewRecord`
	"""

	last_post_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		nullable=True
	)
	"""The time the latest :class:`.Post` in a thread was made. If there haven't
	been any posts so far, this will be :data:`None`.
	"""

	forum = sqlalchemy.orm.relationship(
		"Forum",
		uselist=False,
		passive_deletes="all",
		lazy=True
	)
	"""The :class:`.Forum` a thread is in."""

	votes = sqlalchemy.orm.relationship(
		ThreadVote,
		backref=sqlalchemy.orm.backref(
			"thread",
			uselist=False
		),
		order_by=sqlalchemy.desc(ThreadVote.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""The votes that have been made on a thread.

	.. seealso::
		:class:`.ThreadVote`
	"""

	user = sqlalchemy.orm.relationship(
		"User",
		order_by="desc(User.creation_timestamp)",
		passive_deletes="all",
		lazy=True
	)
	"""The :class:`.User` who created a thread."""

	def get_id_url(self: Thread) -> str:
		"""Gets the JSON-LD ``@id`` route to this thread.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"thread.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Thread) -> None:
		r"""Adds the :attr:`instance_actions <.Thread.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific threads:

			``create_post``:
				Whether or not a user can create posts this thread.

			``delete``:
				Whether or not a user can delete this thread.

			``edit``:
				Whether or not a user can edit this thread.

			``edit_subscription``:
				Whether or not a user can subscribe to / unsubscribe from this thread.

			``edit_vote``:
				Whether or not a user can vote on this thread.

			``merge``:
				Whether or not a user can merge this thread with another thread.

			``move``:
				Whether or not a user can move this thread to another :class:`.Forum`.

			``move_post``:
				Whether or not a user is allowed to move posts to this forum.

			``view``:
				Whether or not a user is allowed to view this thread.

			``view_vote``:
				Whether or not a user is allowed to view their votes on this thread.

			``view_vote``:
				Whether or not a user is allowed to view their subscription to this
				thread.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"create_post": self.get_instance_action_create_post,
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"edit_subscription": self.get_instance_action_view,
			"edit_vote": self.get_instance_action_edit_vote,
			"merge": self.get_instance_action_merge,
			"move": self.get_instance_action_move,
			"move_post": self.get_instance_action_create_post,
			"view": self.get_instance_action_view,
			"view_vote": self.get_instance_action_view,
			"view_subscription": self.get_instance_action_view
		}

	def get_instance_action_create_post(self: Thread, user) -> bool:
		"""Checks whether or not ``user`` is allowed to create a :class:`.Post`
		in this thread.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and
			parsed_permissions.post_view and
			parsed_permissions.post_create
		)

	def get_instance_action_delete(self: Thread, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this thread.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.thread_delete_own
				) or
				parsed_permissions.thread_delete_any
			)
		)

	def get_instance_action_edit(
		self: Thread,
		user,
		is_approved: typing.Union[None, bool] = None,
		is_locked: typing.Union[None, bool] = None,
		is_pinned: typing.Union[None, bool] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this thread.

		:param user: The user, a :class:`.User`.
		:param is_approved: Whether or not this thread should be approved.
			:data:`None` by default, meaning this will be disregarded.
		:param is_locked: Whether or not this thread should be locked. :data:`None`
			by default, meaning this will be disregarded.
		:param is_pinned: Whether or not this thread should be pinned. :data:`None`
			by default, meaning this will be disregarded.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.thread_edit_own
				) or
				parsed_permissions.thread_edit_any
			) and (
				is_approved is None or
				self.is_approved is is_approved or
				parsed_permissions.thread_edit_approval
			) and (
				is_locked is None or
				self.is_locked is is_locked or (
					(
						self.user_id == user.id and
						parsed_permissions.thread_edit_lock_own
					) or
					parsed_permissions.thread_edit_lock_any
				)
			) and (
				is_pinned is None or
				self.is_pinned is is_pinned or
				parsed_permissions.thread_edit_pin
			)
		)

	def get_instance_action_edit_vote(self: Thread, user) -> bool:
		"""Checks whether or not ``user`` is edit their vote on this thread.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.get_parsed_permissions(user).thread_edit_vote
		)

	def get_instance_action_merge(
		self: Thread,
		user,
		future_thread: typing.Union[None, Thread] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to merge this thread with
		another thread.

		:param user: The user, a :class:`.User`.
		:param future_thread: The thread this thread is about to be merged with. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.thread_merge_own
				) or
				parsed_permissions.thread_merge_any
			) and (
				future_thread is None or
				future_thread.instance_actions["merge"](user)
			)
		)

	def get_instance_action_move(
		self: Thread,
		user,
		future_forum=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move this thread to
		another :class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum this thread is about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.
		"""

		parsed_permissions = self.forum.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and (
				(
					self.user_id == user.id and
					parsed_permissions.thread_move_own
				) or
				parsed_permissions.thread_move_any
			) and (
				future_forum is None or (
					(
						self.is_approved and
						future_forum.instance_actions["move_thread_approved"](user)
					) or (
						not self.is_approved and
						future_forum.instance_actions["move_thread_unapproved"](user)
					)
				)
			)
		)

	def get_instance_action_view(self: Thread, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this thread.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return self.forum.get_parsed_permissions(user).thread_view

	@staticmethod
	def get_static_action_create(
		user,
		forum=None,
		is_approved: typing.Union[None, bool] = None,
		is_locked: bool = False,
		is_pinned: bool = False
	) -> bool:
		"""Checks whether or not the ``user`` is allowed to create threads.

		:param user: The user, a :class:`.User`.
		:param forum: The :class:`.Forum` the thread should be created in.
		:param is_approved: Whether or not the thread is supposed to be approved.
			:data:`None` by default, meaning it can be either.
		:param is_locked: Whether or not the thread should be locked. :data:`False`
			by default.
		:param is_pinned: Whether or not the thread should be pinned. :data:`False`
			by default.

		:returns: The result of the check.
		"""

		return (
			Thread.static_actions["view"](user) and (
				(
					is_approved is None and (
						user.parsed_permissions["thread_create_approved"] or
						user.parsed_permissions["thread_create_unapproved"]
					)
				) or (
					is_approved and
					user.parsed_permissions["thread_create_approved"]
				) or (
					not is_approved and
					user.parsed_permissions["thread_create_unapproved"]
				)
			) and (
				not is_locked or
				user.parsed_permissions["thread_edit_pin"]
			) and (
				not is_pinned or
				user.parsed_permissions["thread_edit_lock_own"] or
				user.parsed_permissions["thread_edit_lock_any"]
			) and (
				forum is None or
				forum.instance_actions["create_thread"](
					user,
					is_approved=is_approved,
					is_locked=is_locked,
					is_pinned=is_pinned
				)
			)
		)

	@staticmethod
	def get_static_action_create_post(user) -> bool:
		r"""Checks whether or not the ``user`` is allowed to create
		:class:`.Post`\ s without knowledge of which thread it'll be done in.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from ..post import Post

		return (
			Thread.static_actions["view"](user) and
			Post.static_actions["create"](user)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not the ``user`` is allowed to delete threads.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Thread.static_actions["view"](user) and (
				user.parsed_permissions["thread_delete_own"] or
				user.parsed_permissions["thread_delete_any"]
			)
		)

	@staticmethod
	def get_static_action_edit(
		user,
		approval=False,
		lock=False,
		pin=False
	) -> bool:
		"""Checks whether or not the ``user`` is allowed to edit threads.

		:param user: The user, a :class:`.User`.
		:param approval: Whether or not the user is editing the thread's
			:attr:`is_approved <.Thread.is_approved>` status.
		:param lock: Whether or not the user is editing the thread's
			:attr:`is_locked <.Thread.is_locked>` status.
		:param pin: Whether or not the user is editing the thread's
			:attr:`is_pinned <.Thread.is_pinned>` status.

		:returns: The result of the check.
		"""

		return (
			Thread.static_actions["view"](user) and (
				user.parsed_permissions["thread_edit_own"] or
				user.parsed_permissions["thread_edit_any"]
			) and (
				not approval or
				user.parsed_permissions["thread_edit_approval"]
			) and (
				not pin or
				user.parsed_permissions["thread_edit_pin"]
			) and (
				not lock or
				user.parsed_permissions["thread_edit_lock_own"] or
				user.parsed_permissions["thread_edit_lock_any"]
			)
		)

	@staticmethod
	def get_static_action_edit_vote(user) -> bool:
		"""Checks whether or not the ``user`` is allowed to edit their votes on
		threads.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.

		.. seealso::
			:class:`.ThreadVote`
		"""

		return (
			Thread.static_actions["view"](user) and
			user.parsed_permissions["thread_edit_vote"]
		)

	@staticmethod
	def get_static_action_merge(
		user,
		future_thread: typing.Union[None, Thread] = None
	) -> bool:
		"""Checks whether or not the ``user`` is allowed to merge threads with
		another thread.

		:param user: The user, a :class:`.User`.
		:param future_thread: The thread other threads are going to be merged
			with. :data:`None` by default, meaning this is not considered.

		:returns: The result of the check.
		"""

		return (
			Thread.static_actions["view"](user) and (
				user.parsed_permissions["thread_merge_own"] or
				user.parsed_permissions["thread_merge_any"]
			) and (
				future_thread is None or
				future_thread.instance_actions["merge"](user)
			)
		)

	@staticmethod
	def get_static_action_move(
		user,
		future_forum=None
	) -> bool:
		"""Checks whether or not the ``user`` is allowed to move threads to another
		:class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum threads should be moved to. :data:`None`
			by default, meaning this is not considered.

		:returns: The result of the check.
		"""

		return (
			Thread.static_actions["view"](user) and (
				user.parsed_permissions["thread_move_own"] or
				user.parsed_permissions["thread_move_any"]
			) and (
				future_forum is None or
				future_forum.instance_actions["move_thread"](user)
			)
		)

	@staticmethod
	def get_static_action_move_post(user) -> bool:
		r"""Checks whether or not the ``user`` is allowed to create move
		:class:`.Post`\s to threads, without knowledge of which thread it'll
		be.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from ..post import Post

		return (
			Thread.static_actions["view"](user) and
			Post.static_actions["move"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not the ``user`` is allowed to view threads.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["thread_view"]

	@staticmethod
	def get_action_query_create_post(user) -> sqlalchemy.sql.elements.ClauseList:
		r"""Generates a selectable condition representing which threads ``user`` is
		allowed to create :class:`.Post`\ s in.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.and_(
						ForumParsedPermissions.post_view.is_(True),
						ForumParsedPermissions.post_create.is_(True)
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Thread.user_id == user.id,
							ForumParsedPermissions.thread_delete_own.is_(True)
						),
						ForumParsedPermissions.thread_delete_any.is_(True)
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_edit(
		user,
		is_approved: typing.Union[None, bool] = None,
		is_locked: typing.Union[None, bool] = None,
		is_pinned: typing.Union[None, bool] = None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to edit.

		:param user: The user, a :class:`.User`.
		:param is_approved: Whether or not the thread should be approved.
			:data:`None` by default, meaning this will be disregarded.
		:param is_locked: Whether or not the thread should be locked. :data:`None`
			by default, meaning this will be disregarded.
		:param is_pinned: Whether or not the thread should be pinned. :data:`None`
			by default, meaning this will be disregarded.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Thread.user_id == user.id,
							ForumParsedPermissions.thread_edit_own.is_(True)
						),
						ForumParsedPermissions.thread_edit_any.is_(True)
					),
					(
						sqlalchemy.or_(
							Thread.is_approved.is_(is_approved),
							ForumParsedPermissions.thread_edit_approval.is_(True)
						)
						if is_approved is not None
						else True
					),
					(
						sqlalchemy.or_(
							Thread.is_pinned.is_(is_pinned),
							ForumParsedPermissions.thread_edit_pin.is_(True)
						)
						if is_pinned is not None
						else True
					),
					(
						sqlalchemy.or_(
							Thread.is_locked.is_(is_locked),
							sqlalchemy.or_(
								sqlalchemy.and_(
									Thread.user_id == user.id,
									ForumParsedPermissions.thread_edit_lock_own.is_(True)
								),
								ForumParsedPermissions.thread_edit_lock_any.is_(True)
							)
						)
						if is_pinned is not None
						else True
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_edit_vote(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to edit their votes on.

		:param user: The user, a :class:`.User`.

		:returns: The query.

		.. seealso::
			:class:`.ThreadVote`
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.thread_edit_vote.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_merge(
		user,
		future_thread: typing.Union[Thread, None] = None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to merge with another thread.

		:param user: The user, a :class:`.User`.
		:param future_thread: The thread other threads are about to be merged with.
			By default, this is :data:`None`, meaning it's ignored.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Thread.user_id == user.id,
							ForumParsedPermissions.thread_merge_own.is_(True)
						),
						ForumParsedPermissions.thread_merge_any.is_(True)
					)
				).
				exists()
			),
			(
				True
				if future_thread is None
				else future_thread.action_queries["merge"](user)
			)
		)

	@staticmethod
	def get_action_query_move(
		user,
		future_forum=None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to move to another :class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum threads are about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Thread.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.or_(
						sqlalchemy.and_(
							Thread.user_id == user.id,
							ForumParsedPermissions.thread_move_own.is_(True)
						),
						ForumParsedPermissions.thread_move_any.is_(True)
					)
				).
				exists()
			),
			sqlalchemy.or_(
				sqlalchemy.and_(
					Thread.is_locked.is_(True),
					future_forum.action_queries["move_thread_approved"](user)
				),
				sqlalchemy.and_(
					Thread.is_locked.is_(False),
					future_forum.action_queries["move_thread_unapproved"](user)
				)
			)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a selectable condition representing which threads ``user`` is
		allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return (
			sqlalchemy.select(ForumParsedPermissions.forum_id).
			where(ForumParsedPermissions.thread_view.is_(True)).
			exists()
		)

	static_actions = {
		"create": get_static_action_create,
		"create_post": get_static_action_create_post,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"edit_subscription": get_static_action_view,
		"edit_vote": get_static_action_edit_vote,
		"merge": get_static_action_merge,
		"move": get_static_action_move,
		"move_post": get_static_action_move_post,
		"view": get_static_action_view,
		"view_vote": get_static_action_view,
		"view_subscription": get_static_action_view
	}
	r"""Actions :class:`.User`\ s are allowed to perform on all threads, without
	any indication of which thread it is.

	``create``:
		Whether or not a user can create threads.

	``create_post``:
		Whether or not a user can create posts.

	``delete``:
		Whether or not a user can delete threads.

	``edit``:
		Whether or not a user can edit threads.

	``edit_subscription``:
		Whether or not a user can subscribe to / unsubscribe from threads.

	``edit_vote``:
		Whether or not a user can vote on threads.

	``merge``:
		Whether or not a user can merge threads with another thread.

	``move``:
		Whether or not a user can move threads to another forum.

	``move_post``:
		Whether or not a user is allowed to move posts.

	``view``:
		Whether or not a user can view threads.

	``view_vote``:
		Whether or not a user can view their votes for threads.

	``view_subscription``:
		Whether or not a user can view their subscriptions to threads.
	"""

	action_queries = {
		"create_post": get_action_query_create_post,
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"edit_subscription": get_action_query_view,
		"edit_vote": get_action_query_edit_vote,
		"merge": get_action_query_merge,
		"move": get_action_query_move,
		"view": get_action_query_view,
		"view_vote": get_action_query_view,
		"view_subscription": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Group.instance_actions>`.
	"""

	NOTIFICATION_TYPES = (
		enums.NotificationTypes.NEW_THREAD_FROM_FOLLOWEE,
		enums.NotificationTypes.NEW_THREAD_IN_SUBSCRIBED_FORUM
	)
	r"""The types of :class:`.Notification`\ s that can refer to threads."""

	def write(
		self: Thread,
		session: sqlalchemy.orm.Session
	) -> None:
		r"""Creates a :class:`.Notification` about this thread for:

			#. :class:`.User`\ s subscribed to the :class:`.Forum` this thread is in.
			#. The author's followers, as long as they aren't also subscribed to
			   the forum.

		Then adds the newly created object to the ``session``.

		:param session: The session to add the thread instance to.
		"""

		from ..notification import Notification
		from ..user import user_follows

		# Premature session add and flush. We have to access the ID later.
		session.add(self)
		session.flush()

		subscriber_ids = self.forum.get_subscriber_ids()

		for subscriber_id in subscriber_ids:
			Notification.create(
				session,
				user_id=subscriber_id,
				type=enums.NotificationTypes.NEW_THREAD_IN_SUBSCRIBED_FORUM,
				identifier=self.id
			)

		for follower_id in session.execute(
			sqlalchemy.select(user_follows.c.follower_id).
			where(
				sqlalchemy.and_(
					user_follows.c.followee_id == self.user_id,
					~user_follows.c.followee_id.in_(subscriber_ids)
				)
			)
		).scalars().all():
			Notification.create(
				session,
				user_id=follower_id,
				type=enums.NotificationTypes.NEW_THREAD_FROM_FOLLOWEE,
				identifier=self.id
			)

		return super().write(session)

	def delete(self: Thread) -> None:
		r"""Deletes all :class:`.Notification`\ s associated with this thread,
		as well as the thread itself.
		"""

		from ..notification import Notification
		from ..post import Post

		session = sqlalchemy.orm.object_session(self)

		session.execute(
			sqlalchemy.delete(Notification).
			where(
				sqlalchemy.or_(
					sqlalchemy.and_(
						Notification.type.in_(self.NOTIFICATION_TYPES),
						Notification.identifier == self.id
					),
					sqlalchemy.and_(
						Notification.type.in_(Post.NOTIFICATION_TYPES),
						Notification.identifier.in_(
							sqlalchemy.select(Post.id).
							where(Post.thread_id == self.id)
						)
					)
				)
			).
			execution_options(synchronize_session="fetch")
		)

		return super().delete()

	def get_is_subscribed(self: Thread, user_id: uuid.UUID) -> bool:
		"""Checks whether or not the :class:`.User` with the given ``user_id``
		is subscribed to this thread.

		:param user_id: The :attr:`id <.User.id>` of the user.

		:returns: The result of the check.
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(thread_subscribers.c.thread_id).
			where(
				sqlalchemy.and_(
					thread_subscribers.c.thread_id == self.id,
					thread_subscribers.c.user_id == user_id
				)
			).
			exists().
			select()
		).scalars().one()

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False
	) -> sqlalchemy.sql.Select:
		"""Generates a selection query with permissions already handled.

		Since threads' :class:`.Forum`'s permissions may not be parsed, this will
		always emit additional queries to check.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that threads should
			fulfill, including the context of parsed forum permissions, unlike
			``conditions``. Generally, this will be generated using
			:attr:`action_queries <.Thread.action_queries>`. :data:`True` by
			default, meaning there are no additional conditions.
		:param conditions: Any additional conditions, outside a parsed forum
			permission context. :data:`True` by default, meaning there are no
			conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the primary
			key.

		:returns: The query.
		"""

		from ..forum import Forum, ForumParsedPermissions

		inner_conditions = sqlalchemy.and_(
			ForumParsedPermissions.forum_id == cls.forum_id,
			ForumParsedPermissions.user_id == user.id
		)

		while True:
			rows = session.execute(
				sqlalchemy.select(
					cls.id,
					cls.forum_id,
					(
						sqlalchemy.select(ForumParsedPermissions.forum_id).
						where(inner_conditions).
						exists()
					)
				).
				where(
					sqlalchemy.and_(
						conditions,
						sqlalchemy.or_(
							~(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(inner_conditions).
								exists()
							),
							(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(
									sqlalchemy.and_(
										inner_conditions,
										cls.action_queries["view"](user),
										action_conditions
									)
								).
								exists()
							)
						)
					)
				).
				order_by(order_by).
				limit(limit).
				offset(offset)
			).all()

			if len(rows) == 0:
				# No need to hit the database with a complicated query twice
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(False)
				)

			thread_ids = []
			unparsed_permission_forum_ids = []

			for row in rows:
				(
					thread_id,
					forum_id,
					parsed_permissions_exist
				) = row

				if not parsed_permissions_exist:
					unparsed_permission_forum_ids.append(forum_id)

					continue

				thread_ids.append(thread_id)

			if len(unparsed_permission_forum_ids) != 0:
				for forum in (
					session.execute(
						sqlalchemy.select(Forum).
						where(Forum.id.in_(unparsed_permission_forum_ids))
					).scalars()
				):
					forum.parse_permissions(user)

				session.commit()
			else:
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(cls.id.in_(thread_ids)).
					order_by(order_by)
				)

	def get_subscriber_ids(self: Thread) -> typing.List[uuid.UUID]:
		r"""Gets this thread's subscribers' :attr:`id <.User.id>`\ s.

		:returns: The subscribers' IDs.

		.. seealso::
			:obj:`.thread_subscribers`
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(thread_subscribers.c.user_id).
			where(thread_subscribers.c.thread_id == self.id)
		).scalars().all()


sqlalchemy.event.listen(
	ThreadViewRecord.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION thread_view_records_create_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				UPDATE threads
				SET view_count = view_count + 1
				WHERE
					id = NEW.thread_id AND
					view_count < {constants.BIG_INTEGER_LIMIT};

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_update_counters
		AFTER INSERT
		ON thread_view_records
		FOR EACH ROW EXECUTE FUNCTION thread_view_records_create_update_counters();
		"""
	).execute_if(dialect="postgresql")
)

sqlalchemy.event.listen(
	ThreadVote.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION thread_votes_create_delete_edit_update_value()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
					IF (TG_OP = 'UPDATE' AND NEW.is_upvote = OLD.is_upvote) THEN
						RETURN NULL;
					END IF;

					IF NEW.is_upvote IS TRUE THEN
						UPDATE threads
						SET vote_value = vote_value + 1
						WHERE
							id = NEW.thread_id AND
							vote_value < {constants.BIG_INTEGER_LIMIT};
					ELSE
						UPDATE threads
						SET vote_value = vote_value - 1
						WHERE id = NEW.thread_id;
					END IF;
				ELSIF (TG_OP = 'DELETE') THEN
					IF OLD.is_upvote IS TRUE THEN
						UPDATE threads
						SET vote_value = vote_value - 1
						WHERE id = OLD.thread_id;
					ELSE
						UPDATE threads
						SET vote_value = vote_value + 1
						WHERE
							id = OLD.thread_id AND
							vote_value < {constants.BIG_INTEGER_LIMIT};
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_edit_update_value
		AFTER INSERT OR DELETE OR UPDATE OF is_upvote
		ON thread_votes
		FOR EACH ROW EXECUTE FUNCTION thread_votes_create_delete_edit_update_value();
		"""
	).execute_if(dialect="postgresql")
)

sqlalchemy.event.listen(
	thread_subscribers,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION thread_subscribers_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE threads
					SET subscriber_count = subscriber_count + 1
					WHERE
						id = NEW.thread_id AND
						subscriber_count < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE threads
					SET subscriber_count = subscriber_count - 1
					WHERE id = OLD.thread_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON thread_subscribers
		FOR EACH ROW
		EXECUTE FUNCTION thread_subscribers_create_delete_update_counters();
		"""
	).execute_if(dialect="postgresql")
)

sqlalchemy.event.listen(
	Thread.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION threads_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE users
					SET thread_count = thread_count + 1
					WHERE
						id = NEW.user_id AND
						thread_count < {constants.BIG_INTEGER_LIMIT};

					UPDATE forums
					SET thread_count = thread_count + 1
					WHERE
						id = NEW.forum_id AND
						thread_count < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE users
					SET thread_count = thread_count - 1
					WHERE id = OLD.user_id;

					UPDATE forums
					SET thread_count = thread_count - 1
					WHERE id = OLD.forum_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE OR REPLACE FUNCTION threads_move_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				UPDATE forums
				SET thread_count = thread_count - 1
				WHERE id = OLD.forum_id;

				UPDATE forums
				SET thread_count = thread_count + 1
				WHERE
					id = NEW.forum_id AND
					thread_count < {constants.BIG_INTEGER_LIMIT};
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON threads
		FOR EACH ROW
		EXECUTE FUNCTION threads_create_delete_update_counters();

		CREATE TRIGGER move_update_counters
		AFTER UPDATE OF forum_id
		ON threads
		FOR EACH ROW
		EXECUTE FUNCTION threads_move_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
