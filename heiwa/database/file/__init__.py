"""Models for uploaded files."""

from __future__ import annotations

import mimetypes
import os
import typing

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants

from .. import Base, utils

__all__ = ["File"]
__version__ = "1.0.1"


class File(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Uploaded file model."""

	__tablename__ = "files"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who uploaded a file."""

	type = sqlalchemy.Column(
		sqlalchemy.String(255),
		nullable=False
	)
	r"""The media type of this file.

	.. seealso::
		IANA's `list <https://www.iana.org/assignments/media-types/media-types\
		.xhtml>`_ of officially recognized media types.

		`RFC 4288 - maximum length <https://datatracker.ietf.org/doc/html/rfc4\
		288#section-4.2>`_

	.. note::
		Media types were formerly recognized as MIME types and are still
		commonly referred to as such.
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(63),
		nullable=False
	)
	"""The name of a file.

	Limited to 63 characters, as ext4, a common small-scale Linux server
	filesystem, generally supports up to 255 bytes. UTF-8 characters can go up
	to 4 bytes and the the closest whole number to 255/8 is 31.
	"""

	size = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		nullable=False
	)
	"""The size of a file, in bytes."""

	def get_id_url(self: File) -> str:
		"""Gets the JSON-LD ``@id`` route to this file.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"file.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: File) -> None:
		r"""Adds the :attr:`instance_actions <.File.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific files:

			``delete``:
				Whether or not a user can delete this file.

			``edit``:
				Whether or not a user can edit this file.

			``view``:
				Whether or not a user can view this file.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete_edit,
			"edit": self.get_instance_action_delete_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete_edit(
		self: File,
		user
	) -> bool:
		"""Checks whether or not ``user`` is allowed to delete or edit files.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["file_use"]
		)

	def get_instance_action_view(self: File, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this file.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_static_action_create_delete_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to create, delete or edit
		files.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			File.static_actions["view"](user) and
			user.parsed_permissions["file_use"]
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view files.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_action_query_delete_edit(user) -> sqlalchemy.sql.elements.AsBoolean:
		"""Generates a selectable condition representing which files ``user``
		is allowed to delete or edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			File.action_queries["view"](user),
			user.parsed_permissions["file_use"]
		)

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which files ``user``
		is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return True

	static_actions = {
		"create": get_static_action_create_delete_edit,
		"delete": get_static_action_create_delete_edit,
		"edit": get_static_action_create_delete_edit,
		"view": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all files, without
	any indication of which thread it is.

	``create``:
		Whether or not a user can create files.

	``delete``:
		Whether or not a user can delete files.

	``edit``:
		Whether or not a user can edit files.

	``view``:
		Whether or not a user can view files.
	"""

	action_queries = {
		"delete": get_action_query_delete_edit,
		"edit": get_action_query_delete_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.File.instance_actions>`.
	"""

	def write(
		self: File,
		session: sqlalchemy.orm.Session,
		contents: typing.Union[None, bytes] = None,
		contents_location: typing.Union[None, str] = None
	) -> None:
		"""Sets this file's filesystem contents to the given ``contents``. Unlike
		the :meth:`.User.write` method, these arguments are not optional, since
		all files must have contents.

		:param session: The session to add the new instance to.
		:param contents: The file's contents, in bytes. If :data:`None`, the
			contents are not set.
		:param contents_location: The filesystem location where files are stored.
			If :data:`None`, the behavior is the same as in
			:meth:`set_contents <.File.set_contents>`.
		"""

		if contents is not None:
			# Premature session add and flush. We have to access the ID when
			# writing contents to the filesystem.

			session.add(self)
			session.flush()

			self.set_contents(
				contents,
				contents_location
			)

		return super().write(session)

	def delete(
		self: File,
		contents_location: typing.Union[None, str] = None
	) -> None:
		"""Deletes this file from the filesystem.

		:param contents_location: The filesystem location where files are stored.
		"""

		self.logger.info(
			"Deleting file ID %s, %s bytes",
			self.id,
			self.size
		)

		os.remove(self.get_location(contents_location))

		return super().delete()

	def get_location(
		self: File,
		location: typing.Union[None, str] = None
	) -> str:
		"""Gets this file's location on the filesystem.

		:param location: The location files are stored in. If :data:`None`, this
			is automatically set to the ``FILE_LOCATION`` environment variable.
			If that is also unset, the final fallback is
			``$current_working_directory/files``.

		:returns: The location.
		"""

		if location is None:
			location = os.environ.get(
				"FILE_LOCATION",
				os.path.join(os.getcwd(), "files")
			)

		extension = mimetypes.guess_extension(self.type)

		if extension is None:
			self.logger.warning(
				"Could not guess file ID %s's extension. The set type is %s",
				self.id,
				self.type
			)

			extension = ""

		return os.path.join(
			location,
			f"{self.id}{extension}"
		)

	def get_contents(
		self: File,
		location: typing.Union[None, str] = None
	) -> bytes:
		"""Gets this file's contents on the filesystem.

		:param location: The location files are stored in. If :data:`None`,
			this is automatically set to the ``FILE_LOCATION`` environment
			variable. If that is also unset, the final fallback is
			``$current_working_directory/files``.

		:returns: The contents, in bytes.
		"""

		with open(
			self.get_location(location),
			"rb"
		) as contents_file:
			return contents_file.read()

	def set_contents(
		self: File,
		contents: bytes,
		location: typing.Union[None, str] = None,
		set_size: bool = False
	) -> None:
		"""Sets this file's contents on the filesystem. Ideally, this should not
		be called after a file has already been created and the contents uploaded,
		but it is not forbidden.

		The current :attr:`size <.File.size>`, as well as the author
		:class:`.User`'s :attr:`file_total_size <.User.file_total_size>` is
		automatically counted.

		:param contents: The contents, in bytes.
		:param location: The location files are stored in. If :data:`None`,
			this is automatically set to the ``FILE_LOCATION`` environment
			variable. If that is also unset, the final fallback is
			``$current_working_directory/files``.
		:param set_size: Whether or not to also automatically set the
			:attr:`size <heiwa.database.File.size>` as well. Defaults to
			:data:`False`.
		"""

		from ..user import User

		size = len(contents)

		self.logger.info(
			"Writing contents for file ID %s, %s bytes",
			self.id,
			size
		)

		with open(
			self.get_location(location),
			"wb"
		) as contents_file:
			contents_file.write(contents)

		if set_size:
			self.size = size

		sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.update(User).
			where(
				sqlalchemy.and_(
					User.id == self.user_id,
					(User.file_total_size + self.size) < constants.BIG_INTEGER_LIMIT
				)
			).
			values(
				file_total_size=(User.file_total_size + self.size)
			)
		)


sqlalchemy.event.listen(
	File.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION files_create_delete_update_total_size()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE users
					SET file_total_size = file_total_size + NEW.size
					WHERE
						id = NEW.user_id AND
						(file_total_size + NEW.size) < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE users
					SET file_total_size = file_total_size - OLD.size
					WHERE id = OLD.user_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_total_size
		AFTER INSERT OR DELETE
		ON files
		FOR EACH ROW EXECUTE FUNCTION files_create_delete_update_total_size();
		"""
	).execute_if(dialect="postgresql")
)
