"""Utilities specific to the :class:`Message <heiwa.database.Message>` views,
but not only a single module.
"""

import typing
import uuid

import sqlalchemy

from heiwa import database, exceptions

from ..utils import get_session_and_user_defaults

__all__ = ["validate_message_directory_exists"]


def validate_message_directory_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Message:
	"""Validates that the message directory with the given ``id_`` existss. If
	it doesn't exist or the ``user`` does not have permission to view it, an
	exception is instead.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory to find.
	:param session: The SQLAlchemy session to execute the selection query with.
		Defaults to :attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who should have
		permission to view the directory. Defaults to :attr:`flask.g.user` if
		:data:`None`.

	:raises heiwa.exceptions.APIMessageDirectoryNotFound: Raised when the
		directory does simply not exist, or the given user does not have
		permission to view it.

	:returns: The directory.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if not (
		session.execute(
			database.MessageDirectory.get(
				session,
				user,
				conditions=(database.MessageDirectory.id == id_),
				pkeys_only=True
			).
			exists().
			select()
		)
	):
		raise exceptions.APIMessageDirectoryNotFound
