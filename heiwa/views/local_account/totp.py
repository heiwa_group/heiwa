r"""API views for
:class:`LocalAccountTOTP <heiwa.database.LocalAccountTOTP>`\ s, belonging under
the base set of views for local accounts themselves.
"""

import http.client
import typing

import flask

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from .utils import get_local_account

__all__ = [
	"create",
	"delete",
	"local_account_totp_blueprint",
	"view"
]

local_account_totp_blueprint = flask.Blueprint(
	"local_account_totp_blueprint",
	__name__,
	url_prefix="/totp"
)


@local_account_totp_blueprint.route("", methods=["POST"])
@validators.validate_json({
	"generate_backup_codes": {
		"type": "boolean",
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
def create() -> typing.Union[flask.Response, int]:
	"""Creates a TOTP for the current :attr:`flask.g.user`'s local account.

	:raises heiwa.exceptions.APILocalAccountTOTPAlreadyExists: Raised when TOTP
		is already set up for the user's account.

	:returns: The generated TOTP data, with the HTTP ``201`` status code.
	"""

	account = get_local_account(flask.g.user.id)

	if account.has_totp:
		raise exceptions.APILocalAccountTOTPAlreadyExists

	totp = account.enable_totp(
		flask.current_app.config["LOCAL_ACCOUNT_TOTP_BACKUP_CODE_COUNT"]
		if flask.g.json["generate_backup_codes"]
		else 0
	)

	flask.g.sa_session.commit()

	return flask.jsonify(totp), http.client.CREATED


@local_account_totp_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view() -> typing.Union[flask.Response, int]:
	"""Gets the TOTP information for the current :attr:`flask.g.user`'s local
	account.

	:returns: The TOTP information with the HTTP ``200`` status code. If there
		is no TOTP but the account exists, returns :data:`None` with the same
		status code.
	"""

	account = get_local_account(flask.g.user.id)

	return flask.jsonify(
		flask.g.sa_session.get(
			database.LocalAccountTOTP,
			account.id
		)
	), http.client.OK


@local_account_totp_blueprint.route("", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def delete() -> typing.Union[flask.Response, int]:
	"""Deletes the TOTP for the current :attr:`flask.g.user`'s local account,
	if it exists.

	:raises heiwa.exceptions.APILocalAccountTOTPNotFound: Raised when there is
		no TOTP to be deleted.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	account = get_local_account(flask.g.user.id)

	if not account.has_totp:
		raise exceptions.APILocalAccountTOTPNotFound

	account.disable_totp()

	return flask.jsonify(None), http.client.NO_CONTENT


@local_account_totp_blueprint.route("/backup-codes", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def generate_backup_codes() -> typing.Union[flask.Response, int]:
	"""(Re-)generates backup codes for the current :attr:`flask.g.user`'s local
	account, if it exists.

	:raises heiwa.exceptions.APILocalAccountTOTPNotFound: Raised when the user
		does not have a local account.

	:returns: The generated backup codes, with a HTTP ``201`` status code.
	"""

	account = get_local_account(flask.g.user.id)

	if not account.has_totp:
		raise exceptions.APILocalAccountTOTPNotFound

	totp = flask.g.sa_session.get(
		database.LocalAccountTOTP,
		account.id
	)

	totp.generate_backup_codes(
		count=flask.current_app.config["LOCAL_ACCOUNT_TOTP_BACKUP_CODE_COUNT"]
	)

	return flask.jsonify(totp.backup_codes), http.client.CREATED
