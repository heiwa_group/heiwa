r"""Enums. Currently only contains types for
:class:`Notification <heiwa.database.Notification>`\ s.
"""

import enum

__all__ = ["NotificationTypes"]
__version__ = "1.3.4"


class NotificationTypes(enum.Enum):
	r"""Enums for different types of
	:class:`Notification <heiwa.database.Notification>`\ s.
	"""

	NEW_POST_FROM_FOLLOWEE = "NewPostFromFollowee"
	"""A new :class:`Post <heiwa.database.Post>` from a
	:class:`User <heiwa.database.User>` another user is following.

	.. seealso::
		:obj:`heiwa.database.user_follows`
	"""

	NEW_POST_IN_SUBSCRIBED_THREAD = "NewPostInSubscribedThread"
	"""A new :class:`Post <heiwa.database.Post>` made in a
	:class:`Thread <heiwa.database.Thread>` a :class:`User <heiwa.database.User>`
	subscribed to.

	.. seealso::
		:obj:`heiwa.database.thread_subscribers`
	"""

	NEW_THREAD_FROM_FOLLOWEE = "NewThreadFromFollowee"
	"""A new :class:`Thread <heiwa.database.Thread>` from a
	:class:`User <heiwa.database.User>` another user is following.

	.. seealso::
		:obj:`heiwa.database.user_follows`
	"""

	NEW_THREAD_IN_SUBSCRIBED_FORUM = "NewThreadInSubscribedForum"
	"""A new :class:`Thread <heiwa.database.Thread>` made in a
	:class:`Forum <heiwa.database.Forum>` a :class:`User <heiwa.database.Users>`
	subscribed to.

	.. seealso::
		:obj:`heiwa.database.forum_subscribers`
	"""
