"""API views for :class:`Category <heiwa.database.Category>` objects."""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_category,
	get_forum,
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"category_blueprint",
	"create",
	"delete",
	"edit",
	"list_",
	"mass_delete",
	"mass_edit",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.0.10"

category_blueprint = flask.Blueprint(
	"category",
	__name__,
	url_prefix="/categories"
)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"forum_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Category.name.property.columns[0].type.length
	},
	"description": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Category.description.property.columns[0].type.length
	},
	"order": {
		"type": "integer",
		"min": -2147483647,
		"max": 2147483647
	}
}

CREATE_EDIT_SCHEMA = {
	"forum_id": {
		**ATTR_SCHEMAS["forum_id"],
		"nullable": True,
		"required": True
	},
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"description": {
		**ATTR_SCHEMAS["description"],
		"nullable": True,
		"required": True
	},
	"order": {
		**ATTR_SCHEMAS["order"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"order",
		"creation_timestamp",
		"edit_timestamp",
		"edit_count"
	),
	default_order_by="order",
	default_order_asc=False,
	blueprint_name=category_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(category_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"order": ATTR_SCHEMAS["order"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"forum_id": {
				**ATTR_SCHEMAS["forum_id"],
				"nullable": True
			},
			"name": ATTR_SCHEMAS["name"],
			"description": {
				**ATTR_SCHEMAS["description"],
				"nullable": True
			},
			"order": ATTR_SCHEMAS["order"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"forum_id": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["forum_id"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"description": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["description"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"order": {
				"type": "list",
				"schema": ATTR_SCHEMAS["order"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			},
			"description": {
				**ATTR_SCHEMAS["description"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@category_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Category)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a :class:`Category <heiwa.database.Category>` with the given
	:attr:`forum_id <heiwa.database.Category.forum_id>`,
	:attr:`name <heiwa.database.Category.name>`,
	:attr:`description <heiwa.database.Category.description>` and
	:attr:`order <heiwa.database.Category.order>`. Requires the current
	:class:`User <heiwa.database.User>` to be allowed to perform the ``create``
	action.

	:raises heiwa.exceptions.APIForumNotFound: Raised when the ``forum_id`` was
		not :data:`None` and the corresponding
		:class:`Forum <heiwa.database.Forum>` was not found. This can mean it
		simply does not exist, or the current user does not have permission to
		view it.

	:returns: The newly created category, with the 204 status code.
	"""

	if flask.g.json["forum_id"] is not None:
		with flask.g.sa_session.no_autoflush:
			forum = get_forum(flask.g.json["forum_id"])

			validate_permission(
				flask.g.user,
				"create",
				database.Category,
				forum=forum
			)

	category = database.Category.create(
		flask.g.sa_session,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(category), http.client.CREATED


@category_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Category)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Obtains a list of :class:`Category <heiwa.database.Category>` objects that
	the current :class:`User <heiwa.database.User>` has permission to ``view``,
	and match the ``filter`` if there is one requested, ordered according to
	``order``.

	:returns: The potentially empty list of categories, with the 200 status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Category
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Category.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Category),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@category_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"name": ATTR_SCHEMAS["name"],
				"description": ATTR_SCHEMAS["description"],
				"order": ATTR_SCHEMAS["order"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Category)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	r"""Obtains a list of the :attr:`id <heiwa.database.Category.id>`s of
	:class:`Category <heiwa.database.Category>` objects that the current
	:class:`User <heiwa.database.User>` has permission to ``view`` and ``edit``,
	and match the ``filter`` if there is one requested, ordered according to
	``order``. If a ``forum_id`` is defined in the ``values``, forums whose
	:attr:`category_id <heiwa.database.Forum.category_id>` is one of those IDs and
	:attr:`parent_forum_id <heiwa.database.Forum.parent_forum_id>` does not match
	the new ``forum_id`` have the
	:attr:`category_id <heiwa.database.Forum.category_id>` attribute set to
	:data:`None`. The categories are subsequently updated with the given
	``values``.

	:returns: :data:`None`, with the 204 status code.
	"""

	conditions = True
	action_conditions = database.Category.action_queries["edit"](flask.g.user)

	if "forum_id" in flask.g.json["values"]:
		action_conditions = sqlalchemy.and_(
			action_conditions,
			database.Category.action_queries["move"](
				flask.g.user,
				future_forum=get_forum(flask.g.json["values"]["forum_id"])
			)
		)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Category
			)
		)

	ids = flask.g.sa_session.execute(
		database.Category.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=action_conditions,
			conditions=conditions,
			order_by=get_order_by_expression(database.Category),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	)

	if len(ids) != 0:
		if "forum_id" in flask.g.json["values"]:
			flask.g.sa_session.execute(
				sqlalchemy.update(database.Forum).
				where(
					sqlalchemy.and_(
						database.Forum.category_id.in_(ids),
						database.Forum.parent_forum_id != flask.g.json["values"]["forum_id"]
					)
				).
				values(category_id=None)
			)

		flask.g.sa_session.execute(
			sqlalchemy.update(database.Category).
			where(database.Category.id.in_(ids)).
			values(**flask.g.json["values"])
		)

	return flask.jsonify(None), http.client.NO_CONTENT


@category_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Category)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	r"""Obtains a list of the :attr:`id <heiwa.database.Category.id>`s of
	:class:`Category <heiwa.database.Category>` objects that the current
	:class:`User <heiwa.database.User>` has permission to ``view`` and ``delete``,
	and match the ``filter`` if there is one requested, ordered according to
	``order``. The categories corresponding to those IDs are subsequently deleted.

	:returns: :data:`None`, with the 204 status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Category
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.Category).
		where(
			database.Category.id.in_(
				database.Category.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=(
						database.Category.action_queries["delete"](flask.g.user)
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.Category),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@category_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Category)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the :class:`Category <heiwa.database.Category>` with the given
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category was
		not found.

	:returns: The category, with the 200 status code.
	"""

	return flask.jsonify(
		get_category(id_)
	), http.client.OK


@category_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Category)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the :class:`Category <heiwa.database.Category>` with the requested
	``id_`` with the given values.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category was
		not found. This means it either simply does not exist, or the current
		:class:`User <heiwa.database.User>` does not have permission to view it.
	:raises heiwa.exceptions.APINoPermission: Raised when the current user does
		not have permission to edit the category and/or move it when there is
		also a new :attr:`forum_id <heiwa.database.Category.forum_id>` specified.
	:raises heiwa.exceptions.APICategoryUnchanged: Raised when the user has not
		changed any of the category's attributes.

	:returns: :data:`None`, with the 204 status code.
	"""

	category = get_category(id_)

	validate_permission(
		flask.g.user,
		"edit",
		category
	)

	if flask.g.json["forum_id"] != category.forum_id:
		future_forum = get_forum(flask.g.json["forum_id"])

		validate_permission(
			flask.g.user,
			"move",
			category,
			future_forum=future_forum
		)

	handle_edit(
		category,
		flask.g.json,
		exceptions.APICategoryUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@category_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Category)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the :class:`Category <heiwa.database.Category>` with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category was
		not found. This means it either simply does not exist, or the current
		:class:`User <heiwa.database.User>` does not have permission to view it.
	:raises heiwa.exceptions.APINoPermission: Raised when the current user does
		not have permission to delete the category.

	:returns: :data:`None`, with the 204 status code.
	"""

	category = get_category(id_)

	validate_permission(
		flask.g.user,
		"delete",
		category
	)

	category.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@category_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Category)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the category with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Category.id>` of the category.

	:raises heiwa.exceptions.APICategoryNotFound: Raised when the category was
		not found.

	:returns: A list of the allowed actions, with a 200 status code.

	.. seealso::
		:attr:`heiwa.database.Category.instance_actions`
	"""

	return flask.jsonify(
		get_category(id_)
	), http.client.OK


@category_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on categories, without any knowledge of which one it will be.

	:returns: The list of allowed actions, with a 200 status code.

	.. seealso::
		:attr:`heiwa.database.Category.static_actions`
	"""

	return flask.jsonify(
		database.Category.get_allowed_static_actions(flask.g.user)
	), http.client.OK
