"""Utilities specific to the :class:`User <heiwa.database.User>` views, but not
only a single module.
"""

import typing
import uuid

import flask
import sqlalchemy.orm

from heiwa import database

from ..utils import get_user, validate_user_exists

__all__ = [
	"get_user_self_or_id",
	"validate_user_self_or_id_exists"
]


def get_user_self_or_id(
	id_: typing.Union[None, uuid.UUID],
	session: typing.Union[None, sqlalchemy.orm.Session] = None
) -> database.User:
	"""If the provided ``id_`` is :data:`None`, returns the current
	:attr:`flask.g.user`. If not, returns the :class:`User <heiwa.database.User>`
	who the ``id_`` corresponds to using the
	:func:`get_user <.utils.get_user>` function.

	:param id_: The :attr:`id <heiwa.database.User.id>` of a user, or
		:data:`None`.
	:param session: The SQLAlchemy session to execute the potential seiection
		query with. Defaults to :attr:`flask.g.sa_session` if :data:`None`.

	:raises heiwa.exceptions.APIUserNotFound: Raised when the provided ID does
		not correspond to any user, or the current user does not have permission
		to view that user.

	:returns: The corresponding user.
	"""

	if session is None:
		session = flask.g.sa_session

	if id_ is not None:
		user = get_user(
			id_,
			session,
			flask.g.user
		)
	else:
		user = flask.g.user

	return user


def validate_user_self_or_id_exists(
	id_: typing.Union[None, uuid.UUID],
	session: sqlalchemy.orm.Session
) -> None:
	"""If the provided ``id_`` is :data:`None`, doesn't do anything. Otherwise,
	uses the :func:`validate_user_exists <heiwa.views.utils.validate_user_exists>`
	to validate that the a :class:`User <heiwa.database.User>` with the given
	``id_`` exists.

	:param id_: The :attr:`id <heiwa.database.User.id>` of a user, or
		:data:`None`.
	:param session: The SQLAlchemy session to execute the potential seiection
		query with.

	:raises heiwa.exceptions.APIUserNotFound: Raised when the provided ID does
		not correspond to any user, or the current user does not have permission
		to view that user.
	"""

	if id_ is None:
		return

	validate_user_exists(
		id_,
		session,
		flask.g.user
	)
