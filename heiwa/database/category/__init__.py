""""Models for categories."""

from __future__ import annotations

import typing

import flask
import sqlalchemy
import sqlalchemy.orm

from .. import Base, utils

__all__ = ["Category"]
__version__ = "1.0.8"


class Category(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Category model.

	.. note::
		Like forums, categories have no owners.
	"""

	__tablename__ = "categories"

	forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=True
	)
	"""The :attr:`id <.Forum.id>` of the :class:`.Forum` a category falls under.
	Works almost identically to :attr:`.Forum.parent_forum_id`.

	.. note::
		The original purpose of this was dealing with forum-based permissions. An
		unintended side-effect was also establishing a relationship with forum
		parents and greatly simplifying categorization of subforums.
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""A category's name."""

	description = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
	"""A category's description."""

	order = sqlalchemy.Column(
		sqlalchemy.Integer,
		default=0,
		nullable=False
	)
	"""The order a category will be displayed in by default. The higher it is,
	the higher the category will be.
	"""

	forum = sqlalchemy.orm.relationship(
		"Forum",
		uselist=False,
		passive_deletes="all",
		foreign_keys=[forum_id],
		lazy=True
	)
	"""The :class:`.Forum` a category falls under."""

	def get_id_url(self: Category) -> str:
		"""Gets the JSON-LD ``@id`` route to this category.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"category.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Category) -> None:
		r"""Adds the :attr:`instance_actions <.Category.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific categories:

			``delete``:
				Whether or not a user can delete this category.

			``edit``:
				Whether or not a user can edit this category.

			``move``:
				Whether or not a user can move this category to another :class:`.Forum`.

			``view``:
				Whether or not a user can view this category.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"move": self.get_instance_action_move,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: Category, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this category.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and (
				user.parsed_permissions["category_delete"]
				if self.forum_id is None
				else self.forum.get_parsed_permissions(user).category_delete
			)
		)

	def get_instance_action_edit(self: Category, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this category.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and (
				user.parsed_permissions["category_edit"]
				if self.forum_id is None
				else self.forum.get_parsed_permissions(user).category_edit
			)
		)

	def get_instance_action_move(
		self: Category,
		user,
		future_forum=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move this category to
		another :class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum this category is about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.

		.. seealso::
			:attr:`.Category.forum_id`
		"""

		return (
			self.instance_actions["view"](user) and (
				user.parsed_permissions["category_move"]
				if self.forum_id is None
				else self.forum.get_parsed_permissions(user).category_move
			) and (
				future_forum is None or
				future_forum.instance_actions["move_category"](user)
			)
		)

	def get_instance_action_view(self: Category, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this category.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			user.parsed_permissions["forum_view"]
			if self.forum_id is None
			else self.forum.instance_actions["view"](user)
		)

	@staticmethod
	def get_static_action_create(
		user,
		forum=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create categories.

		:param user: The user, a :class:`.User`.
		:param forum: The :class:`.Forum` to create the categories in. :data:`None`
			by default, meaning this is ignored.

		:returns: The result of the check.
		"""

		return (
			Category.static_actions["view"](user) and
			user.parsed_permissions["category_create"] and (
				forum is None or
				forum.instance_actions["create_category"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete categories.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Category.static_actions["view"](user) and
			user.parsed_permissions["category_delete"]
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit categories.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Category.static_actions["view"](user) and
			user.parsed_permissions["category_edit"]
		)

	@staticmethod
	def get_static_action_move(
		user,
		future_forum=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move categories to another
		:class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum categories should be moved to. By default,
			this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.
		"""

		return (
			Category.static_actions["view"](user) and
			user.parsed_permissions["category_move"] and (
				future_forum is None or
				future_forum.instance_actions["move_category"](user)
			)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view categories.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["forum_view"]

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which categories ``user``
		is allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Category.action_queries["view"](user),
			sqlalchemy.or_(
				sqlalchemy.and_(
					ForumParsedPermissions.category_delete.is_(True),
					~Category.forum_id.is_(None)
				),
				user.parsed_permissions["category_delete"]
			)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which categories ``user``
		is allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Category.action_queries["view"](user),
			sqlalchemy.or_(
				sqlalchemy.and_(
					ForumParsedPermissions.category_edit.is_(True),
					~Category.forum_id.is_(None)
				),
				user.parsed_permissions["category_delete"]
			)
		)

	@staticmethod
	def get_action_query_move(
		user,
		future_forum=None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which categories ``user``
		is allowed to move to another :class:`.Forum`.

		:param user: The user, a :class:`.User`.
		:param future_forum: The forum categories are about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.and_(
			Category.action_queries["view"](user),
			sqlalchemy.or_(
				sqlalchemy.and_(
					ForumParsedPermissions.category_move.is_(True),
					~Category.forum_id.is_(None)
				),
				user.parsed_permissions["category_move"]
			),
			(
				True
				if future_forum is None
				else future_forum.action_queries["move_category"](user)
			)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which categories ``user``
		is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from ..forum import ForumParsedPermissions

		return sqlalchemy.or_(
			sqlalchemy.and_(
				ForumParsedPermissions.forum_view.is_(True),
				~Category.forum_id.is_(None)
			),
			user.parsed_permissions["forum_view"]
		)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"move": get_static_action_move,
		"view": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all categories, without
	any indication of which thread it is.

	``create``:
		Whether or not a user can create categories.

	``delete``:
		Whether or not a user can delete categories.

	``edit``:
		Whether or not a user can edit categories.

	``move``:
		Whether or not a user can move categories to other :class:`.Forum`\ s.

	``view``:
		Whether or not a user can view categories.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"move": get_action_query_move,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Category.instance_actions>`.
	"""

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False
	) -> sqlalchemy.sql.Select:
		"""Generates a selection query with permissions already handled.

		Since this category's :class:`.Forum`'s permissions may not be parsed,
		this will emit additional queries to check, as long there is a forum
		attached.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that categories should
			fulfill, including the context of parsed forum permissions, unlike
			``conditions``. Generally, this will be generated using
			:attr:`action_queries <.Category.action_queries>`. :data:`True` by
			default, meaning there are no additional conditions.
		:param conditions: Any additional conditions, outside a parsed forum
			permission context. :data:`True` by default, meaning there are no
			conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the primary
			key.

		:returns: The query.
		"""

		from ..forum import Forum, ForumParsedPermissions

		inner_conditions = sqlalchemy.and_(
			~cls.forum_id.is_(None),
			ForumParsedPermissions.forum_id == cls.forum_id,
			ForumParsedPermissions.user_id == user.id
		)

		while True:
			rows = session.execute(
				sqlalchemy.select(
					cls.id,
					cls.forum_id,
					(
						sqlalchemy.select(ForumParsedPermissions.forum_id).
						where(inner_conditions).
						exists()
					)
				).
				where(
					sqlalchemy.and_(
						conditions,
						sqlalchemy.or_(
							~(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(inner_conditions).
								exists()
							),
							(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(
									sqlalchemy.and_(
										inner_conditions,
										cls.action_queries["view"](user),
										action_conditions
									)
								).
								exists()
							)
						)
					)
				).
				order_by(order_by).
				limit(limit).
				offset(offset)
			).all()

			if len(rows) == 0:
				# No need to hit the database with a complicated query twice
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(False)
				)

			category_ids = []
			unparsed_permission_forum_ids = []

			for row in rows:
				(
					category_id,
					forum_id,
					parsed_permissions_exist
				) = row

				if (
					forum_id is not None and
					not parsed_permissions_exist
				):
					unparsed_permission_forum_ids.append(forum_id)

					continue

				category_ids.append(category_id)

			if len(unparsed_permission_forum_ids) != 0:
				for forum in (
					session.execute(
						sqlalchemy.select(Forum).
						where(Forum.id.in_(unparsed_permission_forum_ids))
					).scalars()
				):
					forum.parse_permissions(user)

				session.commit()
			else:
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(cls.id.in_(category_ids)).
					order_by(order_by)
				)
