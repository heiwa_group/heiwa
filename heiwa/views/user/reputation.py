r"""API views for
:class:`UserReputation <heiwa.database.UserReputation>` points, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import datetime
import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	get_user,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"EDIT_AND_PARTIAL_CREATE_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"get_reputation",
	"list_",
	"mass_delete",
	"mass_edit",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"user_reputation_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

user_reputation_blueprint = flask.Blueprint(
	"reputation",
	__name__,
	url_prefix="/users/reputation"
)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"giver_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"receiver_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"is_positive": {
		"type": "boolean",
		"nullable": True  # Define here, always nullable
	},
	"message": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.UserReputation.message.property.columns[0].type.length
	}
}

EDIT_AND_PARTIAL_CREATE_SCHEMA = {
	"is_positive": {
		**ATTR_SCHEMAS["is_positive"],
		"required": True
	},
	"message": {
		**ATTR_SCHEMAS["message"],
		"nullable": True,
		"required": True
	}
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=user_reputation_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(user_reputation_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"]
}

search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"giver_id": ATTR_SCHEMAS["giver_id"],
			"receiver_id": ATTR_SCHEMAS["receiver_id"],
			"is_positive": ATTR_SCHEMAS["is_positive"],
			"message": {
				**ATTR_SCHEMAS["message"],
				"nullable": True
			}
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"giver_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["giver_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"receiver_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["receiver_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"is_positive": {
				"type": "list",
				"schema": ATTR_SCHEMAS["is_positive"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"message": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["message"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"message": {
				**ATTR_SCHEMAS["message"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


def get_reputation(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.UserReputation:
	"""Gets the reputation point with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.UserReputation.id>` of the
		reputation point to find.
	:param session: The session to find the reputation point with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The user who must have permission to view the reputation point.
		Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.database.APIUserReputationNotFound: Raised when the ``id_`` does
		not correspond to any reputation point, or the ``user`` does not have
		permission to view it.

	:returns: The reputation point.
	"""

	session, user = get_session_and_user_defaults(session, user)

	reputation = session.execute(
		database.UserReputation.get(
			session,
			user,
			conditions=(database.UserReputation.id == id_)
		)
	).scalars().one_or_none()

	if reputation is None:
		raise exceptions.APIUserReputationNotFound

	return reputation


@user_reputation_blueprint.route("", methods=["POST"])
@validators.validate_json({
	"receiver_id": {
		**ATTR_SCHEMAS["receiver_id"],
		"required": True
	},
	**EDIT_AND_PARTIAL_CREATE_SCHEMA
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.UserReputation)
def create() -> typing.Tuple[flask.Response, int]:
	r"""Creates a reputation point with the given ``receiver_id``, positive
	status (``is_positive``) and ``message``.

	:raises heiwa.exceptions.APIUserReputationReceiverIsSelf: Raised when the
		reputation point's receiver is the same as the current
		:attr:`flask.g.user`.
	:raises heiwa.exceptions.APIUserReputationExceededCooldown: Raised when the
		current :attr:`flask.g.user` has already given this user a reputation
		point, and the amount of time since then is shorter than what the
		``USER_REPUTATION_COOLDOWN`` config key contains. The time when they're
		able to give a reputation point to this user again is given in the
		:attr:`details <heiwa.exceptions.APIUserReputationExceededCooldown.det\
		ails>`.

	:returns: The newly created reputation point, with the ``201`` HTTP status
		code.
	"""

	user = get_user(flask.g.json["receiver_id"])

	if user.id == flask.g.user.id:
		raise exceptions.APIUserReputationReceiverIsSelf

	cooldown_timedelta = datetime.timedelta(
		seconds=flask.current_app.config["USER_REPUTATION_COOLDOWN"]
	)

	last_reputation_timestamp = flask.g.sa_session.execute(
		sqlalchemy.select(database.UserReputation.creation_timestamp).
		where(
			sqlalchemy.and_(
				database.UserReputation.giver_id == flask.g.user.id,
				database.UserReputation.receiver_id == flask.g.json["receiver_id"],
				database.UserReputation.creation_timestamp >= (
					flask.g.current_utc_time - cooldown_timedelta
				)
			)
		)
	).scalars().one_or_none()

	if last_reputation_timestamp is not None:
		raise exceptions.APIUserReputationExceededCooldown(
			last_reputation_timestamp
			+ cooldown_timedelta
		)

	validate_permission(
		flask.g.user,
		"create",
		database.UserReputation,
		receiving_user=user
	)

	reputation = database.UserReputation.create(
		flask.g.sa_session,
		giver_id=flask.g.user.id,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(reputation), http.client.CREATED


@user_reputation_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserReputation)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all recorded reputation points that the current :attr:`flask.g.user`
	has access to view and match the requested filter, if there is one.

	:returns: The list of recorded reputation points, with a ``200`` HTTP status
		code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.UserReputation
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.UserReputation.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.UserReputation),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@user_reputation_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"is_positive": ATTR_SCHEMAS["is_positive"],
				"message": ATTR_SCHEMAS["message"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserReputation)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all recorded reputation points that the current
	:attr:`flask.g.user` has access to both view and edit with the requested
	values. If there is a filter also requested, they must match it.

	:returns: :data:`None` with the ``204`` HTTP status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.UserReputation
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.UserReputation).
		where(
			database.UserReputation.id.in_(
				database.UserReputation.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=(
						database.UserReputation.action_queries["edit"](flask.g.user)
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.UserReputation),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_reputation_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.UserReputation)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all recorded reputation points that the current
	:attr:`flask.g.user` has access to both view and delete. If there is a filter
	requested, they must also match it.

	:returns: :data:`None` with the ``204`` HTTP status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.UserReputation
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.UserReputation).
		where(
			database.UserReputation.id.in_(
				database.UserReputation.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=(
						database.UserReputation.action_queries["delete"](flask.g.user)
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.UserReputation),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_reputation_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserReputation)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the reputation point with the given ``id_``. The current
	:attr:`flask.g.user` must have permission to view it.

	:param id_: The :attr:`id <heiwa.database.UserReputation.id>` of the
		reputation point.

	:raises heiwa.exceptions.APIUserReputationNotFound: Raised when the ``id_``
		does not correspond to any reputation point, or the current user does not
		have permission to view it.

	:returns: The reputation point, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_reputation(id_)
	), http.client.OK


@user_reputation_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(EDIT_AND_PARTIAL_CREATE_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserReputation)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the reputation point with the given ``id_`` with the requested
	values. The current :attr:`flask.g.user` must have permission to both view
	and edit it.

	:param id_: The :attr:`id <heiwa.database.UserReputation.id>` of the
		reputation point.

	:raises heiwa.exceptions.APIUserReputationNotFound: Raised when the ``id_``
		does not correspond to any reputation point, or the current user does not
		have permission to view it.
	:raises heiwa.exceptions.APIUserReputationUnchanged: Raised when no attributes
		of the reputation point have been changed.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	reputation = get_reputation(id_)

	validate_permission(
		flask.g.user,
		"edit",
		reputation
	)

	handle_edit(
		reputation,
		flask.g.json,
		exceptions.APIUserReputationUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_reputation_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.UserReputation)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the reputation point with the given ``id_``. The current
	:attr:`flask.g.user` must have permission to both view and delete it.

	:param id_: The :attr:`id <heiwa.database.UserReputation.id>` of the
		reputation point.

	:raises heiwa.exceptions.APIUserReputationNotFound: Raised when the ``id_``
		does not correspond to any reputation point, or the current user does not
		have permission to view it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	reputation = get_reputation(id_)

	validate_permission(
		flask.g.user,
		"delete",
		reputation
	)

	reputation.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@user_reputation_blueprint.route(
	"/<uuid:id_>/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserReputation)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on the
	reputation point with the given ``id_``. They must at least have the
	permission to view it.

	:param id_: The :attr:`id <heiwa.database.UserReputation.id>` of the
		reputation point.

	:raises heiwa.exceptions.APIUserReputationNotFound: Raised when the ``id_``
		does not correspond to any reputation point, or the current user does not
		have permission to view it.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_reputation(id_)
	), http.client.OK


@user_reputation_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	reputation points, regardless of who they were made by or who they belong
	to.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.UserReputation.get_allowed_static_actions(flask.g.user)
	), http.client.OK
