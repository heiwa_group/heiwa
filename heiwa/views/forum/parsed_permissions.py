r"""API views for
:class:`ForumParsedPermissions <heiwa.database.ForumParsedPermissions>`,
belonging under the base set of views for
:class:`Forum <heiwa.database.Forum>`\ s.
"""

import http.client
import typing
import uuid

import flask

from heiwa import authentication, database, limiter

from ..utils import get_forum, requires_permission

__all__ = [
	"forum_parsed_permissions_blueprint",
	"view"
]

forum_parsed_permissions_blueprint = flask.Blueprint(
	"parsed_permissions",
	__name__,
	url_prefix="/<uuid:id_>/parsed-permissions"
)


@forum_parsed_permissions_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Forum)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the parsed permissions for :attr:`flask.g.user` for the forum with
	the requested ``id_``. If they haven't been parsed yet, it's done so
	atomatically.
	"""

	return flask.jsonify(
		get_forum(id_)
	), http.client.OK
