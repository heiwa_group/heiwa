"""Statistics model."""

from __future__ import annotations

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants

from .. import Base, utils

__all__ = ["Statistic"]
__version__ = "1.0.1"


class Statistic(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	Base
):
	"""Statistics model. Instances should only be created through the
	:meth:`log_statistics <heiwa.tasks.log_statistics>` function being run
	according to its schedule. Once stored, data should not be changed or
	deleted.

	If nothing has changed since the time the last statistic was recorded -
	provided that there is one - a new instance shouldn't be made.
	"""

	__tablename__ = "statistics"

	@staticmethod
	def get_thread_count_default() -> sqlalchemy.sql.selectable.ScalarSelect:
		r"""Gets a selection subquery for the total amount of :class:`.Thread`\ s
		- which is also the :attr:`thread_count <.Statistic.thread_count>`
		default.

		:returns: The selection query.
		"""

		from ..thread import Thread

		return (
			sqlalchemy.select(
				sqlalchemy.func.count(Thread.id)
			).
			limit(constants.BIG_INTEGER_LIMIT).
			scalar_subquery()
		)

	@staticmethod
	def get_post_count_default() -> sqlalchemy.sql.selectable.ScalarSelect:
		r"""Gets a selection subquery for the total amount of :class:`.Post`\ s
		- which is also the :attr:`post_count <.Statistic.post_count>` default.

		:returns: The selection query.
		"""

		from ..post import Post

		return (
			sqlalchemy.select(
				sqlalchemy.func.count(Post.id)
			).
			limit(constants.BIG_INTEGER_LIMIT).
			scalar_subquery()
		)

	@staticmethod
	def get_user_count_default() -> sqlalchemy.sql.selectable.ScalarSelect:
		r"""Gets a selection subquery for the total amount of :class:`.User`\ s
		- which is also the :attr:`user_count <.Statistic.user_count>` default.

		:returns: The selection query.
		"""

		from ..user import User

		return (
			sqlalchemy.select(
				sqlalchemy.func.count(User.id)
			).
			limit(constants.BIG_INTEGER_LIMIT).
			scalar_subquery()
		)

	thread_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=get_thread_count_default,
		nullable=False
	)
	r"""The total number of :class:`.Thread`\ s, irrespective of permissions or
	their :class:`.Forum`.
	"""

	post_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=get_post_count_default,
		nullable=False
	)
	r"""The total number of :class:`.Post`\ s, irrespective of permissions or
	their :class:`.Thread`.
	"""

	user_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=get_user_count_default,
		nullable=False
	)
	r"""The total number of :class:`.User`\ s."""

	def get_id_url(self: Statistic) -> str:
		"""Gets the JSON-LD ``@id`` route to this statistic.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"statistic.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Statistic) -> None:
		r"""Adds the :attr:`instance_actions <.Statistic.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific statistics:

			``view``:
				Whether or not a user can view this statistic.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"view": self.get_instance_action_view
		}

	def get_instance_action_view(self: Statistic, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this statistic.

		:param user: The user.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["statistic_view"]

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view statistic.

		:param user: The user.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["statistic_view"]

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which statistics ``user``
		is allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		return user.parsed_permissions["statistic_view"]

	static_actions = {
		"view": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all statistics, without
	any indication of which statistic it is.

	``view``:
		Whether or not a user can view statistics.
	"""

	action_queries = {
		"view": get_static_action_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Statistic.instance_actions>`.
	"""
