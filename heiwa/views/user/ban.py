r"""API views for
:class:`UserBan <heiwa.database.UserBan>`\ s, belonging under the base set of
views for :class:`User <heiwa.database.User>`\ s.
"""

import datetime
import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	get_user,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"EDIT_AND_PARTIAL_CREATE_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"check_with_timestamp_is_in_the_future",
	"create",
	"edit",
	"expire",
	"get_ban",
	"list_",
	"mass_edit",
	"mass_expire",
	"search_schema",
	"search_schema_filter_max_in_length",
	"search_schema_registry",
	"user_ban_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

user_ban_blueprint = flask.Blueprint(
	"ban",
	__name__
)


def check_with_timestamp_is_in_the_future(
	field: str,
	value: typing.Union[None, datetime.datetime],
	error: typing.Callable[[str, str], None]
) -> None:
	"""Checks whether or not ``value`` is a datetime at any point in the future.
	If not, ``error`` is called. This function should be used within a Cerberus
	validator schema.

	:param field: The current field.
	:param value: The field's value. If :data:`None`, this means the field is
		nullable. Nothing happens in this case.
	:param error: A callback to be called when the validation failed.

	.. note::
		This function is stored in this module instead of
		:class:`Validator <heiwa.validators.Validator>`, since it's only ever
		going to be used here. If that's not the case in the future, it will be
		moved there.
	"""

	if value is None:
		return

	if datetime.datetime.now(tz=value.tzinfo) < value:
		error(field, "must be in the future")


ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"giver_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid",
		"nullable": True  # Should always be nullable, defined here
	},
	"receiver_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"expiration_timestamp": {
		"type": "datetime",
		"check_with": check_with_timestamp_is_in_the_future,
		"coerce": "convert_to_datetime"
	},
	"reason": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.UserBan.reason.property.columns[0].type.length
	}
}

EDIT_AND_PARTIAL_CREATE_SCHEMA = {
	"expiration_timestamp": {
		**ATTR_SCHEMAS["expiration_timestamp"],
		"required": True
	},
	"reason": {
		**ATTR_SCHEMAS["reason"],
		"nullable": True,
		"required": True
	}
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"expiration_timestamp"
	),
	default_order_by="expiration_timestamp",
	default_order_asc=False,
	blueprint_name=user_ban_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(user_ban_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"expiration_timestamp": ATTR_SCHEMAS["expiration_timestamp"]
}

search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"giver_id": ATTR_SCHEMAS["giver_id"],
			"receiver_id": ATTR_SCHEMAS["receiver_id"],
			"expiration_timestamp": ATTR_SCHEMAS["expiration_timestamp"],
			"reason": {
				**ATTR_SCHEMAS["reason"],
				"nullable": True
			}
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"giver_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["giver_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"receiver_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["receiver_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"expiration_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["expiration_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"reason": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["reason"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"reason": {
				**ATTR_SCHEMAS["reason"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


def get_ban(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.UserBan:
	"""Gets the ban with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.UserBan.id>` of the ban to find.
	:param session: The session to find the ban with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The user who must have permission to view the ban. Defaults to
		:attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIUserBanNotFound: Raised when the ``id_`` does not
		correspond to any ban, or the ``user`` does not have permission to view
		it.

	:returns: The ban.
	"""

	session, user = get_session_and_user_defaults(session, user)

	ban = session.execute(
		database.UserBan.get(
			session,
			user,
			conditions=(database.UserBan.id == id_)
		)
	).scalars().one_or_none()

	if ban is None:
		raise exceptions.APIUserBanNotFound

	return ban


@user_ban_blueprint.route("/users/bans", methods=["POST"])
@validators.validate_json({
	"receiver_id": {
		**ATTR_SCHEMAS["receiver_id"],
		"required": True
	},
	**EDIT_AND_PARTIAL_CREATE_SCHEMA
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.UserBan)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a ban with the given ``receiver_id``, ``expiration_timestamp``
	and ``reason``.

	The :attr:`giver_id <heiwa.database.UserBan.giver_id>` is set to the
	:attr:`flask.g.user`'s.

	:raises heiwa.exceptions.APIUserBanAlreadyExists: Raised when the receiver
		is already banned.

	:returns: The newly created ban, with a ``201`` HTTP status code.
	"""

	user = get_user(flask.g.json["receiver_id"])

	if user.is_banned:
		raise exceptions.APIUserBanAlreadyExists

	validate_permission(
		flask.g.user,
		"create",
		database.UserBan,
		receiving_user=user
	)

	ban = user.create_ban(
		giver_id=flask.g.user.id,
		expiration_timestamp=flask.g.json["expiration_timestamp"],
		reason=flask.g.json["reason"]
	)

	flask.g.sa_session.commit()

	return flask.jsonify(ban), http.client.CREATED


@user_ban_blueprint.route("/users/bans", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserBan)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Lists all bans that the current :attr:`flask.g.user` has access to view
	and match the requested filter, if there is one. Whether or not they've
	expired is not considered.

	However, if a user wishes to only see non-expired bans, they can filter by
	their expiration timestamp.

	.. code-block:: json

		{
			"filter": {"$greater_than_or_equal_to": "[ CURRENT DATE AND TIME ]"}
		}

	:returns: The list of bans. By default, this will be all bans within the
		given limit. The HTTP status code is ``200``.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.UserBan.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.UserBan),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@user_ban_blueprint.route("/users/bans", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"expiration_timestamp": ATTR_SCHEMAS["expiration_timestamp"],
				"reason": {
					**ATTR_SCHEMAS["reason"],
					"nullable": True
				}
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserBan)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all bans that :attr:`flask.g.user` has access to both view and
	edit with the requested values. If a filter is also requested, they must
	match that filter.

	Only non-expired bans are considered no matter what, as past bans should
	remain unmodified for future reference.

	:returns: :data:`None`, with the ``204`` HTTP status code.
	"""

	conditions = (~database.UserBan.is_expired)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.UserBan).
		where(
			database.UserBan.id.in_(
				database.UserBan.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=(
						database.UserBan.action_queries["edit"](flask.g.user)
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.UserBan),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_ban_blueprint.route("/users/bans", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserBan)
def mass_expire() -> typing.Tuple[flask.Response, int]:
	"""Expires all bans that the current :attr:`flask.g.user` has access to view
	and edit. If there is a filter requested, they must also match that filter.

	Only non-expired bans are considered no matter what, even though the filter
	condition is still exposed - this is to allow users to only expire bans
	which will expire at some specified point in the future, instead of all of
	them.

	:returns: :data:`None`, with the ``204`` HTTP status code.
	"""

	conditions = (~database.UserBan.is_expired)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	ids = flask.g.sa_session.execute(
		database.UserBan.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=(
				database.UserBan.action_queries["edit"](flask.g.user)
			),
			conditions=conditions,
			order_by=get_order_by_expression(database.UserBan),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).scalars().all()

	if len(ids) != 0:
		flask.g.sa_session.execute(
			sqlalchemy.update(database.User).
			where(
				database.User.id.in_(
					sqlalchemy.select(database.UserBan.receiver_id).
					where(database.UserBan.id.in_(ids))
				)
			).
			values(is_banned=False)
		)

		flask.g.sa_session.execute(
			sqlalchemy.update(database.UserBan).
			where(database.UserBan.id.in_(ids)).
			values(
				expiration_timestamp=flask.g.current_utc_time
			)
		)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_ban_blueprint.route("/users/bans/<uuid:id_>", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserBan)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the ban with the given ``id_``. The current :attr:`flask.g.user` must
	have permission to view it.

	:param id_: The :attr:`id <heiwa.database.UserBan.id>` of the ban.

	:raises heiwa.exceptions.APIUserBanNotFound: Raised when the ``id_`` does not
		correspond to any ban, or the current user does not have permission to view
		it.

	:returns: The ban, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_ban(id_)
	), http.client.OK


@user_ban_blueprint.route("/users/bans/<uuid:id_>", methods=["PUT"])
@validators.validate_json(EDIT_AND_PARTIAL_CREATE_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserBan)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the ban with the given ``id_`` with the requested values. The
	current :attr:`flask.g.user` must have permission to both view and edit it.

	:param id_: The :attr:`id <heiwa.database.UserBan.id>` of the ban.

	:raises heiwa.exceptions.APIUserBanNotFound: Raised when the ``id_`` does not
		correspond to any ban, or the current user does not have permission to view
		it.
	:raises heiwa.exceptions.APIUserBanUnchanged: Raised when none of the ban's
		attributes have been changed.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	ban = get_ban(id_)

	validate_permission(
		flask.g.user,
		"edit",
		ban
	)

	handle_edit(
		ban,
		flask.g.json,
		exceptions.APIUserBanUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_ban_blueprint.route("/users/bans/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.UserBan)
def expire(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Expires the ban with the given ``id_``. The current :attr:`flask.g.user`
	must have permission to both view and edit it.

	:param id_: The :attr:`id <heiwa.database.UserBan.id>` of the ban.

	:raises heiwa.exceptions.APIUserBanNotFound: Raised when the ``id_`` does not
		correspond to any ban, or the current user does not have permission to view
		it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	ban = get_ban(id_)

	validate_permission(
		flask.g.user,
		"edit",
		ban
	)

	ban.user.delete_ban()

	return flask.jsonify(None), http.client.NO_CONTENT


@user_ban_blueprint.route(
	"/users/bans/<uuid:id_>/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.UserBan)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on the
	ban with the given ``id_``. They must at least have the permission to view
	it.

	:param id_: The :attr:`id <heiwa.database.UserBan.id>` of the ban.

	:raises heiwa.exceptions.APIUserBanNotFound: Raised when the ``id_`` does not
		correspond to any ban, or the current user does not have permission to view
		it.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_ban(id_)
	), http.client.OK


@user_ban_blueprint.route("/users/bans/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	bans, regardless of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.UserBan.get_allowed_static_actions(flask.g.user)
	), http.client.OK
