r"""Models related to :class:`.User`\ s' preferences."""

from __future__ import annotations


import sqlalchemy
import sqlalchemy.ext.hybrid
import sqlalchemy.orm

from .. import Base, utils

__all__ = ["UserPreferences"]


class UserPreferences(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.EditInfoMixin,
	Base
):
	""":class:`.User` preferences model. Automatically created and associated with
	each new registered user. When there is a preference which could affect the
	user's privacy, it will always default to the option which doesn't.

	.. note::
		The ``creation_timestamp`` attribute does not exist for this model, as
		it's created at the same time as its user and should never change users
		in part due to its private nature.

	.. note::
		Since instances of this model are only ever going to be visible to their
		owner, no permissions are handled.
	"""

	__tablename__ = "user_preferences"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a set of preferences
	belongs to. Once created, this value should never be changed.
	"""

	should_record_activity = sqlalchemy.Column(
		sqlalchemy.Boolean(),
		nullable=False,
		default=False
	)
	"""Whether or not a user wants their
	:attr:`last_activity_timestamp <.User.last_activity_timestamp>` to be updated
	when they send API requests.
	"""
