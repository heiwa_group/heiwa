"""API Exceptions."""

from __future__ import annotations

import http.client

__all__ = [
	"APIException",
	"APIAuthorizationHeaderInvalid",
	"APIAuthorizationHeaderMissing",
	"APIAuthorizationJWTInvalid",
	"APIAuthorizationJWTInvalidClaims",
	"APIAuthorizationJWTInvalidSubjectResourceType",
	"APIAuthorizationJWTSubjectNotFound",
	"APICategoryNotFound",
	"APICategoryUnchanged",
	"APIFileExceededMaxTotalSize",
	"APIFileNotAllowedType",
	"APIFileNotFound",
	"APIFileUnchanged",
	"APIForumCategoryOutsideParent",
	"APIForumChildLevelLimitReached",
	"APIForumNotFound",
	"APIForumParentIsChild",
	"APIForumPermissionsGroupMassUpdateInconsistent",
	"APIForumPermissionsGroupNotFound",
	"APIForumPermissionsGroupUnchanged",
	"APIForumPermissionsUserMassUpdateInconsistent",
	"APIForumPermissionsUserNotFound",
	"APIForumPermissionsUserUnchanged",
	"APIForumSubscriptionAlreadyExists",
	"APIForumSubscriptionNotFound",
	"APIForumUnchanged",
	"APIGroupIsLastDefault",
	"APIGroupNotFound",
	"APIGroupPermissionsForLastDefault",
	"APIGroupPermissionsNotFound",
	"APIGroupPermissionsUnchanged",
	"APIGroupUnchanged",
	"APIGuestSessionLimitReached",
	"APIJSONInvalid",
	"APIJSONMissing",
	"APILocalAccountEmailAlreadyUsed",
	"APILocalAccountEmailNeverVerified",
	"APILocalAccountNotFound",
	"APILocalAccountPasswordInvalid",
	"APILocalAccountPasswordResetEmailSendingFailed",
	"APILocalAccountPasswordResetJWTInvalid",
	"APILocalAccountPasswordResetJWTInvalidClaims",
	"APILocalAccountPasswordResetJWTInvalidScope",
	"APILocalAccountPasswordResetJWTInvalidSubjectResourceType",
	"APILocalAccountPasswordResetJWTSubjectMismatch",
	"APILocalAccountTOTPAlreadyExists",
	"APILocalAccountTOTPCodeInvalid",
	"APILocalAccountTOTPNotFound",
	"APILocalAccountUnchanged",
	"APILocalAccountVerificationAlreadyDone",
	"APILocalAccountVerificationEmailSendingFailed",
	"APILocalAccountVerificationJWTInvalid",
	"APILocalAccountVerificationJWTInvalidClaims",
	"APILocalAccountVerificationJWTInvalidScope",
	"APILocalAccountVerificationJWTInvalidSubjectResourceType",
	"APILocalAccountVerificationJWTSubjectNotFound",
	"APIMessageDirectoryNotFound",
	"APIMessageDirectoryUnchanged",
	"APIMessageNotFound",
	"APIMessageReceiverBlockedSender",
	"APIMessageUnchanged",
	"APINoPermission",
	"APINotificationNotFound",
	"APIOIDCAuthenticationFailed",
	"APIOIDCNonceInvalid",
	"APIOIDCServiceNotFound",
	"APIOIDCStateInvalid",
	"APIPostNotFound",
	"APIPostUnchanged",
	"APIPostVoteNotFound",
	"APIPostVoteUnchanged",
	"APIRateLimitExceeded",
	"APIStatisticNotFound",
	"APIThreadLocked",
	"APIThreadNotFound",
	"APIThreadSubscriptionAlreadyExists",
	"APIThreadSubscriptionNotFound",
	"APIThreadUnchanged",
	"APIThreadViewRecordExceededCooldown",
	"APIThreadVoteNotFound",
	"APIThreadVoteUnchanged",
	"APIUserAvatarNotAllowedType",
	"APIUserAvatarUnchanged",
	"APIUserBanAlreadyExists",
	"APIUserBanNotFound",
	"APIUserBanUnchanged",
	"APIUserBanned",
	"APIUserBlockAlreadyExists",
	"APIUserBlockNotFound",
	"APIUserFollowAlreadyExists",
	"APIUserGroupAlreadyAdded",
	"APIUserGroupIsLastDefault",
	"APIUserGroupNotAdded",
	"APIUserNameNotUnique",
	"APIUserNotFound",
	"APIUserPermissionsUnchanged",
	"APIUserPreferencesUnchanged",
	"APIUserReputationExceededCooldown",
	"APIUserReputationNotFound",
	"APIUserReputationReceiverBlockedSender",
	"APIUserReputationReceiverIsSelf",
	"APIUserReputationUnchanged",
	"APIUserUnchanged"
]
__version__ = "1.53.0"


class APIException(Exception):
	"""The base class for all API exceptions."""

	code = http.client.INTERNAL_SERVER_ERROR
	"""The HTTP error code of an exception. By default, this will be ``500``."""

	details = None
	"""The details about an exception. :data:`None` by default."""

	def __init__(
		self: APIException,
		details: object = details,
		**kwargs
	) -> None:
		"""Sets the :attr:`details <.APIException.details>` class variable to the
		given value. If this method isn't used, it remains :data:`None`.

		:param details: The exception's details.
		"""

		self.details = details

		super().__init__(**kwargs)

	def __str__(self: APIException) -> str:
		"""Turns this instance into a human-readable string.

		:returns: The string.
		"""

		details = (
			self.details
			if self.details is not None
			else "no details"
		)

		return f"{self.__class__.__name__}, code {self.code} - {details}"

	def __repr__(self: APIException) -> str:
		"""Turns this instance into a typical ``__repr__`` string.

		:returns: The string.
		"""

		return (
			f"<{self.__class__.__name__}(code={self.code}, details={self.details})>"
		)


class APIAuthorizationHeaderInvalid(APIException):
	"""Exception class for when the ``Authorization`` header is required and
	present, but not valid. (e.g. Basic instead of Bearer, when only Bearer
	is supported)
	"""

	code = http.client.BAD_REQUEST


class APIAuthorizationHeaderMissing(APIException):
	"""Exception class for when the ``Authorization`` header is required, but
	not present.
	"""

	code = http.client.BAD_REQUEST


class APIAuthorizationJWTInvalid(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` JWT
	(usually provided in the ``Authorization`` header) is not at all valid and
	could not be decoded.

	.. seealso::
		:func:`heiwa.authentication.authenticate_via_jwt`
	"""

	code = http.client.BAD_REQUEST


class APIAuthorizationJWTInvalidClaims(APIException):
	"""Exception class for when a provided :class:`User <heiwa.database.User>`
	JWT is valid and could be decoded, but the claims contained within it are
	not. This can, for example, mean that it has expired.
	"""

	code = http.client.BAD_REQUEST


class APIAuthorizationJWTInvalidSubjectResourceType(APIException):
	"""Exception class for when a provided :class:`User <heiwa.database.User>`
	JWT is otherwise valid, but the resource type in its ``sub`` claim is not
	the user class name. This likely means it's generated by this service,
	but supposed to be used for something else.
	"""

	code = http.client.BAD_REQUEST


class APIAuthorizationJWTSubjectNotFound(APIException):
	"""Exception class for when a provided :class:`User <heiwa.database.User>`
	JWT and its claims are valid, but the user they represent does not exist.
	Usually, this means that user has been deleted.
	"""

	code = http.client.NOT_FOUND


class APICategoryNotFound(APIException):
	"""Exception class for when a requested
	:class:`Category <heiwa.database.Category>` does not exist.
	"""

	code = http.client.NOT_FOUND


class APICategoryUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Category <heiwa.database.Category>`, but all values are the
	exact same as the existing ones.
	"""

	code = http.client.FORBIDDEN


class APIFileExceededMaxTotalSize(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	upload a :class:`File <heiwa.database.File>`, but its size in bytes added to
	their current :attr:`file_total_size <heiwa.database.User.file_total_size>`
	exceeds that specified in the ``FILE_MAX_TOTAL_SIZE`` config key.
	"""

	code = http.client.FORBIDDEN


class APIFileNotAllowedType(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	upload a :class:`File <heiwa.database.File>`, but its media / MIME type
	is not allowed as per the config.
	"""

	code = http.client.FORBIDDEN


class APIFileNotFound(APIException):
	"""Exception class for when a :class:`File <heiwa.database.File>` was not
	found, after a user attempted to find it.
	"""

	code = http.client.NOT_FOUND


class APIFileUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to change a :class:`File <heiwa.database.File>`'s metadata or contents, but
	all values are the same as the existing ones.
	"""

	code = http.client.FORBIDDEN


class APIForumCategoryOutsideParent(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to assign a :class:`Category <heiwa.database.Category>` to a
	:class:`Forum <heiwa.database.Forum>`, while also assigning a parent forum
	whose :attr:`id <heiwa.database.Forum.id>` does not match the category's
	:attr:`forum_id <heiwa.database.Category.forum_id>`.
	"""

	code = http.client.FORBIDDEN


class APIForumChildLevelLimitReached(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to create a :class:`Forum <heiwa.database.Forum>` whose
	:attr:`child_level <heiwa.database.Forum.child_level>` is higher than the
	config's ``FORUM_MAX_CHILD_LEVEL`` key.
	"""

	code = http.client.FORBIDDEN


class APIForumNotFound(APIException):
	"""Exception class for when a requested :class:`Forum <heiwa.database.Forum>`
	does not exist.
	"""

	code = http.client.NOT_FOUND


class APIForumParentIsChild(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	assign a parent :class:`Forum <heiwa.database.Forum>`, but its ID is the same
	as the child forum's.
	"""

	code = http.client.FORBIDDEN


class APIForumPermissionsGroupMassUpdateInconsistent(APIException):
	"""Exception class for when a mass
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>` update
	would inevitably cause a primary key collision. For example, multiple existing
	instances being updated with the same new
	:attr:`forum_id <heiwa.database.ForumPermissionsGroup.forum_id>` and
	:attr:`group_id <heiwa.database.ForumPermissionsGroup.group_id>`.
	"""

	code = http.client.BAD_REQUEST


class APIForumPermissionsGroupNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to delete a :class:`Group <heiwa.database.Group>`'s permissions for a certain
	:class:`Forum <heiwa.database.Forum>`, but there are none.

	.. seealso::
		:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	"""

	code = http.client.NOT_FOUND


class APIForumPermissionsGroupUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to edit a :class:`Group <heiwa.database.Group>`'s permissions for a certain
	:class:`Forum <heiwa.database.Forum>`, but there are none.

	.. seealso::
		:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	"""

	code = http.client.FORBIDDEN


class APIForumPermissionsUserMassUpdateInconsistent(APIException):
	"""Exception class for when a mass
	:class:`ForumPermissionsUser <heiwa.database.ForumPermissionsUser>` update
	would inevitably cause a primary key collision. For example, multiple existing
	instances being updated with the same new
	:attr:`forum_id <heiwa.database.ForumPermissionsUser.forum_id>` and
	:attr:`user_id <heiwa.database.ForumPermissionsUser.user_id>`.
	"""

	code = http.client.BAD_REQUEST


class APIForumPermissionsUserNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to delete another user's permissions for a certain
	:class:`Forum <heiwa.database.Forum>`, but there are none.

	.. seealso::
		:class:`ForumPermissionsUser <heiwa.database.ForumPermissionsUser>`
	"""

	code = http.client.NOT_FOUND


class APIForumPermissionsUserUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts
	to edit another user's permissions for a certain
	:class:`Forum <heiwa.database.Forum>`, but all values are the exact same as
	the existing ones.

	.. seealso::
		:class:`ForumPermissionsUser <heiwa.database.ForumPermissionsUser>`
	"""

	code = http.client.FORBIDDEN


class APIForumSubscriptionAlreadyExists(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	subscribe to a :class:`Forum <heiwa.database.Forum>`, but has already done so
	before.

	.. seealso::
		:obj:`forum_subscribers <heiwa.database.forum_subscribers>`
	"""

	code = http.client.FORBIDDEN


class APIForumSubscriptionNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	unsubscribe from a :class:`Forum <heiwa.database.Forum>`, but there is no
	subscription in the first place.

	.. seealso::
		:obj:`forum_subscribers <heiwa.database.forum_subscribers>`
	"""

	code = http.client.NOT_FOUND


class APIForumUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Forum <heiwa.database.Forum>`, but all values are the exact
	same as the existing ones.
	"""

	code = http.client.FORBIDDEN


class APIGroupIsLastDefault(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete the last :class:`Group <heiwa.database.Group>` whose
	:attr:`default_for <heiwa.database.Group.default_for>` column contains ``*``,
	or edit it in such a way that it no longer contains that value there.
	"""

	code = http.client.FORBIDDEN


class APIGroupNotFound(APIException):
	"""Exception class for when a requested :class:`Group <heiwa.database.Group>`
	was not found.
	"""

	code = http.client.NOT_FOUND


class APIGroupPermissionsForLastDefault(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete permissions or leave permissions :data:`None` for the last
	:class:`Group <heiwa.database.Group>` whose
	:attr:`default_for <heiwa.database.Group.default_for>` column contains ``*``.

	.. seealso::
		:class:`GroupPermissions <heiwa.database.GroupPermissions>`
	"""

	code = http.client.FORBIDDEN


class APIGroupPermissionsNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete a :class:`Group <heiwa.database.Group>`'s permissions, but there are
	none.

	.. seealso::
		:class:`GroupPermissions <heiwa.database.GroupPermissions>`
	"""

	code = http.client.NOT_FOUND


class APIGroupPermissionsUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	change a :class:`Group <heiwa.database.Group>`'s permissions, but has not
	changed a single one of their values.
	"""

	code = http.client.FORBIDDEN


class APIGroupUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Group <heiwa.database.Group>`, but has not changed a single one
	of its attributes.
	"""

	code = http.client.FORBIDDEN


class APIGuestSessionLimitReached(APIException):
	"""Exception class for when a visitor attempts to obtain a guest
	:class:`User <heiwa.database.User>` account through the
	:mod:`guest <heiwa.views.guest>` endpoint, but too many guests have already
	registered with the same IP address within the specified amount of time.
	These values can be changed using the ``GUEST_MAX_SESSIONS_PER_IP`` and
	``GUEST_SESSION_EXPIRES_AFTER`` config values.
	"""

	code = http.client.FORBIDDEN


class APIJSONInvalid(APIException):
	"""Exception class for when the JSON data sent to an API endpoint which
	requires input validation did not pass it. This will usually also be raised
	with details about which data there was an issue with included in the details.

	.. seealso::
		:class:`heiwa.validators.APIValidator`
	"""

	code = http.client.BAD_REQUEST


class APIJSONMissing(APIException):
	"""Exception class for when an API endpoint was excepting to receive JSON
	data, but there was none.

	.. seealso::
		:func:`heiwa.validators.validate_json`
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountEmailAlreadyUsed(APIException):
	"""Exception class for when a user attempts to register or modify an existing
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but provides an email
	address that has already been used previously.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountEmailNeverVerified(APIException):
	"""Exception class for when a user attempts to log into their
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but has never verified
	it - no :class:`User <heiwa.database.User>` is associated with it and
	its :attr:`is_verified <heiwa.database.LocalAccount.email_is_verified>`
	attribute is :data:`False`.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountNotFound(APIException):
	"""Exception class for when a user attempts to find a
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but there is none.
	"""

	code = http.client.NOT_FOUND


class APILocalAccountPasswordInvalid(APIException):
	"""Exception class for when a user attempts to enter a
	:class:`LocalAccount <heiwa.database.LocalAccount>` password, but it does
	not match the existing hashed one.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountPasswordResetEmailSendingFailed(APIException):
	"""Exception class for when a user asks for a password reset message to be
	sent to their :class:`LocalAccount <heiwa.database.LocalAccount>`'s email
	address, but the sending fails.

	This can mean multiple things - more likely, the email address being on an
	inexistent domain or not present on an existing one. But since it can also
	mean a server fault, the ``503`` status code has been chosen.
	"""

	code = http.client.SERVICE_UNAVAILABLE


class APILocalAccountPasswordResetJWTInvalid(APIException):
	"""Exception class for whne a user attempts to reset their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s password, but the JWT
	used to do so is entirely invalid.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountPasswordResetJWTInvalidClaims(APIException):
	"""Exception class for when a user attempts to reset their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s password and present a
	valid JWT, but its claims are invalid.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountPasswordResetJWTInvalidScope(APIException):
	"""Exception class for when a user attempts to reset their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s password and present a
	valid JWT with valid claims, but the scope contained in the ``sub`` key is
	not ``password_reset``.

	.. warning::
		This, in theory, should never happen. If it does, there's a high chance
		something is very wrong.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountPasswordResetJWTInvalidSubjectResourceType(APIException):
	"""Exception class for when a user attempts to reset their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s password and present a
	valid JWT with valid claims, but the resource type contained in the ``sub``
	key is not the local account class name.

	.. warning::
		This, in theory, should never happen. If it does, there's a high chance
		something is very wrong.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountPasswordResetJWTSubjectMismatch(APIException):
	"""Exception class for when a user attempts to reset their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s password and present a
	valid JWT with valid claims, but the account ID in the ``sub`` claim does
	not match the one presented in other parts of the request.

	This can mean the account holder has access to another account's password
	reset token.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountTOTPAlreadyExists(APIException):
	"""Exception class for when a user attempts to set up a
	:class:`LocalAccountTOTP <heiwa.database.LocalAccountTOTP>`, but one already
	exists for the associated account.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountTOTPBackupCodeInvalid(APIException):
	"""Exception class for when a user attempts to enter a TOTP backup code for a
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but it does not match
	any of the known ones.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountTOTPCodeInvalid(APIException):
	"""Exception class for when a user attempts to enter a TOTP code for a
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but it's not valid. This
	usually just means an old or invalid code, but can signal replay attacks in
	rare cases.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountTOTPNotFound(APIException):
	"""Exception class for when a user attempts to delete the TOTP for their
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but there is none.
	"""

	code = http.client.NOT_FOUND


class APILocalAccountUnchanged(APIException):
	"""Exception class for when a user attempts to edit their
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but has not changed any
	of the existing values.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountVerificationAlreadyDone(APIException):
	"""Exception class for when a user attempts to verify their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s email address, but
	has done so already. This should only be checked when the presented JWT's
	signature is validated, to prevent others from being able to check this.
	"""

	code = http.client.FORBIDDEN


class APILocalAccountVerificationEmailSendingFailed(APIException):
	"""Exception class for when a user attempts to register a
	:class:`LocalAccount <heiwa.database.LocalAccount>`, but the email they enter
	cannot, for any reason, receive messages.

	This can mean multiple things - more likely, the email address being on an
	inexistent domain or not present on an existing one. But since it can also
	mean a server fault, the ``503`` status code has been chosen.
	"""

	code = http.client.SERVICE_UNAVAILABLE


class APILocalAccountVerificationJWTInvalid(APIException):
	"""Exception class for when a user attempts to verify their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s
	:attr:`email <heiwa.database.LocalAccount.email>` address, but the JWT
	presented for doing so is, for any reason, invalid.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountVerificationJWTInvalidClaims(APIException):
	"""Exception class for when a user attempts to verify their
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s
	:attr:`email <heiwa.database.LocalAccount.email>` address, but the presented
	JWT's claims are invalid.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountVerificationJWTInvalidScope(APIException):
	"""Exception class for when a user presents an otherwise valid JWT to verify
	:class:`LocalAccount <heiwa.database.LocalAccount>`'s
	:attr:`email <heiwa.database.LocalAccount.email>` address, but the presented
	scope identifier in the ``sub`` claim is incorrect. It should be
	``verification``.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountVerificationJWTInvalidSubjectResourceType(APIException):
	"""Exception class for when a user presents an otherwise valid JWT to verify
	their :class:`LocalAccount <heiwa.database.LocalAccount>`'s
	:attr:`email <heiwa.database.LocalAccount.email>` address, but the resource
	type identifier in the ``sub`` claim is not correct. It should the class
	name.
	"""

	code = http.client.BAD_REQUEST


class APILocalAccountVerificationJWTSubjectNotFound(APIException):
	"""Exception class for when a user presents a completely valid JWT to verify
	their :class:`LocalAccount <heiwa.database.LocalAccount>`'s
	:attr:`email <heiwa.database.LocalAccount.email>` address, but an
	:attr:`id <heiwa.database.LocalAccount.id>` corresponding to its subject
	(``sub``) was not found.
	"""

	code = http.client.NOT_FOUND


class APIMessageDirectoryNotFound(APIException):
	"""Exception class for when a requested
	:class:`MessageDirectory <heiwa.database.MessageDirectory>` was not found.
	"""

	code = http.client.NOT_FOUND


class APIMessageDirectoryUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	update a :class:`MessageDirectory <heiwa.database.MessageDirectory>`, but
	hasn't changed any of the existing values.
	"""

	code = http.client.FORBIDDEN


class APIMessageNotFound(APIException):
	"""Exception class for when a requested
	:class:`Message <heiwa.database.Message>` was not found.
	"""

	code = http.client.NOT_FOUND


class APIMessageReceiverBlockedSender(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	send a :class:`Message <heiwa.database.Message>` to another user, but they
	have been blocked.

	.. seealso::
		:obj:`heiwa.database.user_blocks`
	"""

	code = http.client.FORBIDDEN


class APIMessageUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Message <heiwa.database.Message>`, but has not changed a single
	one of its attributes.
	"""

	code = http.client.FORBIDDEN


class APINoPermission(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	perform an action they do not have permission to. For example, ``delete`` an
	instance of a :class:`Forum <heiwa.database.Forum>` when they are a default
	guest.

	.. seealso::
		:class:`heiwa.database.utils.PermissionControlMixin`
	"""

	code = http.client.UNAUTHORIZED


class APINotificationNotFound(APIException):
	"""Exception class for when a requested
	:class:`Notification <heiwa.database.Notification>` was not found.
	"""

	code = http.client.NOT_FOUND


class APIOIDCAuthenticationFailed(APIException):
	"""Exception class for when an authentication via OIDC failed for any
	reason. This can, for example, be the specified server not responding or
	returning invalid data.
	"""

	code = http.client.UNAUTHORIZED


class APIOIDCNonceInvalid(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` presents a
	correct state during an OIDC authentication process, but the nonce is
	invalid.
	"""

	code = http.client.BAD_REQUEST


class APIOIDCServiceNotFound(APIException):
	"""Exception class for when a requested OIDC service was not found."""

	code = http.client.NOT_FOUND


class APIOIDCStateInvalid(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` presents a
	correct nonce during an OIDC authentication process, but the state is
	invalid.
	"""

	code = http.client.BAD_REQUEST


class APIPostNotFound(APIException):
	"""Exception class for when a requested :class:`Post <heiwa.database.Post>`
	was not found.
	"""

	code = http.client.NOT_FOUND


class APIPostUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Post <heiwa.database.Post>`, but has not changed a single one
	of its attributes.
	"""

	code = http.client.FORBIDDEN


class APIPostVoteNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete their vote on a :class:`Post <heiwa.database.Post>`, but there is none
	to be found.

	.. seealso::
		:class:`heiwa.database.PostVote`
	"""

	code = http.client.NOT_FOUND


class APIPostVoteUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	change their vote on a :class:`Post <heiwa.database.Post>`, but has not
	changed any of its attributes. In this case, it will only be the
	:attr:`is_upvote <heiwa.database.PostVote.is_upvote>` attribute by default.

	.. seealso::
		:class:`heiwa.database.PostVote`
	"""

	code = http.client.FORBIDDEN


class APIRateLimitExceeded(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` has
	exceeded their rate limit for a specific API endpoint.

	.. seealso::
		:class:`heiwa.limiter.Limiter`
	"""

	code = http.client.TOO_MANY_REQUESTS


class APIStatisticNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	find a :class:`Statistic <heiwa.database.Statistic>` - by example, by its
	:attr:`id <heiwa.database.Statistic.id>` - but none was found.
	"""

	code = http.client.NOT_FOUND


class APIThreadLocked(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	create a :class:`Post <heiwa.database.Post>` in, edit, or otherwise interact
	with a locked :class:`Thread <heiwa.database.Thread>`.
	"""

	code = http.client.FORBIDDEN


class APIThreadNotFound(APIException):
	"""Exception class for when a requested
	:class:`Thread <heiwa.database.Thread>` was not found.
	"""

	code = http.client.NOT_FOUND


class APIThreadSubscriptionAlreadyExists(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	subscribe to a :class:`Thread <heiwa.database.Thread>`, but has already done
	so before.

	.. seealso::
		:obj:`heiwa.database.thread_subscribers`
	"""

	code = http.client.FORBIDDEN


class APIThreadSubscriptionNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	unsubscribe from a :class:`Thread <heiwa.database.Thread>`, but there is no
	subscription to be found.

	.. seealso::
		:obj:`heiwa.database.thread_subscribers`
	"""

	code = http.client.NOT_FOUND


class APIThreadUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a :class:`Thread <heiwa.database.Thread>`, but has not changed any of
	its attributes.
	"""

	code = http.client.FORBIDDEN


class APIThreadViewRecordExceededCooldown(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	add a view to a :class:`Thread <heiwa.database.Thread>`, but the time since
	they've last added one was too short. How short is too short is defined using
	the ``THREAD_VIEW_COOLDOWN`` config key. The time the user is allowed to add
	a view again should be included in the
	:attr:`details <.APIThreadViewRecordExceededCooldown.details>`.

	.. seealso::
		:class:`heiwa.database.ThreadViewRecord`
	"""

	code = http.client.FORBIDDEN


class APIThreadVoteNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete their vote on a :class:`Thread <heiwa.database.Thread>`, but there is
	none to be found.

	.. seealso::
		:class:`heiwa.database.ThreadVote`
	"""

	code = http.client.NOT_FOUND


class APIThreadVoteUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	change their vote on a :class:`Thread <heiwa.database.Thread>`, but has not
	changed any of its attributes. In this case, it will only be the
	:attr:`is_upvote <heiwa.database.ThreadVote.is_upvote>` attribute by default.

	.. seealso::
		:class:`heiwa.database.ThreadVote`
	"""

	code = http.client.FORBIDDEN


class APIUserAvatarNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete their (or someone else's) avatar, but there is none to be found.

	.. seealso::
		:meth:`heiwa.database.User.get_avatar`
	"""

	code = http.client.NOT_FOUND


class APIUserAvatarNotAllowedType(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	set an avatar, but its media type is not allowed. Which types are allowed is
	defined in the ``USER_AVATAR_TYPES`` config value.

	.. seealso::
		:meth:`heiwa.database.User.get_avatar`
	"""

	code = http.client.BAD_REQUEST


class APIUserAvatarUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	set an avatar, but it's the exact same as the previous one.
	"""

	code = http.client.FORBIDDEN


class APIUserBanAlreadyExists(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	ban another user, but there is a ban already present.

	.. seealso::
		:class:`heiwa.database.UserBan`
	"""

	code = http.client.FORBIDDEN


class APIUserBanNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	view a ban which does not exist or they do not have permission to view.

	.. seealso::
		:class:`heiwa.database.UserBan`
	"""

	code = http.client.NOT_FOUND


class APIUserBanUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit another user's ban, but has not changed a single one of its attributes.

	.. seealso::
		:class:`heiwa.database.UserBan`
	"""

	code = http.client.FORBIDDEN


class APIUserBanned(APIException):
	"""Exception class for when an authenticated
	:class:`User <heiwa.database.User>` attempts to access an API endpoint, but
	they have been banned.

	.. seealso::
		:attr:`heiwa.database.User.is_banned`

		:class:`heiwa.database.UserBan`
	"""

	code = http.client.FORBIDDEN


class APIUserBlockAlreadyExists(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	block another user, but has already done so before.

	.. seealso::
		:obj:`heiwa.database.user_blocks`
	"""

	code = http.client.FORBIDDEN


class APIUserBlockNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	unblock another user, but there is no block to be found.

	.. seealso::
		:obj:`heiwa.database.user_blocks`
	"""

	code = http.client.NOT_FOUND


class APIUserFollowAlreadyExists(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	follow another user, but has already done so in the past.

	.. seealso::
		:obj:`heiwa.database.user_follows`
	"""

	code = http.client.FORBIDDEN


class APIUserFollowNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	unfollow another user, but there is no follow to be found.

	.. seealso::
		:obj:`heiwa.database.user_follows`
	"""

	code = http.client.NOT_FOUND


class APIUserGroupAlreadyAdded(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	assign a :class:`Group <heiwa.database.Group>` to another user (or
	themselves), but it's already been assigned before.

	.. seealso::
		:obj:`heiwa.database.user_groups`
	"""

	code = http.client.FORBIDDEN


class APIUserGroupIsLastDefault(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	remove the last assigned :class:`Group <heiwa.database.Group>` whose
	:attr:`default_for <heiwa.database.Group.default_for>` column contains ``*``.

	.. seealso::
		:obj:`heiwa.database.user_groups`
	"""

	code = http.client.FORBIDDEN


class APIUserGroupNotAdded(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	remove a :class:`Group <heiwa.database.Group>` from another user (or
	themselves), but it's never been assigned in the first place.

	.. seealso::
		:obj:`heiwa.database.user_groups`
	"""

	code = http.client.FORBIDDEN


class APIUserNameNotUnique(APIException):
	"""Exception class for when :class:`User <heiwa.database.User>` names are
	required to be unique by the current app's config and a selected name is
	not unique.
	"""

	code = http.client.FORBIDDEN


class APIUserNotFound(APIException):
	"""Exception class for when a requested :class:`User <heiwa.database.User>`
	does not exist.
	"""

	code = http.client.NOT_FOUND


class APIUserPermissionsNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	delete another user's (or their own) permissions, but there are none to be
	found.

	.. seealso::
		:class:`heiwa.database.UserPermissions`
	"""

	code = http.client.NOT_FOUND


class APIUserPermissionsUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit another user's (or their own) permissions, but has not changed a single
	one of their values.

	.. seealso::
		:class:`heiwa.database.UserPermissions`
	"""

	code = http.client.FORBIDDEN


class APIUserPreferencesUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit their preferences, but has not changed a single one of their values.

	.. seealso::
		:class:`heiwa.database.UserPreferences`
	"""

	code = http.client.FORBIDDEN


class APIUserReputationExceededCooldown(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	create a reputation point for another user, but the time that has passed since
	then is shorter than the current limit.

	.. seealso::
		:class:`heiwa.database.UserReputation`
	"""

	code = http.client.FORBIDDEN


class APIUserReputationNotFound(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	find a reputation point - for example, by its ID - but there is none
	corresponding to the given identifier.

	.. seealso::
		:class:`heiwa.database.UserReputation`
	"""

	code = http.client.NOT_FOUND


class APIUserReputationReceiverBlockedSender:
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	give a reputation point to another user, but that user has blocked them.

	.. seealso::
		:class:`heiwa.database.UserReputation`
	"""

	code = http.client.FORBIDDEN


class APIUserReputationReceiverIsSelf(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	give a reputation point to themselves.

	.. seealso::
		:class:`heiwa.database.UserReputation`
	"""

	code = http.client.FORBIDDEN


class APIUserReputationUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit a reputation point, but has not changed a single one of its attributes.

	.. seealso::
		:class:`heiwa.database.UserReputation`
	"""

	code = http.client.FORBIDDEN


class APIUserUnchanged(APIException):
	"""Exception class for when a :class:`User <heiwa.database.User>` attempts to
	edit another user (or themselves), but has not changed a single one of their
	attributes.
	"""

	code = http.client.FORBIDDEN
