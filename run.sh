#!/bin/bash

##
# Reflects database models to the postgres database if this script is run for
# the first time, starts the scheduler in a separate process and runs the Heiwa
# app in a Gunicorn server, optimized for a 4-core system. (4 cores * 2 + 1)
#
# Gunicorn documentation: https://docs.gunicorn.org/en/latest/index.html
# Flask documentation, more options and information: https://flask.palletsprojects.com/en/2.0.x/patterns/appfactories/
##

# https://www.geeksforgeeks.org/bash-scripting-how-to-check-if-file-exists/

# Check if this script has already been run. If not, reflect database models
# and save that information for next time by creating an ``already_run.lock``
# file.
if [ ! -f already_run.lock ];
then
	FLASK_APP=heiwa python3.10 -m flask reflect &&
	touch already_run.lock
fi

# https://stackoverflow.com/a/13864829
# Thanks to Lionel and BSMP!

# If unset, do ``INFO`` by default
if [[ -z "${LOGGING_LEVEL+set}" ]]; then
	export LOGGING_LEVEL="INFO";
fi

# Run the scheduler in one process
python3.10 -c "import heiwa; heiwa.run_scheduler()" &

# Run the Gunicorn server in another
python3.10 -m gunicorn -w 9 -b :5001 heiwa:"create_app()" &

# https://docs.docker.com/config/containers/multi-service_container/

wait -n

# Exit with status of process that exited first
exit $?
