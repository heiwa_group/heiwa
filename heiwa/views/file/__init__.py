r"""API views for :class:`File <heiwa.database.File>`\ s.

.. warning::
	Ideally, files would never be handled all at once and read in chunks. This,
	however, is not possible with the simple way uploads are currently handled -
	a lot of memory may thus be used during uploads.

	*Note - I may come back to this in the future.*
"""

import http.client
import os
import typing
import uuid

import flask
import magic
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	get_config,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"check_file_changed",
	"create",
	"delete",
	"edit",
	"edit_contents",
	"file_blueprint",
	"get_file",
	"list_",
	"mass_delete",
	"mass_edit",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"validate_file_type",
	"view",
	"view_contents",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.0.0"

file_blueprint = flask.Blueprint(
	"file",
	__name__,
	url_prefix="/files"
)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"user_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"type": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.File.name.property.columns[0].type.length
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.File.name.property.columns[0].type.length
	},
	"size": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"$contents": {
		"type": "binary",
		"coerce": "decode_base64",
		"maxlength": get_config()["FILE_MAX_SINGLE_SIZE"]
	}
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"size"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=file_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(file_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"size": ATTR_SCHEMAS["size"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"user_id": ATTR_SCHEMAS["user_id"],
			"type": ATTR_SCHEMAS["type"],
			"name": ATTR_SCHEMAS["name"],
			"size": ATTR_SCHEMAS["size"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"type": {
				"type": "list",
				"schema": ATTR_SCHEMAS["type"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"size": {
				"type": "list",
				"schema": ATTR_SCHEMAS["size"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})

CHUNK_SIZE = 8192
"""The size of file read chunks used in the :func:`.check_file_changed`
function. 8 KiB.
"""


def get_file(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.File:
	"""Gets the file with the given ``id_``. If it doesn't exist or the ``user``
	does not have the permission to view it, an exception is raised instead.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file to find.
	:param session: The SQLAlchemy session to execute the selection query with.
		Defaults to :attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who should have
		permission to view the file. Defaults to :attr:`flask.g.user` if
		:data:`None`.

	:raises heiwa.exceptions.APIFileNotFound: Raised when the file does simply
		not exist, or the given user does not have permission to view it.

	:returns: The file.
	"""

	session, user = get_session_and_user_defaults(session, user)

	file_ = session.execute(
		database.File.get(
			session,
			user,
			conditions=(database.File.id == id_)
		)
	).scalars().one_or_none()

	if file_ is None:
		raise exceptions.APIFileNotFound

	return file_


def check_file_changed(
	old_file_location: str,
	new_file: bytes
) -> bool:
	"""Checks whether or not the ``old_file_location``'s contents and ``new_file``
	match.

	So as to avoid using a lot of memory, the comparison is done in chunks of
	8 KiB. This is also the reason why this is a separate function.

	:param old_file_location: The location of the old file.
	:param new_file: The new file, in the form of ``bytes``.

	:returns: The result of the check.
	"""

	position = 0

	with open(old_file_location, "rb") as old_file:
		while True:
			position += CHUNK_SIZE

			chunk = old_file.read(position)

			if len(chunk) == 0:
				return True

			if new_file[position - CHUNK_SIZE:position] != chunk:
				return False


def validate_file_type(
	file_: bytes,
	get: bool = False
) -> typing.Union[None, str]:
	"""Validates whether or not the given ``file_`` is one of the types given in
	the ``FILE_TYPES`` config key using ``libmagic``. If the validation is
	successful, gives back the guessed type.

	Only the first 4 KiB (or less) of the file are considered when its type is
	being guessed. This means large files will not unnecessarily use twice the
	memory space during the time they're processed.

	:param file_: The file's bytes.
	:param get: Whether or not to also return the guessed type.

	:raises heiwa.exceptions.APIFileNotAllowedType: Raised when the file is not
		one of the allowed media (MIME) types. The guessed type is given in the
		exception's details.

	:returns: The guessed type, if ``get`` is :data:`True`. Otherwise, returns
		:data:`None`.
	"""

	guessed_type = magic.from_buffer(
		file_[:4096],
		mime=True
	)

	if guessed_type not in flask.current_app.config["FILE_TYPES"]:
		raise exceptions.APIFileNotAllowedType(guessed_type)

	if get:
		return guessed_type

	return None  # R1710 inconsistent-return-statements


@file_blueprint.route("", methods=["POST"])
@validators.validate_json({
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"contents": {
		**ATTR_SCHEMAS["$contents"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.File)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a file with the given ``name`` and base64-encoded ``content``.

	:raises heiwa.exceptions.APIFileNotAllowedType: Raised when the guessed media
		(MIME) type of the file does not match any of those found in the
		``FILE_TYPES`` config key.
	:raises heiwa.exceptions.APIFileExceededMaxTotalSize: Raised when the current
		:attr:`flask.g.user`'s
		:attr:`file_total_size <heiwa.database.User.file_total_size>`, combined
		with the size of the file being uploaded, would exceed the limit set by
		the ``FILE_MAX_TOTAL_SIZE`` config key. The new total size is given in
		the exception's details.

	:returns: Metadata about the newly uploaded file (stored in the database),
		with the HTTP ``201`` status code. The file itself is not returned.
	"""

	guessed_type = validate_file_type(
		flask.g.json["contents"],
		get=True
	)

	size = len(flask.g.json["contents"])
	new_total_size = flask.g.user.file_total_size + size

	if new_total_size > flask.current_app.config["FILE_MAX_TOTAL_SIZE"]:
		raise exceptions.APIFileExceededMaxTotalSize(new_total_size)

	file_ = database.File.create(
		flask.g.sa_session,
		user_id=flask.g.user.id,
		type=guessed_type,
		name=flask.g.json["name"],
		size=size,
		write_kwargs={
			"contents": flask.g.json["contents"]
		}
	)

	flask.g.sa_session.commit()

	return flask.jsonify(file_), http.client.CREATED


@file_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.File)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets the metadata of all files that the current :attr:`flask.g.user` is
	allowed to view. If there is a filter requested, they must also match it.

	:returns: The list of file metadata, with the HTTP ``200`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.File
			)
		)

	files = flask.g.sa_session.execute(
		sqlalchemy.select(database.File).
		where(conditions).
		order_by(get_order_by_expression(database.File)).
		limit(flask.g.json["limit"]).
		offset(flask.g.json["offset"])
	).scalars().all()

	return flask.jsonify(files), http.client.OK


@file_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"name": ATTR_SCHEMAS["name"],
				"contents": ATTR_SCHEMAS["$contents"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.File)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all files :attr:`flask.g.user` is allowed to - including the
	contents. If there is a filter requested, they must also match it.

	:raises heiwa.exceptions.APIFileNotAllowedType: Raised when the contents are
		being updated, and their guessed media (MIME) does not match any of those
		found in the ``FILE_TYPES`` config key.
	:raises heiwa.exceptions.APIFileExceededMaxTotalSize: Raised when the contents
		are being updated and their total size, subtracted from the existing files'
		sizes and added to the current :attr:`flask.g.user`'s
		:attr:`file_total_size <heiwa.database.User.file_total_size>` would exceed
		the limit set in the ``FILE_MAX_TOTAL_SIZE`` config key. The new total
		size is given in the exception's details.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	not_updating_contents = ("contents" not in flask.g.json["values"])

	if not_updating_contents:
		validate_file_type(flask.g.json["values"]["contents"])

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.File
			)
		)

	files = database.File.get(
		flask.g.sa_session,
		flask.g.user,
		action_conditions=database.File.action_queries["edit"](flask.g.user),
		conditions=conditions,
		order_by=get_order_by_expression(database.File),
		limit=flask.g.json["limit"],
		offset=flask.g.json["offset"],
		pkeys_only=not_updating_contents
	)

	if not_updating_contents:
		flask.g.sa_session.execute(
			sqlalchemy.update(database.File).
			where(
				database.File.id.in_(files)
			).
			values(**flask.g.json["values"]).
			execution_options(synchronize_session="fetch")
		)
	else:
		size = len(flask.g.json["values"]["contents"])
		new_total_size = (
			flask.g.user.file_total_size
			+ (
				size - file_.size
				for file_ in files
			)
		)

		if new_total_size > flask.current_app.config["FILE_MAX_TOTAL_SIZE"]:
			raise exceptions.APIFileExceededMaxTotalSize(new_total_size)

		for file_ in files:
			# Don't need to calculate the size twice
			file_.size = size

			for key, value in flask.g.json["values"]:
				if key == "contents" or getattr(file_, key) == value:
					continue

				setattr(file_, key, value)

			if check_file_changed(
				file_.get_location(),
				flask.g.json["values"]["contents"]
			):
				file_.set_contents(flask.g.json["values"]["contents"])

	return flask.jsonify(None), http.client.NO_CONTENT


@file_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.File)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all files the current :attr:`flask.g.user` is allowed to, including
	their contents stored on the filesystem. If there is a filter requested, they
	must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.File
			)
		)

	for file_ in database.File.get(
		flask.g.sa_session,
		flask.g.user,
		action_conditions=database.File.action_queries["delete"](flask.g.user,),
		conditions=conditions,
		order_by=get_order_by_expression(database.File),
		limit=flask.g.json["limit"],
		offset=flask.g.json["offset"]
	):
		file_.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@file_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.File)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the file with the requested ``id_``'s metadata - its object stored
	in the database.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:returns: The metadata, with the HTTP ``200`` status code.

	.. seealso::
		To view the file's contents, :func:`.view_contents`
	"""

	return flask.jsonify(
		get_file(id_)
	), http.client.OK


@file_blueprint.route("/<uuid:id_>/contents", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.File)
def view_contents(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the file with the requested ``id_``'s contents - stored on the
	filesystem, instead of the database.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:returns: The contents, associated with their media (MIME) type and the
		HTTP ``200`` status code.
	"""

	file_ = get_file(id_)
	location = file_.get_location()

	return flask.send_file(
		location,
		mimetype=file_.type,
		as_attachment=True,
		attachment_filename=file_.name,
		last_modified=os.path.getmtime(location)
	), http.client.OK


@file_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json({
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.File)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the file with the requested ``id_``'s metadata - information about
	it stored in the database.

	Right now, that is only the :attr:`name <heiwa.database.File.name>`, but
	the endpoint is structured in the same way as other ``edit`` ones to keep
	access as universal as possible. :attr:`type <heiwa.database.File.type> and
	:attr:`size <heiwa.database.File.size>`` could both easily be faked and it's
	possible to guess them programatically, so editing them is not possible.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:raises heiwa.exceptions.APIFileUnchanged: Raised when no attributes of the
		file's metadata have been visibly changed.

	:returns: The updated file metadata, with the HTTP ``200`` status code.

	.. seealso::
		For editing the file's contents, :func:`.edit_contents`
	"""

	file_ = get_file(id_)

	validate_permission(
		flask.g.user,
		"edit",
		file_
	)

	handle_edit(
		file_,
		flask.g.json,
		exceptions.APIFileUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(file_), http.client.OK


@file_blueprint.route("/<uuid:id_>/contents", methods=["PUT"])
@validators.validate_json({
	"contents": {
		**ATTR_SCHEMAS["$contents"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.File)
def edit_contents(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the file with the requested ``id_``'s contents, stored on the
	filesystem.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:raises heiwa.exceptions.APIFileUnchanged: Raised when the contents of the
		file have not been changed.
	:raises heiwa.exceptions.APIFileExceededMaxTotalSize: Raised when the new
		:attr:`file_total_size <heiwa.database.User.file_total_size>` of the
		current :attr:`flask.g.user` would exceed the limit set in the
		``FILE_MAX_TOTAL_SIZE`` config key.

	:returns: :data:`None`, with the HTTP ``200`` status code.

	.. note::
		The updated contents are not returned so as to preserve bandwidth. This
		is also the case with other similar endpoints, including those for user
		avatars.
	"""

	file_ = get_file(id_)

	validate_permission(
		flask.g.user,
		"edit",
		file_
	)

	if not check_file_changed(
		file_.get_location(),
		flask.g.json["contents"]
	):
		raise exceptions.APIFileUnchanged

	new_size = len(flask.g.json["contents"])
	new_total_size = (
		flask.g.user.file_total_size
		+ new_size
		- file_.size
	)

	if new_total_size > flask.g.config["FILE_MAX_TOTAL_SIZE"]:
		raise exceptions.APIFileExceededMaxTotalSize(new_total_size)

	file_.size = new_size
	file_.set_contents(flask.g.json["contents"])

	return flask.jsonify(None), http.client.OK


@file_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.File)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the file with the requested ``id_``, including its contents.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	file_ = get_file(id_)

	validate_permission(
		flask.g.user,
		"delete",
		file_
	)

	file_.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@file_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.File)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the file with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.File.id>` of the file.

	:raises heiwa.exceptions.APIFileNotFound: Raised when the file was
		not found.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_file(
			flask.g.sa_session,
			flask.g.user,
			id_
		).get_allowed_instance_actions(flask.g.user)
	), http.client.OK


@file_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform of any file, irrespective of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.File.get_allowed_static_actions(flask.g.user)
	), http.client.OK
