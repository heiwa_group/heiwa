r"""API views for
:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`,
belonging under the base set of views for
:class:`Forum <heiwa.database.Forum>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_forum,
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	handle_edit,
	parse_search,
	requires_permission,
	validate_forum_exists,
	validate_permission,
	validate_group_exists
)
from .utils import (
	generate_permission_in_filter_schema_partial,
	PERMISSION_ATTR_SCHEMAS_PARTIAL,
	PERMISSION_CREATE_EDIT_SCHEMA,
	PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL,
	PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA
)

__all__ = [
	"ATTR_SCHEMAS",
	"delete",
	"delete_parsed_permissions_for_group_users",
	"edit",
	"forum_permissions_group_blueprint",
	"get_permissions",
	"search_schema",
	"search_schema_filter_max_in_length",
	"search_schema_registry",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]


forum_permissions_group_blueprint = flask.Blueprint(
	"permissions_group",
	__name__
)


def delete_parsed_permissions_for_group_users(
	ids: typing.Iterable[uuid.UUID],
	session: sqlalchemy.orm.Session
) -> None:
	r"""Deletes the
	:class:`ForumParsedPermissions <heiwa.database.ForumParsedPermissions>` for
	all users belonging to the :class:`Group <heiwa.database.Group>`\ s with the
	given ``ids``.

	:param ids: The :attr:`id <heiwa.database.Group.id>` of the groups.
	:param session: The SQLAlchemy session to execute the deletion query with.

	.. seealso::
		:obj:`heiwa.database.user_groups`
	"""

	session.execute(
		sqlalchemy.delete(database.ForumParsedPermissions).
		where(
			database.ForumParsedPermissions.user_id.in_(
				sqlalchemy.select(database.user_groups.c.user_id).
				where(database.user_groups.c.group_id.in_(ids))
			)
		)
	)


def get_permissions(
	forum_id: uuid.UUID,
	group_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None,
	validate_existence: bool = True
) -> database.ForumPermissionsGroup:
	"""Gets the group forum permissions with the given ``forum_id`` and
	``group_id``.

	:param forum_id: The
		:attr:`forum_id <heiwa.database.ForumPermissionsGroup.forum_id>` of the
		permissions to find.
	:param group_id: The
		:attr:`group_id <heiwa.database.ForumPermissionsGroup.group_id>` of the
		permissions to find.
	:param session: The session to find the permissions with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the permissions. Defaults to :attr:`flask.g.user` if :data:`None`.
	:param validate_existence: Whether or not to raise an exception when the
		permissions were not found. Defaults to :data:`True`.

	:raises heiwa.exceptions.APIForumPermissionsGroupNotFound: Raised when the
		permissions don't exist, or the ``user`` does not have permission to view
		them. Depends on ``validate_existence`` being :data:`True`.

	:returns: The permissions.
	"""

	session, user = get_session_and_user_defaults(session, user)

	permissions = session.execute(
		database.ForumPermissionsGroup.get(
			session,
			user,
			conditions=sqlalchemy.and_(
				database.ForumPermissionsGroup.forum_id == forum_id,
				database.ForumPermissionsGroup.group_id == group_id
			)
		)
	).scalars().one_or_none()

	if validate_existence and permissions is None:
		raise exceptions.APIForumPermissionsGroupNotFound

	return permissions


ATTR_SCHEMAS = {
	"group_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	**PERMISSION_ATTR_SCHEMAS_PARTIAL
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=forum_permissions_group_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(
		forum_permissions_group_blueprint.name
	)
)

search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"group_id": ATTR_SCHEMAS["group_id"],
			**PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"group_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["group_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			**generate_permission_in_filter_schema_partial(
				search_schema_filter_max_in_length
			)
		},
		"maxlength": 1
	}
})


@forum_permissions_group_blueprint.route(
	"/<uuid:forum_id>/permissions/groups/<uuid:group_id>",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.ForumPermissionsGroup)
def view(
	forum_id: uuid.UUID,
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets the
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>` for the
	group with the given ``group_id`` and the forum with the given ``forum_id``.

	:param forum_id: The :attr:`id <heiwa.database.Forum.id>` of the forum.
	:param group_id: The :attr:`id <heiwa.database.User.id>` of the user.

	:returns: The permissions, with the ``200`` HTTP status code. If there aren't
		any, :data:`None` with the same status is returned instead.
	"""

	validate_forum_exists(forum_id)

	validate_group_exists(group_id)

	return flask.jsonify(
		get_permissions(
			forum_id,
			group_id,
			validate_existence=False
		)
	), http.client.OK


@forum_permissions_group_blueprint.route(
	"/<uuid:forum_id>/permissions/groups/<uuid:group_id>",
	methods=["PUT"]
)
@validators.validate_json(PERMISSION_CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.ForumPermissionsGroup)
def edit(
	forum_id: uuid.UUID,
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	r"""Updates the
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>` for the
	group with the given ``group_id`` and the forum with the given ``forum_id``.
	If they don't exist, they're automatically created instead.

	Parsed forum permissions for all the group's
	:class:`User <heiwa.database.User>`\ s are deleted.

	:param forum_id: The :attr:`id <heiwa.database.Forum.id>` of the forum.
	:param group_id: The :attr:`id <heiwa.database.User.id>` of the user.

	:returns: The newly updated or created permissions. If created, the HTTP
		status code is ``201``. Otherwise, it's ``200``.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`

		:obj:`heiwa.database.user_groups`
	"""

	forum = get_forum(forum_id)

	validate_group_exists(group_id)

	permissions = get_permissions(
		forum_id,
		group_id,
		validate_existence=False
	)

	if permissions is None:
		validate_permission(
			flask.g.user,
			"create",
			database.ForumPermissionsGroup,
			forum=forum
		)

		permissions = database.ForumPermissionsGroup.create(
			flask.g.sa_session,
			forum_id=forum.id,
			group_id=group_id,
			**flask.g.json
		)

		status = http.client.CREATED
	else:
		validate_permission(
			flask.g.user,
			"edit",
			permissions
		)

		handle_edit(
			permissions,
			flask.g.json,
			exceptions.APIForumPermissionsGroupUnchanged,
			current_utc_time=flask.g.current_utc_time
		)

		status = http.client.OK

	delete_parsed_permissions_for_group_users(
		(permissions.group_id,),
		flask.g.sa_session
	)

	flask.g.sa_session.commit()

	return flask.jsonify(permissions), status


@forum_permissions_group_blueprint.route(
	"/<uuid:forum_id>/permissions/groups/<uuid:group_id>",
	methods=["DELETE"]
)
@validators.validate_json(PERMISSION_CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.ForumPermissionsGroup)
def delete(
	forum_id: uuid.UUID,
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	r"""Deletes the
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>` for the
	group with the given ``group_id`` and the forum with the given ``forum_id``.

	Parsed forum permissions for the group's
	:class:`User <heiwa.database.User>`\ s are deleted, as reparsing them could
	take a significant amount of time depending on how many there are.

	:param forum_id: The :attr:`id <heiwa.database.Forum.id>` of the forum.
	:param group_id: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIForumPermissionsGroupNotFound: Raised when there
		are no set forum permissions for the given user, or the current user does
		not have the permission to view them.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`

		:obj:`heiwa.database.user_groups`
	"""

	validate_forum_exists(forum_id)

	validate_group_exists(group_id)

	permissions = get_permissions(
		forum_id,
		group_id
	)

	validate_permission(
		flask.g.user,
		"delete",
		permissions
	)

	permissions.delete()

	delete_parsed_permissions_for_group_users(
		(permissions.group_id,),
		flask.g.sa_session
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@forum_permissions_group_blueprint.route(
	"/permissions/groups",
	methods=["QUERY"]
)
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.ForumPermissionsGroup)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Lists all
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	instances that the current :attr:`flask.g.user` has the permission to view.
	If there is a filter requested, they must also match it.

	:returns: The list of groups' forum permissions, with a ``200`` HTTP status
		code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.ForumPermissionsGroup
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.ForumPermissionsGroup.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.ForumPermissionsGroup),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@forum_permissions_group_blueprint.route(
	"/permissions/groups",
	methods=["PUT"]
)
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"schema": {
				"forum_id": ATTR_SCHEMAS["forum_id"],
				"group_id": ATTR_SCHEMAS["group_id"],
				**PERMISSION_CREATE_EDIT_SCHEMA
			},
			"minlength": 1
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.ForumPermissionsGroup)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	instances that the current :attr:`flask.g.user` is allowed to. If there is
	a filter requested, they must also match it.

	All old parsed permissions for the related forums are deleted.

	:raises heiwa.exceptions.APIForumPermissionsGroupMassUpdateInconsistent:
		Raised when the update would inevitably cause a primary key collision.
		For example, multiple existing instances being updated with the same new
		:attr:`forum_id <heiwa.database.ForumPermissionsGroup.forum_id>` and
		:attr:`group_id <heiwa.database.ForumPermissionsGroup.group_id>`.

	:returns: :data:`None`, with a HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.ForumPermissionsGroup
			)
		)

	ids = flask.g.sa_session.execute(
		database.ForumPermissionsGroup.get(
			flask.g.sa_session,
			flask.g.user,
			conditions=conditions,
			action_conditions=(
				database.ForumPermissionsGroup.action_queries["edit"](flask.g.user)
			),
			order_by=get_order_by_expression(database.ForumPermissionsGroup),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).all()

	if len(ids) != 0:
		found_inconsistency = False

		group_ids = (
			group_id
			for forum_id, group_id in ids
		)

		if (
			"forum_id" in flask.g.json["values"] and
			"group_id" in flask.g.json["values"]
		):
			if len(ids) != 1:
				found_inconsistency = True

			if (
				flask.g.sa_session.execute(
					sqlalchemy.select(database.ForumPermissionsGroup.forum_id).
					where(
						sqlalchemy.tuple_(
							database.ForumPermissionsGroup.forum_id,
							database.ForumPermissionsGroup.group_id
						) == (
							flask.g.json["values"]["forum_id"],
							flask.g.json["values"]["group_id"]
						)
					).
					exists().
					select()
				).scalars().one()
			):
				found_inconsistency = True
		elif "forum_id" in flask.g.json["values"]:
			if flask.g.sa_session.execute(
				sqlalchemy.select(database.ForumPermissionsGroup.forum_id).
				where(
					sqlalchemy.and_(
						database.ForumPermissionsGroup.forum_id.in_(
							forum_id
							for forum_id, group_id in ids
						),
						database.ForumPermissionsGroup.group_id.in_(
							sqlalchemy.select(database.ForumPermissionsGroup.group_id).
							where(
								database.ForumPermissionsGroup.forum_id
								== flask.g.json["values"]["forum_id"]
							)
						)
					)
				).
				exists().
				select()
			).scalars().one():
				found_inconsistency = True
		elif (
			"group_id" in flask.g.json["values"] and
			flask.g.sa_session.execute(
				sqlalchemy.select(database.ForumPermissionsGroup.forum_id).
				where(
					sqlalchemy.and_(
						database.ForumPermissionsGroup.forum_id.in_(
							sqlalchemy.select(database.ForumPermissionsGroup.forum_id).
							where(
								database.ForumPermissionsGroup.group_id
								== flask.g.json["values"]["group_id"]
							)
						),
						database.ForumPermissionsGroup.group_id.in_(group_ids)
					)
				).
				exists().
				select()
			).scalars().one()
		):
			found_inconsistency = True

		if found_inconsistency:
			raise exceptions.APIForumPermissionsGroupMassUpdateInconsistent

		flask.g.sa_session.execute(
			sqlalchemy.update(database.ForumPermissionsGroup).
			where(
				sqlalchemy.tuple_(
					database.ForumPermissionsGroup.forum_id,
					database.ForumPermissionsGroup.group_id
				).in_(ids)
			).
			values(**flask.g.json["values"])
		)

		delete_parsed_permissions_for_group_users(
			group_ids,
			flask.g.sa_session
		)

	return flask.jsonify(None), http.client.OK


@forum_permissions_group_blueprint.route(
	"/permissions/groups",
	methods=["DELETE"]
)
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.ForumPermissionsGroup)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Delete all
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	instances that the current :attr:`flask.g.user` has the permission to. If
	there is a filter requested, they must also match it.

	All old parsed permissions for the related forums are deleted.

	:returns: :data:`None`, with a HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.ForumPermissionsGroup
			)
		)

	ids = flask.g.sa_session.execute(
		database.ForumPermissionsGroup.get(
			flask.g.sa_session,
			flask.g.user,
			conditions=conditions,
			action_conditions=(
				database.ForumPermissionsGroup.action_queries["delete"](flask.g.user)
			),
			order_by=get_order_by_expression(database.ForumPermissionsGroup),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).all()

	if len(ids) != 0:
		flask.g.sa_session.execute(
			sqlalchemy.delete(database.ForumPermissionsGroup).
			where(
				sqlalchemy.tuple_(
					database.ForumPermissionsGroup.forum_id,
					database.ForumPermissionsGroup.group_id
				).in_(ids)
			)
		)

		delete_parsed_permissions_for_group_users(
			(
				group_id
				for forum_id, group_id in ids
			),
			flask.g.sa_session
		)

	return flask.jsonify(None), http.client.OK


@forum_permissions_group_blueprint.route(
	"/<uuid:forum_id>/permissions/groups/<uuid:group_id>/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.ForumPermissionsGroup)
def view_allowed_actions_instance(
	forum_id: uuid.UUID,
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on the
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>` for the
	group with the given ``group_id`` and the forum with the given ``forum_id``.

	The user must at least be able to perform the static ``view`` action on
	these objects.

	:param forum_id: The :attr:`id <heiwa.database.Forum.id>` of the forum.
	:param group_id: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIForumPermissionsGroupNotFound: Raised when there
		are no set forum permissions for the given user, thus no allowed actions
		can exist.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	validate_forum_exists(forum_id)

	validate_group_exists(group_id)

	return flask.jsonify(
		get_permissions(
			forum_id,
			group_id
		).get_allowed_instance_actions(flask.g.user)
	), http.client.OK


@forum_permissions_group_blueprint.route(
	"/permissions/groups/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	:class:`ForumPermissionsGroup <heiwa.database.ForumPermissionsGroup>`
	instances, regardless of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.ForumPermissionsGroup.get_allowed_static_actions(flask.g.user)
	), http.client.OK
