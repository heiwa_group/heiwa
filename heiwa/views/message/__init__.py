r"""API views for :class:`Message <heiwa.database.Message>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	get_user,
	handle_edit,
	parse_search,
	requires_permission,
	validate_user_exists,
	validate_permission
)
from .directory import message_directory_blueprint
from .utils import validate_message_directory_exists

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_AND_PARTIAL_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"get_message",
	"list_",
	"mass_delete",
	"mass_edit",
	"message_blueprint",
	"message_directory_blueprint",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.2.1"

message_blueprint = flask.Blueprint(
	"message",
	__name__,
	url_prefix="/messages"
)

message_blueprint.register_blueprint(message_directory_blueprint)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"sender_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"receiver_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"sender_directory_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"receiver_directory_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"is_read": {
		"type": "boolean"
	},
	"encrypted_session_key": {
		"type": "binary",
		"coerce": "decode_base64",
		"length_divisible_by": 16,
		"minlength": 128,  # encrypted with 1024-bit RSA
		"maxlength": 512   # encrypted with 4096-bit RSA
	},
	"iv": {
		"type": "binary",
		"coerce": "decode_base64",
		"minlength": 16,
		"maxlength": 16
	},
	"tag": {
		"type": "string",
		"minlength": 1,  # Both values are completely arbitrary.
		"maxlength": database.Message.tag.property.columns[0].type.length
	},
	"encrypted_content": {
		"type": "binary",
		"coerce": "decode_base64",
		"length_divisible_by": 16,
		"minlength": 16,  # Min. AES-CBC length, block size 16
		"maxlength": 262160  # ~65536 character message with each char being 4 bytes
	}
}

CREATE_AND_PARTIAL_EDIT_SCHEMA = {
	"receiver_id": {
		**ATTR_SCHEMAS["receiver_id"],
		"required": True
	},
	"sender_directory_id": {
		**ATTR_SCHEMAS["sender_directory_id"],
		"nullable": True,
		"required": True
	},
	"encrypted_session_key": {
		**ATTR_SCHEMAS["encrypted_session_key"],
		"required": True
	},
	"iv": {
		**ATTR_SCHEMAS["iv"],
		"required": True
	},
	"tag": {
		**ATTR_SCHEMAS["tag"],
		"nullable": True,
		"required": True
	},
	"encrypted_content": {
		**ATTR_SCHEMAS["encrypted_content"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=message_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(message_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"sender_id": ATTR_SCHEMAS["sender_id"],
			"receiver_id": ATTR_SCHEMAS["receiver_id"],
			"sender_directory_id": {
				**ATTR_SCHEMAS["sender_directory_id"],
				"nullable": True
			},
			"is_read": ATTR_SCHEMAS["is_read"],
			"encrypted_session_key": ATTR_SCHEMAS["encrypted_session_key"],
			"iv": ATTR_SCHEMAS["iv"],
			"tag": {
				**ATTR_SCHEMAS["tag"],
				"nullable": True
			},
			"encrypted_content": ATTR_SCHEMAS["encrypted_content"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"sender_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["sender_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"receiver_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["receiver_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"sender_directory_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["sender_directory_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"encrypted_session_key": {
				"type": "list",
				"schema": ATTR_SCHEMAS["encrypted_session_key"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"iv": {
				"type": "list",
				"schema": ATTR_SCHEMAS["iv"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"tag": {
				"type": "list",
				"schema": ATTR_SCHEMAS["tag"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"encrypted_content": {
				"type": "list",
				"schema": ATTR_SCHEMAS["encrypted_content"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	}
})


def get_message(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Message:
	"""Gets the message with the given ``id_``. If it doesn't exist or the
	``user`` does not have permission to view it, an exception is raised instead.

	:param id_: The :attr:`id <heiwa.database.Message.id>` of the message
		to find.
	:param session: The SQLAlchemy session to execute the selection query with.
		Defaults to :attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who should have
		permission to view the message. Defaults to :attr:`flask.g.user` if
		:data:`None`.

	:raises heiwa.exceptions.APIMessageNotFound: Raised when the message does
		simply not exist, or the given user does not have permission to view it.

	:returns: The message.
	"""

	session, user = get_session_and_user_defaults(session, user)

	message = session.execute(
		database.Message.get(
			session,
			user,
			conditions=(database.Message.id == id_)
		)
	).scalars().one_or_none()

	if message is None:
		raise exceptions.APIMessageNotFound

	return message


@message_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_AND_PARTIAL_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Message)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a message with the given ``receiver_id``, ``encrypted_session_key``,
	``iv``, ``tag`` and ``encrypted_content``.

	:raises heiwa.exceptions.APIMessageReceiverBlockedSender: Raised when the
		message's receiver exists, but has blocked the current
		:attr:`flask.g.user`.

	:returns: The newly created message, with the HTTP ``201`` status code.
	"""

	if flask.g.json["sender_directory_id"] is not None:
		validate_message_directory_exists(flask.g.json["sender_directory_id"])

	user = get_user(flask.g.json["receiver_id"])

	if user.get_is_blockee(flask.g.user.id):
		raise exceptions.APIMessageReceiverBlockedSender

	message = database.Message.create(
		flask.g.sa_session,
		sender_id=flask.g.user.id,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(message), http.client.CREATED


@message_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Message)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all messages that the current :attr:`flask.g.user` is allowed to
	view. If there is a filter requested, they must also match it.

	:returns: The list of messages, with the HTTP ``200`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Message
			)
		)

	messages = flask.g.sa_session.execute(
		sqlalchemy.select(database.Message).
		where(conditions).
		order_by(get_order_by_expression(database.Message)).
		limit(flask.g.json["limit"]).
		offset(flask.g.json["offset"])
	).scalars().all()

	return flask.jsonify(messages), http.client.OK


@message_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"receiver_id": ATTR_SCHEMAS["receiver_id"],
				"is_read": ATTR_SCHEMAS["is_read"],
				"encrypted_session_key": ATTR_SCHEMAS["encrypted_session_key"],
				"iv": ATTR_SCHEMAS["iv"],
				"tag": {
					**ATTR_SCHEMAS["tag"],
					"nullable": True
				},
				"encrypted_content": ATTR_SCHEMAS["encrypted_content"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Message)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all messages :attr:`flask.g.user` is allowed to.

	This can differ based on the ``is_read`` attribute - if it's being changed,
	the only changeable attribute is that, only on received messages. Otherwise,
	all other attributes can be changed on sent messages.

	If there is a filter requested, the messages must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.

	.. note::
		Changing the receiver ID originally wasn't allowed, but there are edge
		cases where it could be helpful - like a user migrating to a new account
		where they've kept their previous public key they also use elsewhere.
		And since there isn't any real reason to prevent users from doing it,
		it's now allowed.
	"""

	conditions = True

	if "receiver_id" in flask.g.json["values"]:
		validate_user_exists(flask.g.json["values"]["receiver_id"])

	if flask.g.json["values"].get("sender_directory_id") is not None:
		validate_message_directory_exists(
			flask.g.json["values"]["sender_directory_id"]
		)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Message
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Message).
		where(
			database.Message.id.in_(
				database.Message.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.Message.action_queries["edit"](
						flask.g.user,
						attrs=flask.g.json["values"]
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.Message),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@message_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Message)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all messages the current :attr:`flask.g.user` is allowed to. If
	there is a filter requested, they must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.

	.. note::
		The choice of allowing receivers to delete messages may be controversial.
		However, especially in one-on-one chats (which are the only ones currently
		supported), the element of extra privacy outweighs edge cases where being
		able to completely delete messages sent to you could be undesirable.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Message
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.Message).
		where(
			database.Message.id.in_(
				database.Message.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.Message.action_queries["delete"](flask.g.user),
					conditions=conditions,
					order_by=get_order_by_expression(database.Message),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@message_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Message)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the message with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Message.id>` of the message.

	:returns: The message, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_message(id_)
	), http.client.OK


@message_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json({
	"is_read": {
		**ATTR_SCHEMAS["is_read"],
		"required": True
	},
	**CREATE_AND_PARTIAL_EDIT_SCHEMA
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Message)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the message with the requested ``id_``.

	If ``is_read`` is being updated, the message must be a received one, of which
	no other attributes can be changed. Otherwise, the message must be a sent
	one, where all other attributes can be changed.

	:param id_: The :attr:`id <heiwa.database.Message.id>` of the message.

	:returns: The updated message, with the HTTP ``200`` status code.
	"""

	message = get_message(id_)

	validate_permission(
		flask.g.user,
		"edit",
		message,
		attrs=flask.g.json
	)

	if flask.g.json["sender_directory_id"] is not None:
		validate_message_directory_exists(flask.g.json["sender_directory_id"])

	handle_edit(
		message,
		flask.g.json,
		exceptions.APIMessageUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(message), http.client.OK


@message_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Message)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the message with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Message.id>` of the message.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	message = get_message(id_)

	validate_permission(
		flask.g.user,
		"delete",
		message
	)

	message.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@message_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Message)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the message with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Message.id>` of the message.

	:raises heiwa.exceptions.APIMessageNotFound: Raised when the message was
		not found.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_message(
			flask.g.sa_session,
			flask.g.user,
			id_
		).get_allowed_instance_actions(flask.g.user)
	), http.client.OK


@message_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform of any message, irrespective of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.Message.get_allowed_static_actions(flask.g.user)
	), http.client.OK
