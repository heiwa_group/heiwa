"""Utilities specific to the :class:`Forum <heiwa.database.Forum>` views, but
not only a single module.
"""

import typing

from heiwa import constants, database

from ..utils import (
	PERMISSION_KEY_ATTR_SCHEMA,
	PERMISSION_KEY_CREATE_EDIT_SCHEMA
)

__all__ = [
	"PERMISSION_ATTR_SCHEMAS_PARTIAL",
	"PERMISSION_CREATE_EDIT_SCHEMA",
	"PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL",
	"PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA",
	"generate_permission_in_filter_schema_partial"
]

PERMISSION_ATTR_SCHEMAS_PARTIAL = {
	"forum_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	**{
		key: PERMISSION_KEY_ATTR_SCHEMA
		for key in database.ForumPermissionMixin.DEFAULT_PERMISSIONS
	}
}
"""A schema of attributes which can be used in both group and user forum
permissions, once ``group_id`` or ``user_id`` is added.

.. seealso::
	:class:`heiwa.database.ForumPermissionsGroup`

	:class:`heiwa.database.ForumPermissionsUser`
"""


PERMISSION_CREATE_EDIT_SCHEMA = {
	key: PERMISSION_KEY_CREATE_EDIT_SCHEMA
	for key in database.ForumPermissionMixin.DEFAULT_PERMISSIONS
}
"""A Cerberus schema used in the creation of forum group and user permissions.
Since forum IDs and user / group IDs will be specified elsewhere, they're not
included and this schema isn't treated as partial.

.. seealso::
	:class:`heiwa.database.ForumPermissionsGroup`

	:class:`heiwa.database.ForumPermissionsUser`
"""


PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL = {
	"forum_id": PERMISSION_ATTR_SCHEMAS_PARTIAL["forum_id"],
	"creation_timestamp": PERMISSION_ATTR_SCHEMAS_PARTIAL["creation_timestamp"],
	"edit_timestamp": {
		**PERMISSION_ATTR_SCHEMAS_PARTIAL["edit_timestamp"],
		"nullable": True
	},
	"edit_count": PERMISSION_ATTR_SCHEMAS_PARTIAL["edit_count"],
	**{
		key: PERMISSION_ATTR_SCHEMAS_PARTIAL[key]
		for key in database.ForumPermissionMixin.DEFAULT_PERMISSIONS
	}
}
"""A Cerberus schema used in filtering with the ``$equals`` rule. This can be
used in both user and group permissions, but needs to have additional specific
attributes added to encompass the entire model.

.. seealso::
	:class:`heiwa.database.ForumPermissionsGroup`

	:class:`heiwa.database.ForumPermissionsUser`
"""


PERMISSION_LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": (
		PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL["creation_timestamp"]
	),
	"edit_timestamp": PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL["edit_timestamp"],
	"edit_count": PERMISSION_EQUAL_FILTER_SCHEMA_PARTIAL["edit_count"]
}
"""A Cerberus schema used in filtering with the ``$less_than``,
``$greater_than`` and other similar rules. This can be used in both user and
group permissions, as these attributes don't differ.

.. seealso::
	:class:`heiwa.database.ForumPermissionsGroup`

	:class:`heiwa.database.ForumPermissionsUser`
"""


def generate_permission_in_filter_schema_partial(
	max_list_length: int
) -> typing.Dict:
	"""Generates a Cerberus schema used in filtering with the ``$in`` rule. This
	can be used in both user and group permissions, but needs to have additional
	specific attributes added to encompass the entire model.

	:param max_list_length: The maximum length of all lists.

	.. seealso::
		:class:`heiwa.database.ForumPermissionsGroup`

		:class:`heiwa.database.ForumPermissionsUser`
	"""

	return {
		"forum_id": {
			"type": "list",
			"schema": PERMISSION_ATTR_SCHEMAS_PARTIAL["forum_id"],
			"minlength": 2,
			"maxlength": max_list_length
		},
		"creation_timestamp": {
			"type": "list",
			"schema": PERMISSION_ATTR_SCHEMAS_PARTIAL["creation_timestamp"],
			"minlength": 2,
			"maxlength": max_list_length
		},
		"edit_timestamp": {
			"type": "list",
			"schema": {
				**PERMISSION_ATTR_SCHEMAS_PARTIAL["edit_timestamp"],
				"nullable": True
			},
			"minlength": 2,
			"maxlength": max_list_length
		},
		"edit_count": {
			"type": "list",
			"schema": PERMISSION_ATTR_SCHEMAS_PARTIAL["edit_count"],
			"minlength": 2,
			"maxlength": max_list_length
		},
		**{
			key: {
				"type": "list",
				"schema": PERMISSION_ATTR_SCHEMAS_PARTIAL[key],
				"minlength": 2,
				"maxlength": max_list_length
			}
			for key in database.ForumPermissionMixin.DEFAULT_PERMISSIONS
		}
	}
