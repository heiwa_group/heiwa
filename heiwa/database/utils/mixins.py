"""Mixins for SQLAlchemy models."""

from __future__ import annotations

import datetime
import logging
import typing

import sqlalchemy
import sqlalchemy.orm

from heiwa import constants

from .generators import get_uuid
from .types import UUID

__all__ = [
	"BasePermissionMixin",
	"CDWMixin",
	"CreationTimestampMixin",
	"EditInfoMixin",
	"IdMixin",
	"PermissionControlMixin",
	"ReprMixin"
]


@sqlalchemy.orm.declarative_mixin
class CDWMixin:
	"""A mixin used to simplify the creation and deletion of objects."""

	@classmethod
	def create(
		cls,
		session: sqlalchemy.orm.Session,
		*args,
		write_kwargs: typing.Union[
			None,
			typing.Dict[str, typing.Any]
		] = None,
		**kwargs
	):
		"""Creates an instance of the mixed-in class with the provided arguments,
		and calls the :meth:`write <.CDWMixin.write>` method.

		:param session: The session to use for the
			:meth:`write <CDWMixin.write>` method.

		:returns: The newly created instance.
		"""

		self = cls(
			*args,
			**kwargs
		)

		if write_kwargs is None:
			write_kwargs = {}

		self.write(session, **write_kwargs)

		return self

	def write(
		self,
		session: sqlalchemy.orm.Session
	) -> None:
		"""Adds the current instance of the mixed-in class to the provided
		``session`` and logs the function being run, if the class is also a
		subclass of :class:`.LoggerMixin`.

		:param session: The session to add the new instance to.
		"""

		session.add(self)
		session.flush()

		# Add first, so we have IDs and such
		if isinstance(self, LoggerMixin):
			self.logger.debug(
				"Added %s to session %s",
				self,
				session
			)

	def delete(self) -> None:
		"""Deletes the current instance of the mixed-in class from its current
		session and logs the function being run, if the class is also a subclass
		of :class:`.LoggerMixin`.
		"""

		session = sqlalchemy.orm.object_session(self)

		session.delete(self)

		if isinstance(self, LoggerMixin):
			self.logger.debug(
				"Deleted %s from the %s session",
				self,
				session
			)


class LoggerMixin:
	"""A utility mixin used for logging. Adds the ``logger`` attribute."""

	def __init__(self, *args, **kwargs) -> None:
		r"""Creates a :class:`Logger <logging.Logger>` at ``self.logger``, then
		calls the superclass' ``__init__``.
		"""

		self.logger = logging.getLogger(__name__)

		super().__init__(*args, **kwargs)

	@sqlalchemy.orm.reconstructor
	def sa_init(self):
		r"""Creates a :class:`Logger <logging.Logger>` at ``self.logger``, then
		calls the superclass' ``sa_init``, if it has that method.
		"""

		self.logger = logging.getLogger(__name__)

		superclass = super()

		if hasattr(superclass, "sa_init"):
			superclass.sa_init()


class PermissionControlMixin:
	"""A utility mixin used to handle permissions."""

	def __init__(self, *args, **kwargs) -> None:
		r"""Runs the :meth:`permission_control_init <.PermissionControlMixin.pe\
		rmission_control_init>` method, then calls the superclass' ``__init__``.
		"""

		self.permission_control_init()

		super().__init__(*args, **kwargs)

	@sqlalchemy.orm.reconstructor
	def sa_init(self):
		r"""Runs the :meth:`permission_control_init <.PermissionControlMixin.pe\
		rmission_control_init>` method, then calls the superclass' ``sa_init``,
		if it has that method.
		"""

		self.permission_control_init()

		superclass = super()

		if hasattr(superclass, "sa_init"):
			superclass.sa_init()

	def permission_control_init(self) -> None:
		"""Adds the following attributes:

		:attr:`instance_actions <.PermissionControlMixin.instance_actions>`:

			The actions a user is / isn't allowed to perform on one given instance
			of the mixed-in object. Unlike
			:attr:`static_actions <.PermissionControlMixin.static_actions>`, this
			can and usually should vary with each instance.

			.. seealso::
				:meth:`.PermissionControlMixin.get_allowed_instance_actions`

			.. warning::
				Input validation is not handled here. When permissions are being evaluated,
				it should have been performed already. If data entered is invalid (e.g.
				adding another entry to a one-on-one relasionship), the user's permission
				to perform said action can still be evaluated as :data:`True`.

		:attr:`controlled_columns <.PermissionControlMixin.controlled_columns>`:

			The columns in the mixed-in object a given user is allowed to view. By
			default, this value is an empty dictionary, meaning they're allowed to view
			all columns.

			.. seealso::
				:meth:`.PermissionControlMixin.get_allowed_columns`
		"""

		self.instance_actions = {}

		self.controlled_columns = {}

	static_actions = {}
	"""The actions a user is / isn't allowed to perform on any instance of the
	mixed-in object.

	.. seealso::
		:meth:`.PermissionControlMixin.get_allowed_static_actions`
	"""

	action_queries = {}
	"""Permissions for actions specified for the mixed-in object, but formatted
	in such a way that they are evaluable within SQL queries. Since a user can
	change the result, these should be callables with a single ``user`` argument.

	.. seealso::
		:meth:`.PermissionControlMixin.get`
	"""

	@classmethod
	def get_allowed_static_actions(
		cls,
		user
	) -> typing.List[str]:
		"""Gets a list of all actions that ``user`` is allowed to perform as per
		the mixed-in class's
		:attr:`static_actions <.PermissionControlMixin.static_actions>`.

		:param user: The user whose allowed actions should be obtained.

		:returns: A list of the allowed actions.
		"""

		return [
			action_name
			for action_name, action_func in cls.static_actions.items()
			if action_func(user)
		]

	def get_allowed_instance_actions(
		self,
		user
	) -> typing.List[str]:
		"""Gets a list of all actions that ``user`` is allowed to perform as per
		the current instance of the mixed-in class's
		:attr:`static_actions <.PermissionControlMixin.instance_actions>`.

		:param user: The user whose allowed actions should be obtained.

		:returns: A list of the allowed actions.
		"""

		return [
			action_name
			for action_name, action_func in self.instance_actions.items()
			if action_func(user)
		]

	def get_allowed_columns(
		self,
		user=None
	) -> typing.List[str]:
		"""Finds all columns in the current instance of the mixed-in class that
		``user`` is allowed to view, as per the
		:attr:`controlled_columns <.PermissionControlMixin.controlled_columns>`.

		If the value is an empty dictionary, all columns in this object are
		returned.

		If the value is not an empty dictionary, but doesn't contain some
		columns, it's assumed those are allowed to be viewed. To explicitly
		forbid them, use a function that always returns :data:`False`, but still
		accepts the ``user`` argument.

		:param user: The user whose permissions should be evaluated. If
			:data:`None` and the viewable columns are not an empty dictionary,
			an empty list is returned.

		:returns: A list of the allowed columns' names.
		"""

		all_columns = sqlalchemy.inspect(self).mapper.column_attrs

		if user is None and len(self.controlled_columns) != 0:
			return []

		allowed_columns = []

		for column in all_columns:
			if (
				column.key not in self.controlled_columns or
				self.controlled_columns[column.key](user)
			):
				allowed_columns.append(column.key)

		return allowed_columns

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False
	):
		"""Generates a selection query with permissions already handled. This
		may execute additional queries with some models.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that instances of the
			mixed-in class should fulfill, including the context of data used
			within actions - for example, parsed forum permissions. Generally,
			this will be generated using
			:attr:`action_queries <.PermissionControlMixin.action_queries>`.
			:data:`True` by default, meaning there are no additional conditions.
		:param conditions: Any additional conditions. :data:`True` by default,
			meaning there are no conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the object's
			primary key(s).

		:returns: The query.

		.. note::
			In this default implementation, ``conditions`` are in the same position
			as ``action_conditions`` and will be treated the same, but this is not
			the case with most other ones, and could cause trouble if not included
			here.
		"""

		# I don't like this. It seems Python doesn't support star expressions in
		# multiline if statements though. Maybe I can come up with a better way
		# to do it.

		if not pkeys_only:
			items_to_select = (cls,)
		else:
			items_to_select = sqlalchemy.inspect(cls).primary_key

		return (
			sqlalchemy.select(*items_to_select).
			where(
				sqlalchemy.and_(
					cls.action_queries["view"](user),
					conditions,
					action_conditions
				)
			).
			order_by(order_by).
			limit(limit).
			offset(offset)
		)


class ReprMixin:
	"""A utility mixin used to help with converting objects into strings."""

	def __repr__(self) -> str:
		"""Returns the :meth:`_repr <.ReprMixin._repr>` method with all of the
		mixed-in model's primary keys.
		"""

		return self._repr(
			**{
				primary_key.key: getattr(self, primary_key.key)
				for primary_key in sqlalchemy.inspect(self).mapper.primary_key
			}
		)

	def _repr(
		self,
		**fields: typing.Dict[str, typing.Any]
	) -> str:
		"""Automatically generates a pretty ``__repr__``, based on the given fields.

		:param fields: The fields to be included.

		:returns: The ``__repr__``.

		.. note::
			Function can be found in `this <https://stackoverflow.com/a/55749579>`_
			Stack Overflow discussion. Thanks to
			`Stephen Fuhry <https://stackoverflow.com/users/111033/stephen-fuhry>`_!
		"""

		field_strings = []
		at_least_one_attached_attr = False

		for key, field in fields.items():
			try:
				field_strings.append(f"{key}={field!r}")
			except sqlalchemy.orm.exc.DetachedInstanceError:
				field_strings.append(f"{key}=DetachedInstanceError")
			else:
				at_least_one_attached_attr = True

		if at_least_one_attached_attr:
			return f"<{self.__class__.__name__}({','.join(field_strings)})>"

		return f"<{self.__class__.__name__} {id(self)}>"


@sqlalchemy.orm.declarative_mixin
class IdMixin:
	"""A utility mixin used to uniquely identify objects."""

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)
	"""The ID of an object, in the form of a UUID4. Uses the :func:`.get_uuid`
	function to ensure there are no duplicates within the table, even if the
	chances of a collision occurring are extremely slim.
	"""


@sqlalchemy.orm.declarative_mixin
class CreationTimestampMixin:
	"""A utility mixin used to store the time an object was created."""

	creation_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		default=lambda: datetime.datetime.now(tz=datetime.timezone.utc),
		nullable=False
	)
	"""The time an object was created, in UTC."""


@sqlalchemy.orm.declarative_mixin
class EditInfoMixin:
	"""A utility mixin which contains information about the mixed-in object's
	edits.
	"""

	edit_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		nullable=True
	)
	"""The last time an object was edited. If :data:`None`, that has never
	happened so far.

	.. note::
		Originally, this used the ``onupdate`` property and changed each time
		an object was updated no matter the attribute, but since internal
		changes not requested by users will happen fairly often, we have to
		make an explicit :meth:`edited <.EditInfoMixin.edited>` method.
	"""

	edit_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The amount of times an object was edited. Increments by ``1`` each time
	:meth:`edited <.EditInfoMixin.edited>` is called. Defaults to ``0``.
	"""

	def edited(
		self,
		current_utc_time: typing.Union[None, datetime.datetime] = None
	) -> None:
		"""Sets the :attr:`edit_timestamp <.EditInfoMixin.edit_timestamp>`
		attribute to the current UTC date and time. Also increments the
		:attr:`edit_count <.EditInfoMixin.edit_timestamp>` attribute by ``1``,
		provided that it's under ``9223372036854775807`` or 2^63 - 1, the
		maximum 8-byte signed integer value.

		If the mixed-in object is an instance of :class:`.LoggerMixin`, logs
		this function being run.

		:param current_utc_time: The current date and time, in the UTC timezone,
			to be used as the time this object was edited. If :data:`None`, the
			date and time is recorded at runtime.

		.. note::
			Using an unsigned integer here would be more sensible. However, since
			this is not a standard SQL type and SQLAlchemy doesn't provide a
			standardized interface for it, it's been left as a signed one for the
			time being. Its limit is still much higher than any number occurring
			in the real world.
		"""

		if isinstance(self, LoggerMixin):
			self.logger.debug("Marking %s as edited", self)

		self.edit_timestamp = (
			current_utc_time
			if current_utc_time is not None
			else datetime.datetime.now(tz=datetime.timezone.utc)
		)

		if self.edit_count < constants.BIG_INTEGER_LIMIT:
			self.edit_count += 1


@sqlalchemy.orm.declarative_mixin
class BasePermissionMixin:
	"""A utility mixin with columns corresponding to all permissions recognized
	by default, as well as methods to use them.
	"""

	category_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	file_use = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_merge = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	group_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	group_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	group_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	group_edit_permissions = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	message_use = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	statistic_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_create_approved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_create_unapproved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_approval = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_lock_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_lock_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_pin = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_merge_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_merge_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_edit_ban = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_edit_groups = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_edit_permissions = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	user_edit_reputation = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)

	DEFAULT_PERMISSIONS = {
		"category_create": None,
		"category_delete": None,
		"category_edit": None,
		"category_move": None,
		"category_view": None,
		"file_use": None,
		"forum_create": None,
		"forum_delete": None,
		"forum_edit": None,
		"forum_merge": None,
		"forum_move": None,
		"forum_view": None,
		"group_create": None,
		"group_delete": None,
		"group_edit": None,
		"group_edit_permissions": None,
		"message_use": None,
		"post_create": None,
		"post_delete_own": None,
		"post_delete_any": None,
		"post_edit_own": None,
		"post_edit_any": None,
		"post_edit_vote": None,
		"post_move_own": None,
		"post_move_any": None,
		"post_view": None,
		"statistic_view": None,
		"thread_create_approved": None,
		"thread_create_unapproved": None,
		"thread_delete_own": None,
		"thread_delete_any": None,
		"thread_edit_own": None,
		"thread_edit_any": None,
		"thread_edit_approval": None,
		"thread_edit_lock_own": None,
		"thread_edit_lock_any": None,
		"thread_edit_pin": None,
		"thread_edit_vote": None,
		"thread_merge_own": None,
		"thread_merge_any": None,
		"thread_move_own": None,
		"thread_move_any": None,
		"thread_view": None,
		"user_delete": None,
		"user_edit": None,
		"user_edit_ban": None,
		"user_edit_groups": None,
		"user_edit_permissions": None,
		"user_edit_reputation": None
	}
	"""The default values of all permissions. In this case, :data:`None`."""

	def to_permissions(self) -> typing.Dict[
		str,
		typing.Union[
			None,
			bool
		]
	]:
		"""Transforms the values in this instance to the standard format for
		permissions - a dictionary, where string keys represent permissions,
		and their boolean value represents whether or not they're granted.
		"""

		return {
			permission_name: getattr(self, permission_name)
			for permission_name in self.DEFAULT_PERMISSIONS
		}
