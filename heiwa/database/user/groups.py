r"""Tables related to :class:`.User`\ s' :class:`.Group`\ s."""

import sqlalchemy

from .. import Base, utils

__all__ = ["user_groups"]


user_groups = sqlalchemy.Table(
	"user_groups",
	Base.metadata,
	sqlalchemy.Column(
		"user_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	),
	sqlalchemy.Column(
		"group_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"groups.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
)
r"""A table associating :class:`.User`\ s with respective
:class:`.Group`\ s.
"""
