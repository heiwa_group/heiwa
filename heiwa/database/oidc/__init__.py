"""Utility models for OIDC authentication."""

from __future__ import annotations

import sqlalchemy

from .. import Base, utils

__all__ = ["OIDCAuthentication"]
__version__ = "1.0.1"


class OIDCAuthentication(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	Base
):
	"""CSRF / replay attack protection model for OIDC authentication."""

	__tablename__ = "oidc_authentication"

	identifier = sqlalchemy.Column(
		sqlalchemy.String(64),
		primary_key=True
	)
	"""The unique identifier for the user who requested to authenticate.
	In general, this will be an IP address hashed using Argon2id.
	"""

	nonce = sqlalchemy.Column(
		sqlalchemy.String(30),
		nullable=False
	)
	"""An OIDC nonce."""

	state = sqlalchemy.Column(
		sqlalchemy.String(30),
		nullable=False
	)
	"""An OIDC state."""
