"""Utilities for generating Cerberus schemas used within API endpoints."""

import typing

import cerberus.schema

from heiwa import get_config

__all__ = [
	"get_search_schema",
	"get_search_schema_filter_in_max_list_length",
	"get_search_schema_filter_registry"
]


# Return type hint runcated, there is no real need for a hint so verbose
def get_search_schema(
	allowed_order_attrs: typing.Iterable[str],
	use_filter: bool = True,
	default_order_by: typing.Union[None, str] = None,
	default_order_asc: typing.Union[None, bool] = None,
	limit_max: typing.Union[None, int] = None,
	offset_max: typing.Union[None, int] = None,
	blueprint_name: typing.Union[None, str] = None
) -> typing.Dict:
	"""Generates a Cerberus schema used for validating input to endpoints using
	search functions. These can, for example, be ``list_``, ``mass_edit`` and
	``mass_delete``.

	:param allowed_order_attrs: The model attributes a user can order the
		results by.
	:param use_filter: Whether or not to include a validator for ``filter``.
		This is used for filtering out different results and supports a wide
		range of conditions, operators and recursion. :data:`True` by default.
	:param default_order_by: The attribute to order by by default. If left as
		:data:`None`, the user will have to specify an attribute in each request.
	:param default_order_asc: Whether or not the order will be ascending by
		default. If left as :data:`None`, the user will have to specify this in
		each request.
	:param limit_max: The maximum number of results. If left as :data:`None` and
		the provided ``blueprint_name`` is in the ``LIMIT_MAX_SPECIFIC`` config
		key, it's set to its max limit. If it's not present, this value is set to
		the ``LIMIT_MAX_DEFAULT`` config key.
	:param offset_max: The maximum offset. If left as :data:`None` and the
		provided ``blueprint_name`` is in the ``OFFSET_MAX_SPECIFIC`` config key,
		it's set to its max offset. If it's not present, this value is set to the
		``OFFSET_MAX_DEFAULT`` config key.
	:param blueprint_name: The current Flask
		:class:`Blueprint <flask.Blueprint>`'s :attr:`name <flask.Blueprint.name>`.
		This is used for fetching default ``limit_max`` and ``offset_max`` values,
		when they're unset. :data:`None` by default.

	:returns: The generated schema.
	"""

	config = get_config()

	if limit_max is None:
		if (
			blueprint_name is not None and
			blueprint_name in config["LIMIT_MAX_SPECIFIC"]
		):
			limit_max = config["LIMIT_MAX_SPECIFIC"][blueprint_name]
		else:
			limit_max = config["LIMIT_MAX_DEFAULT"]

		# limit_max = config["LIMIT_MAX_SPECIFIC"].get(
		# 	blueprint_name,
		# 	default=config["LIMIT_MAX_DEFAULT"]
		# )

	if offset_max is None:
		if (
			blueprint_name is not None and
			blueprint_name in config["OFFSET_MAX_SPECIFIC"]
		):
			offset_max = config["OFFSET_MAX_SPECIFIC"][blueprint_name]
		else:
			offset_max = config["OFFSET_MAX_DEFAULT"]

	result = {
		"order": {
			"type": "dict",
			"schema": {
				"by": {
					"type": "string",
					"allowed": allowed_order_attrs,
					"required": True
				},
				"asc": {
					"type": "boolean",
					"required": True
				}
			},
			"required": True
		},
		"limit": {
			"type": "integer",
			"min": 1,
			"max": limit_max,
			"required": True
		},
		"offset": {
			"type": "integer",
			"min": 0,
			"max": offset_max,
			"default": 0,
			"required": True
		}
	}

	if (
		default_order_by is not None and
		default_order_asc is not None
	):
		result["order"].update({
			"default": {
				"by": default_order_by,
				"asc": default_order_asc
			}
		})
	elif default_order_by is not None:
		result["order"]["schema"]["by"]["default"] = default_order_by
	elif default_order_asc is not None:
		result["order"]["schema"]["asc"]["default"] = default_order_asc

	if use_filter:
		result["filter"] = {
			"type": "dict",
			"schema": "search_filter",
			"minlength": 1,
			"maxlength": 1
		}

	return result


def get_search_schema_filter_in_max_list_length(
	blueprint_name: typing.Union[None, str] = None
) -> int:
	"""Gets the maximum length of lists contained within the ``$in`` search
	filtering schema. If ``blueprint_name`` is left as :data:`None`, the value
	returned is the ``SEARCH_MAX_IN_LIST_LENGTH_DEFAULT`` config key. If not,
	the value can also be its entry in ``SEARCH_MAX_IN_LIST_LENGTH_SPECIFIC``,
	if it has one.

	:param blueprint_name: The current Flask
		:class:`Blueprint <flask.Blueprint>`'s :attr:`name <flask.Blueprint.name>`.

	:returns: The maximum ``$in`` list length.
	"""

	config = get_config()

	if (
		blueprint_name is not None and
		blueprint_name in config["SEARCH_MAX_IN_LIST_LENGTH_SPECIFIC"]
	):
		return (
			config
			["SEARCH_MAX_IN_LIST_LENGTH_SPECIFIC"]
			[blueprint_name]
		)

	return config["SEARCH_MAX_IN_LIST_LENGTH_DEFAULT"]


def get_search_schema_filter_registry(
	rules: typing.Dict[
		str,
		typing.Dict[
			str,
			typing.Union[
				typing.Dict,
				typing.Any
			]
		]
	],
	schema_name: str = "search_filter"
) -> cerberus.schema.SchemaRegistry:
	"""Generates a registry for use by the ``filter`` key in search schemas.

	:param rules: All additional rules
	:param schema_name: The name of the schema. Uses a sensisble ``search_filter``
		default.

	:returns: The schema.
	"""

	inner_schema = {
		"type": "list",
		"schema": {
			"type": "dict",
			"schema": schema_name,
			"minlength": 1,
			"maxlength": 1
		},
		"minlength": 2
	}

	return cerberus.schema.SchemaRegistry({
		schema_name: {
			**rules,
			"$and": inner_schema,
			"$or": inner_schema,
			"$not": inner_schema["schema"]
		}
	})
