Abstract types
==============

.. automodule:: heiwa.types
   :members:
   :special-members:
