"""SQLAlchemy ORM models, tables and utilities."""

import sqlalchemy.orm

Base = sqlalchemy.orm.declarative_base()

from . import utils
from .category import Category
from .file import File
from .forum import (
	Forum,
	ForumParsedPermissions,
	ForumPermissionMixin,
	ForumPermissionsGroup,
	ForumPermissionsUser,
	forum_subscribers
)
from .group import Group, GroupPermissions
from .local_account import LocalAccount, LocalAccountTOTP
from .message import Message, MessageDirectory
from .notification import Notification
from .oidc import OIDCAuthentication
from .post import Post, PostVote
from .statistic import Statistic
from .thread import Thread, ThreadViewRecord, ThreadVote, thread_subscribers
from .user import (
	User,
	UserBan,
	UserPermissions,
	UserReputation,
	UserPreferences,
	user_blocks,
	user_follows,
	user_groups
)

__all__ = [
	"Base",
	"Category",
	"File",
	"Forum",
	"ForumParsedPermissions",
	"ForumPermissionMixin",
	"ForumPermissionsGroup",
	"ForumPermissionsUser",
	"Group",
	"GroupPermissions",
	"LocalAccount",
	"LocalAccountTOTP",
	"Message",
	"MessageDirectory",
	"Notification",
	"OIDCAuthentication",
	"Post",
	"PostVote",
	"Statistic",
	"Thread",
	"ThreadViewRecord",
	"ThreadVote",
	"User",
	"UserBan",
	"UserPermissions",
	"UserPreferences",
	"UserReputation",
	"forum_subscribers",
	"thread_subscribers",
	"user_blocks",
	"user_follows",
	"user_groups",
	"utils"
]
__version__ = "0.45.3"
